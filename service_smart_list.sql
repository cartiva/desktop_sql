﻿/*
-- sales looks ok

select a.buyer_bopname_id, b.make, b.model, b.inpmast_vin, a.delivery_date
-- select count(*) -- 14438
from sls.ext_bopmast_partial a
inner join arkona.xfm_inpmast b on a.vin = b.inpmast_vin
  and b.current_row = true
where b.year between 2014 and 2018
  and b.make in ('chevrolet','buick','gmc','cadillac','honda','nissan')
  and a.deal_status = 'U'
  and a.capped_date = (
    select max(capped_date)
    from sls.ext_bopmast_partial
    where vin = a.vin);


drop table if exists sales cascade;
create temp table sales as
select *
  -- select count(*) -- 14438
from (
  select a.store as store_code, a.buyer_bopname_id, b.make, b.model, b.inpmast_vin, delivery_date, 
    row_number() over (partition by vin order by a.delivery_date desc)
  from sls.ext_bopmast_partial a
  inner join arkona.xfm_inpmast b on a.vin = b.inpmast_vin
    and b.current_row = true
  where b.year between 2014 and 2018
    and b.make in ('chevrolet','buick','gmc','cadillac','honda','nissan')
    and a.deal_status = 'U'
) x where row_number = 1;
 
create index on sls.ext_bopmast_partial(capped_date);
create index on sls.ext_bopmast_partial(vin);  
create index on sls.ext_bopmast_partial(deal_status);
create index on sls.ext_bopmast_partial(store);

-- service looks ok 
drop table if exists service;
create temp table service as
-- select count(*) from ( -- 19996
select *
from (
  select a.customerkey, b.make, b.model, b.vin, d.the_date, row_number() over (partition by vin order by the_date desc)
  from ads.ext_fact_repair_order a
  inner join ads.ext_dim_Vehicle b on a.vehiclekey = b.vehiclekey
  inner join ads.ext_dim_customer c on a.customerkey = c.customerkey
  inner join dds.dim_date d on a.closedatekey = d.date_key
  where b.modelyear in ('2014','2015','2016','2017','2018')
    and b.make in ('chevrolet','buick','gmc','cadillac','honda','nissan')
    and c.fullname not like '%VOIDED%'
    and c.fullname not like 'INVENTORY%'  
    and c.fullname not like 'NATIONAL%'
    and c.fullname not like 'RYDELL A%'
    and length(trim(a.ro)) > 6  
  group by a.customerkey, b.make, b.model, b.vin, d.the_date
) x where row_number = 1


drop table if exists combo_1;
create temp table combo_1 as
select 'sales', a.buyer_bopname_id, make, model, inpmast_vin as vin, delivery_date as sale_date, null::date as service_date
from sales a
union 
select 'service', customerkey, make, model, vin, null::Date as sale_Date, the_date as service_Date
from service b
order by vin;

select a.*, b.first_name, b.last_company_name
from combo_1 a
left join arkona.xfm_bopname b on a.buyer_bopname_id = b.bopname_record_key
  and b.current_row 
where b.last_company_name is null  

-- way fucking too many without bopname records
-- for example
select *
from arkona.xfm_bopname
where bopname_record_key = 187595 -- which is customerkey in factrepairorder for ro 2719484

that value, 187595 does not exists anywhere except in factrepairorder

select *
from ads.ext_dim_customer 
-- where lastname = 'wanzel'
where bnkey = 187595

select *
from ads.ext_Fact_repair_order
where ro = '2719484'

select min(open_Date), max(open_date)
from arkona.ext_Sdprhdr_history
where ro_number = '2719484'

select *
from arkona.ext_Sdprhdr_history
where ro = '2719484'


select ro_number, customer_key, cust_name, open_date, vin
from arkona.ext_sdprhdr
union
select ro, customer_key, customer_name, open_date, vin
from arkona.ext_sdprhdr_history



select * -- 15 fucking thousand rows
from arkona.xfm_Bopname a 
where not exists(
  select 1 
  from ads.ext_dim_customer
  where bnkey = a.bopname_record_key)

select *  -- holy shit 174911 rows
from ads.ext_fact_Repair_order a 
where not exists (
  select 1
  from arkona.xfm_bopname
  where bopname_record_key = a.customerkey)


select *  -- 3700 rows
from ads.ext_dim_customer a
where not exists (
  select 1
  from arkona.xfm_bopname
  where bopname_record_key = a.bnkey)  

---------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------
so this is all a different issue
for this report use sdprhdr and bopname for service


---------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------


drop table if exists ros cascade;
create temp table ros as
select company_number as store_code, ro_number, customer_key, cust_name, arkona.db2_integer_to_date_long(open_date) as open_date, vin
from arkona.ext_sdprhdr
union
select store_code, ro, customer_key, customer_name, open_date, vin
from arkona.ext_sdprhdr_history
where open_date > '01/01/2013';
create index on ros(vin);
create index on ros(customer_key);
create index on ros(cust_name);
create index on ros(store_code);


drop table if exists service cascade;
create temp table service as
select *
from (
select store_code, a.customer_key, b.make, b.model, a.vin, a.open_date,
   row_number() over (partition by vin order by open_date desc) 
from ros a 
join arkona.xfm_inpmast b on a.vin = b.inpmast_vin
  and b.current_row 
  and b.make in ('chevrolet','buick','gmc','cadillac','honda','nissan')
  and b.year between 2014 and 2018
where a.cust_name not like '%VOIDED%'
  and a.cust_name not like 'INVENTORY%'  
  and a.cust_name not like 'NATIONAL%'
  and a.cust_name not like 'RYDELL A%'
  and a.cust_name not like '%ADESA%'
) x where row_number = 1;


drop table if exists combo_1;
create temp table combo_1 as
select 'sales', store_code, a.buyer_bopname_id, make, model, inpmast_vin as vin, delivery_date as sale_date, null::date as service_date
from sales a
union 
select 'service',store_code, customer_key, make, model, vin, null::Date as sale_Date, open_date as service_Date
from service b
order by vin;


select vin
from (
  select vin, first_name, last_company_name
  from (
    select a.*, b.first_name, b.last_company_name
    from combo_1 a
    join arkona.xfm_bopname b on a.buyer_bopname_id = b.bopname_record_key
      and b.current_row) c
  group by vin, first_name, last_company_name) d  
group by vin
having count(*) > 1


select last_company_name, count(*)
from (
select a.*, b.first_name, b.last_company_name
from combo_1 a
join arkona.xfm_bopname b on a.buyer_bopname_id = b.bopname_record_key
  and b.current_row
  and b.last_company_name <> 'ADESA'
  and b.last_comapny_name not like 'RYDELL A%') x
group by  last_company_name    
order by count(*) desc

select *
from (
select a.*, b.first_name, b.last_company_name
from combo_1 a
join arkona.xfm_bopname b on a.buyer_bopname_id = b.bopname_record_key
  and b.current_row
  and b.last_company_name <> 'ADESA') x
where first_name like '%inven%'

drop table if exists combo_2 cascade;
create temp table combo_2 as
select a.store_code, a.buyer_bopname_id, a.make, a.model, a.vin, b.first_name, b.last_company_name, 
  max(a.sale_Date) as sale_date, max(a.service_date) as service_date
from combo_1 a
join arkona.xfm_bopname b on a.buyer_bopname_id = b.bopname_record_key
  and a.store_code = b.bopname_company_number
  and b.current_row
  and b.last_company_name not like '%ADESA%'
  and b.last_company_name not like 'RYDELL A%'
group by a.store_code, a.buyer_bopname_id, a.make, a.model, a.vin, b.first_name, b.last_company_name;


select *
from combo_2 limit
where vin in (
  select vin
  from combo_2
  group by vin
  having count(*) > 1)
order by vin  


drop table if exists combo_3 cascade;
create temp table combo_3 as
select a.first_name, a.last_company_name, b.address_1 as address, b.city, b.state_code as state, b.zip_code as zip,
  a.make, a.model, a.vin, a.service_date as last_service_date, a.sale_date as last_sale_date
from combo_2 a
join arkona.xfm_bopname b on a.buyer_bopname_id = b.bopname_record_key
where b.current_row;

select *
from combo_3
order by last_company_name



the issue is store, PK of bopname is store/bopname_record_key, so, i guess, use store of sale or ro

-- look ast mcsparrow for example, multiple rows for same car due to different addresses
select *
from combo_3
where make = 'honda'

select *
from combo_3
where last_company_name = 'mcsparron';

select *
from 

fucking name file
select * from  arkona.xfm_bopname where last_company_name = 'mcsparron'

select * from  arkona.xfm_bopname where bopname_record_key = 1000194


and now the issue is the ro store does not match the bopname store
for instance: 

bopname_record_key 1000194 : RY1 mcspannon
                             RY2 gehrke
ro done at RY2, cust on ro is mcspannon
looks like i am going to have to match name as well ?!?!?!  
                           
select * from ros where cust_name like 'mcspa%'

select * from arkona.xfm_inpmast where inpmast_vin = '5FNRL5H66FB060931'

select * from service where vin = '5FNRL5H66FB060931'

select * from combo_1 where vin = '5FNRL5H66FB060931'

select * from combo_2 where vin = '5FNRL5H66FB060931'

select * from arkona.xfm_bopname where bopname_record_key = 1000194


select a.cust_name, b.bopname_search_name, similarity(a.cust_name, b.bopname_search_name ) 
from arkona.ext_sdprhdr a
left join arkona.xfm_bopname b on a.customer_key = b.bopname_record_key
  and b.current_row
where a.ro_number in ('2718209','2730627','2782729','2782877')


select a.store_code, a.buyer_bopname_id, a.make, a.model, a.vin, b.first_name, b.last_company_name
from combo_1 a
join arkona.xfm_bopname b on a.buyer_bopname_id = b.bopname_record_key
  and a.store_code = b.bopname_company_number
  and b.current_row
  and b.last_company_name not like '%ADESA%'
  and b.last_company_name not like 'RYDELL A%'
group by a.store_code, a.buyer_bopname_id, a.make, a.model, a.vin, b.first_name, b.last_company_name;

select * from ros limit 10

select a.*, b.bopname_company_number, b.bopname_search_name, similarity(a.cust_name, b.bopname_search_name) 
from ros a
left join arkona.xfm_bopname b on a.customer_key = b.bopname_record_key
  and b.current_row
where a.vin not like '0%'  
  and a.vin not like '1111%'
  and length(a.vin) = 17
  and a.cust_name not like '%VOIDED%'
  and a.cust_name not like 'INVENTORY%'  
  and a.cust_name not like 'NATIONAL%'
  and a.cust_name not like 'RYDELL A%'
  and a.cust_name not like '%ADESA%'  
  and a.cust_name not like 'CARWASH%'
order by ro_number 
limit 2000


select a.*, b.bopname_search_name, similarity(a.cust_name, b.bopname_search_name)
from (
select a.store_code, a.customer_key, a.cust_name
from ros a
where a.vin not like '0%'  
  and a.vin not like '1111%'
  and length(a.vin) = 17
  and a.cust_name not like '%VOIDED%'
  and a.cust_name not like 'INVENTORY%'  
  and a.cust_name not like 'NATIONAL%'
  and a.cust_name not like 'RYDELL A%'
  and a.cust_name not like '%ADESA%'  
  and a.cust_name not like 'CARWASH%'
group by a.store_code, a.customer_key, a.cust_name) a
left join arkona.xfm_bopname b on a.customer_key = b.bopname_Record_key
  and b.current_row
order by a.customer_key  

select *
from arkona.xfm_bopname
where bopname_Record_key = 395

*/
-- 10/12 where it's at
fucking name file
do sales and service separately
match key and name for each

so, partial doesnt have the name
select * from sls.ext_bopmast_partial limit 10


select count(*) from arkona.ext_bopmast  --48823
select count(*) from sls.ext_bopmast_partial --55503


select max(delivery_date) from arkona.ext_bopmast -- 10/5/17

fuck it, doing a full scrape on bopmast
got it


drop table if exists sales_1 cascade;
create temp table sales_1 as
select bopmast_company_number, record_key, bopmast_vin, buyer_number, bopmast_Search_name, delivery_date
from (
  select a.bopmast_company_number, a.record_key, a.bopmast_vin, a.buyer_number, a.bopmast_Search_name, a.delivery_date,
    row_number() over (partition by a.bopmast_vin order by a.delivery_date desc) 
  from arkona.ext_bopmast a
  join arkona.xfm_inpmast b on a.bopmast_vin = b.inpmast_vin
    and b.current_row
    and b.year between 2014 and 2018
    and b.make in ('chevrolet','buick','gmc','cadillac','honda','nissan')
  where a.record_Status = 'U'
  and a.bopmast_search_name not like 'RYDELL %'
  and a.bopmast_search_name not like 'ADESA%'   ) c
where row_number = 1;
create unique index on sales_1(bopmast_vin);

-- this could be getting close
drop table if exists sales_2 cascade;
create temp table sales_2 as
select a.*, b.bopname_company_number, b.bopname_search_name, similarity(a.bopmast_Search_name, b.bopname_search_name),
  row_number() over (partition by a.bopmast_vin order by similarity(a.bopmast_Search_name, b.bopname_search_name) desc),
  a.bopmast_company_number = b.bopname_company_number 
from sales_1 a
left join arkona.xfm_bopname b on a.buyer_number = b.bopname_record_key
  and b.current_row;

-- sales 2 with multiple bopname records, 338 rows
select * 
from sales_2 a
where exists (
  select 1
  from sales_2
  where bopmast_company_number = a.bopmast_company_number
    and record_key = a.record_key
      and row_number > 1)
      
-- limit it with similarity, good enuf, 158 rows and no multiple rows for deal
drop table if exists sales_3 cascade;
create temp table sales_3 as
select * 
from sales_2 a
where exists (
  select 1
  from sales_2
  where bopmast_company_number = a.bopmast_company_number
    and record_key = a.record_key
      and row_number > 1)
and similarity > .2    
union
select * --14134
from sales_2 a
where not exists (
  select 1
  from sales_2
  where bopmast_company_number = a.bopmast_company_number
    and record_key = a.record_key
      and row_number > 1);
create unique index on sales_3(bopmast_company_number, record_key);      

-- sales 3 now has everything i need to create the final sales part of this stuff
drop table if exists sales_4 cascade;
create temp table sales_4 as
select b.first_name, b.last_company_name, b.address_1 as address, b.city, 
  b.state_code as state, b.zip_code as zip,
  c.make, c.model, c.inpmast_vin as vin, null::date as last_service_date, 
  a.delivery_date as last_sale_date
from sales_3 a
join arkona.xfm_bopname b on a.buyer_number = b.bopname_record_key
  and b.current_row
  and a.bopname_search_name = b.bopname_search_name
join arkona.xfm_inpmast c on a.bopmast_vin = c.inpmast_vin
  and c.current_row;  
create unique index on sales_4(vin) ;  

-- ok, now service
-- 1 row per ro, all data from sdprhdr
drop table if exists service_1 cascade;
create temp table service_1 as
select a.company_number, a.ro_number, a.vin, a.customer_key, a.cust_name, 
  arkona.db2_integer_to_date_long(open_date) as open_date
from arkona.ext_sdprhdr a
join arkona.xfm_inpmast b on a.vin = b.inpmast_vin
  and b.current_row
  and b.year between 2014 and 2018
  and b.make in ('chevrolet','buick','gmc','cadillac','honda','nissan')
where a.cust_name not like '%VOIDED%'
  and a.cust_name not like 'INVENTORY%'  
  and a.cust_name not like 'NATIONAL%'
  and a.cust_name not like 'RYDELL A%'  
  and a.cust_name <> 'COURTESY DELIVERY'
  and a.cust_name not like '%HONDA%';
create unique index on service_1(ro_number);


-- limit to most recent ro
drop table if exists service_2 cascade;
create temp table service_2 as
select company_number, ro_number, vin, customer_key, cust_name, open_date
from (
  select a.*, 
    row_number() over (partition by a.vin order by open_date desc)
  from service_1 a) b
where row_number = 1;
create unique index on service_2(ro_number);
create unique index on service_2(vin);

-- add the join to bopname
drop table if exists service_3 cascade;
create temp table service_3 as
select a.*, b.bopname_company_number, b.bopname_search_name, similarity(a.cust_name, b.bopname_search_name),
  row_number() over (partition by a.vin order by similarity(a.cust_name, b.bopname_search_name) desc)
from service_2 a
left join arkona.xfm_bopname b on a.customer_key = b.bopname_record_key
  and b.current_row;


-- service_3 with multiple bopname records, 314 rows
select * 
from service_3 a
where exists (
  select 1
  from service_3
  where company_number = a.company_number
    and customer_key = a.customer_key
      and row_number > 1)
   

-- just like sales, .2 similarity works
-- limit it with similarity, good enuf, 158 rows and no multiple rows for deal
drop table if exists service_4 cascade;
create temp table service_4 as
select * 
from service_3 a
where exists (
  select 1
  from service_3
  where company_number = a.company_number
    and customer_key = a.customer_key
      and row_number > 1)
and similarity > .2    
union
select * 
from service_3 a
where not exists (
  select 1
  from service_3
  where company_number = a.company_number
    and customer_key = a.customer_key
      and row_number > 1);
create unique index on service_4(ro_number);
create unique index on service_4(vin);      

select * from service_4


drop table if exists service_5 cascade;
create temp table service_5 as
select b.first_name, b.last_company_name, b.address_1 as address, b.city, 
  b.state_code as state, b.zip_code as zip,
  c.make, c.model, c.inpmast_vin as vin, a.open_date as last_service_date, 
  null::date as last_sale_date
from service_4 a
join arkona.xfm_bopname b on a.customer_key = b.bopname_record_key
  and b.current_row
  and a.cust_name = b.bopname_search_name
join arkona.xfm_inpmast c on a.vin = c.inpmast_vin
  and c.current_row;  
create unique index on service_5(vin) ;  

drop table if exists combo_1;
create temp table combo_1 as
select * from sales_4
union
select * from service_5;

select * from combo_1 limit 10

drop table if exists combo_2 cascade;
create temp table combo_2 as
select first_name,last_company_name,address,city,state,zip,make,model,vin,
  max(last_service_date) as last_service_date, max(last_sale_date) as last_sale_date
from combo_1 a
-- where vin = '1GTV2UEC2EZ262017'
group by first_name,last_company_name,address,city,state,zip,make,model,vin;
create unique index on combo_2(vin, first_name, last_company_name, address, city, zip);


select * from combo_2 order by vin

do a union on sales & service without the null date fieldthen just get the most recent date

select * from sales_4 limit 10


-- 10/14/18 this is what i sent to andrew
select first_name,last_company_name,address,city,state,zip,make,model,vin,
  case when source = 'service' then the_date end as last_service_date,
  case when source = 'sales' then the_date end as last_sale_date
from (
select x.*, row_number() over (partition by vin order by the_date desc)
from (
  select 'sales' as source, first_name,last_company_name,address,city,state,zip,make,model,vin,last_sale_date as the_date from sales_4 
  union
  select 'service', first_name,last_company_name,address,city,state,zip,make,model,vin,last_service_date as the_date from service_5) x) y
where row_number = 1  
  and length(vin) = 17
order by 
  case
    when make = 'HONDA' then make
    when make = 'NISSAN' then make
    else 'xxx'
  end desc




















dont know how or why i overlooked it, but PK on bopname is store/bopname_record_key
duh
select bopname_Record_key from arkona.xfm_bopname where current_row group by bopname_Record_key having count(*) > 1
select * from arkona.xfm_bopname where bopname_Record_key = 1004626















