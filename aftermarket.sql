﻿-- inventory  preloaded with aftermarket
-- posted to inventory account, journal AFM, occasionally CRC
select *
from (
  select distinct stock_number, tags
  from vauto.mike_corp_report
  where tags like '%after%') a
full outer join (
  select a.control, b.inpmast_vin, c.account, d.journal_code, e.description, sum(a.amount)
  from fin.fact_gl a
  inner join (
    select inpmast_stock_number, inpmast_vin
    from arkona.xfm_inpmast
    where status = 'I'
      and current_row = true
      and type_n_u = 'N'
      and left(inventory_account, 1) = '1') b on a.control = b.inpmast_stock_number
  inner join fin.dim_account c on a.account_key = c.account_key
  inner join fin.dim_journal d on a.journal_key = d.journal_key
  inner join fin.dim_gl_Description e on a.gl_description_key = e.gl_description_key  
  where d.journal_code = 'AFM' or (d.journal_code = 'CRC' and e.description like '%ACCES%')
  group by a.control, b.inpmast_vin, c.account, d.journal_code, e.description) b on a.stock_number = b.control
order by b.control

there are 3 vehicles, G34481, G34671, G34518
where aftermarket is posted to both AFM and CRC
these are errors
at this time, 9/25, the only vehicle to which aftermarket is posted (correctly ?) to only CRC is G33242, G34617, G34576


-- so, let's do AFM, and only CRC where no AFM exists
-- no grouping required for AFM
drop table if exists afm;
create temp table afm as
select aa.the_date, a.control, b.inpmast_vin, c.account, d.journal_code, e.description, amount
from fin.fact_gl a
inner join dds.dim_date aa on a.date_key = aa.date_key
inner join (
  select inpmast_stock_number, inpmast_vin
  from arkona.xfm_inpmast
  where status = 'I'
    and current_row = true
    and type_n_u = 'N'
    and left(inventory_account, 1) = '1') b on a.control = b.inpmast_stock_number
inner join fin.dim_account c on a.account_key = c.account_key
inner join fin.dim_journal d on a.journal_key = d.journal_key
inner join fin.dim_gl_Description e on a.gl_description_key = e.gl_description_key  
where d.journal_code = 'AFM'
order by control;

-- and only crc
select aa.the_date, a.control, b.inpmast_vin, c.account, d.journal_code, e.description, amount
from fin.fact_gl a
inner join dds.dim_date aa on a.date_key = aa.date_key
inner join (
  select inpmast_stock_number, inpmast_vin
  from arkona.xfm_inpmast
  where status = 'I'
    and current_row = true
    and type_n_u = 'N'
    and left(inventory_account, 1) = '1') b on a.control = b.inpmast_stock_number
inner join fin.dim_account c on a.account_key = c.account_key
inner join fin.dim_journal d on a.journal_key = d.journal_key
inner join fin.dim_gl_Description e on a.gl_description_key = e.gl_description_key  
where d.journal_code = 'CRC' 
  and e.description like '%ACCES%'
  and not exists (
    select 1
    from afm 
    where control = a.control)

-- good enuf
select *
from (
  select a.control as stock_number, amount as aftermarket
--   select aa.the_date, a.control, b.inpmast_vin, c.account, d.journal_code, e.description, amount
  from fin.fact_gl a
  join dds.dim_date aa on a.date_key = aa.date_key
  join (
    select inpmast_stock_number, inpmast_vin
    from arkona.xfm_inpmast
    where status = 'I'
      and current_row = true
      and type_n_u = 'N'
      and left(inventory_account, 1) = '1') b on a.control = b.inpmast_stock_number
  join fin.dim_account c on a.account_key = c.account_key
  join fin.dim_journal d on a.journal_key = d.journal_key
  join fin.dim_gl_Description e on a.gl_description_key = e.gl_description_key  
  where d.journal_code = 'AFM') a
union 
  select a.control as stock_number, amount as aftermarket
--   select aa.the_date, a.control, b.inpmast_vin, c.account, d.journal_code, e.description, amount
  from fin.fact_gl a
  join dds.dim_date aa on a.date_key = aa.date_key
  join (
    select inpmast_stock_number, inpmast_vin
    from arkona.xfm_inpmast
    where status = 'I'
      and current_row = true
      and type_n_u = 'N'
      and left(inventory_account, 1) = '1') b on a.control = b.inpmast_stock_number
  join fin.dim_account c on a.account_key = c.account_key
  join fin.dim_journal d on a.journal_key = d.journal_key
  join fin.dim_gl_Description e on a.gl_description_key = e.gl_description_key  
  where d.journal_code = 'CRC' 
    and e.description like '%ACCES%'
order by the_date    



-- questions for jeri

  select *
  from (
  --   select a.control as stock_number, amount as aftermarket
    select aa.the_date, a.control, b.inpmast_vin, c.account, d.journal_code, e.description, amount
    from fin.fact_gl a
    join dds.dim_date aa on a.date_key = aa.date_key
    join (
      select inpmast_stock_number, inpmast_vin
      from arkona.xfm_inpmast
      where status = 'I'
        and current_row = true
        and type_n_u = 'N'
        and left(inventory_account, 1) = '1') b on a.control = b.inpmast_stock_number
    join fin.dim_account c on a.account_key = c.account_key
    join fin.dim_journal d on a.journal_key = d.journal_key
    join fin.dim_gl_Description e on a.gl_description_key = e.gl_description_key  
    where d.journal_code = 'AFM') a
  union all
  --   select a.control as stock_number, amount as aftermarket
    select aa.the_date, a.control, b.inpmast_vin, c.account, d.journal_code, e.description, amount
    from fin.fact_gl a
    join dds.dim_date aa on a.date_key = aa.date_key
    join (
      select inpmast_stock_number, inpmast_vin
      from arkona.xfm_inpmast
      where status = 'I'
        and current_row = true
        and type_n_u = 'N'
        and left(inventory_account, 1) = '1') b on a.control = b.inpmast_stock_number
    join fin.dim_account c on a.account_key = c.account_key
    join fin.dim_journal d on a.journal_key = d.journal_key
    join fin.dim_gl_Description e on a.gl_description_key = e.gl_description_key  
    where d.journal_code = 'CRC' 
      and (e.description like '%ACCES%' or e.description like '%AFM%')
order by control      