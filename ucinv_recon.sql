﻿
select year_month, page, line, col, line_label, gm_account, gl_account, store, area, department, sub_Department, amount
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
where page = 1
  and year_month > 201706
  and store = 'ry1'


select b.the_date, a.control, a.doc, c.account, c.description, d.journal_code, a.amount, e.description, 
  account_type_code, account_type, department
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
inner join fin.dim_account c on a.account_key = c.account_key
inner join fin.dim_journal d on a.journal_key = d.journal_key
inner join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key
where a.post_status = 'Y'
  and account in ('124000','124001','124100','124101')
  and journal_code in ('SVI','POT')
  and department = 'used vehicle'  
--   and year_month = 201706
  and control in (
    select stock_number
    from sls.deals
    where year_month = 201706)
order by length(control), control


-- ucinv_tmp_recon_ros_1 ----------------------------------------------------------------
-- from inventory accounts
select control, doc, sum(amount)
from (
select b.the_date, a.control, a.doc, c.account, c.description, d.journal_code, a.amount, e.description, 
  account_type_code, account_type, department
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
inner join fin.dim_account c on a.account_key = c.account_key
inner join fin.dim_journal d on a.journal_key = d.journal_key
inner join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key
where a.post_status = 'Y'
  and account in ('124000','124001','124100','124101')
  and journal_code in ('SVI','POT')
  and department = 'used vehicle'  
--   and year_month = 201706
  and control in (
    select stock_number
    from sls.deals
    where year_month = 201706)) x
group by control, doc    

-- P6 accounts, this feels "off" but let's forge ahead
select b.the_date, a.control, a.doc, c.account, c.description, d.journal_code, a.amount, e.description, 
  account_type_code, account_type, department
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
inner join fin.dim_account c on a.account_key = c.account_key
inner join fin.dim_journal d on a.journal_key = d.journal_key
inner join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key
where a.post_status = 'Y'
  and account_type = 'cogs'
  and journal_code in ('SVI','POT','SCA')
  and department = 'used vehicle'  
--   and year_month = 201706
  and control in (
    select stock_number
    from sls.deals
    where year_month = 201706)

-- that isn't exactly what the ads query is doing, try to match it better
-- this is it
drop table if exists tmp_recon_ros;
create temp table tmp_recon_ros as
select control as stock_number, doc as ro, sum(amount) as amount  
from fin.fact_gl a
inner join fin.dim_account c on a.account_key = c.account_key
inner join fin.dim_journal d on a.journal_key = d.journal_key
inner join (
  select stock_number
  from sls.deals
  where store_code = 'ry1'
    and vehicle_type_code = 'u'
    and year_month = 201706
  group by stock_number
  having sum(unit_count) > 0  ) e on a.control = e.stock_number
where a.post_status = 'Y'
  and
    case
      when c.account in ('124000','124001','124100','124101') then d.journal_code in ('SVI','POT')
      when c.account in (
        select gl_account
        from fin.fact_fs a
        inner join fin.dim_fs_account b on a.fs_account_key = b.fs_account_key
        inner join fin.dim_fs c on a.fs_key = c.fs_key
        inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
        inner join fin.dim_account e on b.gl_account = e.account
        where c.page = 16
          and e.department_code = 'uc'
          and e.account_type_code = '5'
          and d.store = 'ry1'
        group by gl_account) then d.journal_code in ('SVI','SCA','POT')
    end
group by control, doc
having sum(amount) <>  0;   

select * from tmp_recon_ros order by length(stock_number), stock_number
select * from tmp_recon_ros where stock_number = '29360a'
-- ucinv_tmp_recon_ros_1 ---------------------------------------------------------------- 

-- ucinv_tmp_Recon_sales ----------------------------------------------------------------
drop table if exists tmp_recon_gross;
create temp table tmp_recon_gross as
select a.stock_number, -1 * sum(b.amount) as recon_gross
from tmp_recon_ros a
inner join fin.fact_gl b on a.ro = b.control
inner join fin.dim_account c on b.account_key = c.account_key
  and c.department_code in ('bs','ql','re','sd','pd')
inner join (
  select gl_account
  from fin.fact_fs a
  inner join fin.dim_fs_account b on a.fs_account_key = b.fs_account_key
  inner join fin.dim_fs c on a.fs_key = c.fs_key
  inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
  inner join fin.dim_account e on b.gl_account = e.account
  where c.page = 16
  group by gl_account) d on c.account = d.gl_account
group by a.stock_number

order by stock_number

select * from tmp_recon_gross

select * from sls.deals limit 10

-- ucinv_tmp_Recon_sales ----------------------------------------------------------------

-- recon good (enuf)
drop table if exists uc_gross;
create temp table uc_gross as
select a.stock_number, aa.sale_group, aa.model_year, aa.make, aa.model, 
  case sale_type_code
    when 'R' then 'retail'
    when 'W' then 'wholesale'
  end as sale_type, 
  a.delivery_date::date - c.fromts::date as days_owned, 
  coalesce(d.days_available, 0) as days_available,
  e.front_gross, e.fi_gross, coalesce(b.recon_gross, 0) as recon_gross,
  coalesce(e.front_gross, 0) + coalesce(e.fi_gross, 0) + coalesce(b.recon_gross, 0) as total_gross
from (
  select stock_number, vin, sale_type_code, delivery_date
  from sls.deals
  where year_month = 201706
    and store_code = 'ry1'
    and vehicle_type_code = 'u'
  group by stock_number, vin, sale_type_code, delivery_date
  having sum(unit_count) > 0) a
left join (
  select *
  from sls.deals_by_month
  where year_month = 201706
    and store_code = 'ry1'
    and vehicle_type = 'used') aa on a.stock_number = aa.stock_number
left join tmp_recon_gross b on a.stock_number = b.stock_number
left join ads.ext_vehicle_inventory_items c on a.stock_number = c.stocknumber
left join (
  select a.vehicleinventoryitemid, 
    case
      when a.thruts is null then current_date - a.fromts::date
      else a.thruts::date - a.fromts::date
    end as days_available
  from ads.ext_vehicle_inventory_item_statuses a
  inner join (
    select vehicleinventoryitemid, max(fromts) as fromts
    from ads.ext_vehicle_inventory_item_statuses
    where category = 'RMFlagAV'
    group by vehicleinventoryitemid) b on a.vehicleinventoryitemid = b.vehicleinventoryitemid and a.fromts = b.fromts
  where a.category = 'RMFlagAV') d on c.vehicleinventoryitemid = d.vehicleinventoryitemid
left join sls.deals_gross_by_month e on a.stock_number = e.control;


select * from uc_gross order by stock_number

select sale_group, sale_type, sum(front_gross) 
from uc_gross 
group by sale_group, sale_type

select * 
from uc_gross
order by sale_group, sale_type, stock_number


select stock_number, sale_type, front_gross, fi_Gross, recon_gross, total_gross
from uc_gross
order by stock_number


-- redo to breakout sales, cogs, dept

drop table if exists tmp_recon_ros;
create temp table tmp_recon_ros as
select control as stock_number, delivery_date, doc as ro, 
--   sum(case when c.account_type = 'sale' then amount else 0 end) as sales,
--   sum(case when c.account_type = 'cogs' then amount else 0 end) as cogs,
  sum(amount) as gross
from fin.fact_gl a
inner join fin.dim_account c on a.account_key = c.account_key
inner join fin.dim_journal d on a.journal_key = d.journal_key
inner join (
  select stock_number, delivery_date
  from sls.deals
  where store_code = 'ry1'
    and vehicle_type_code = 'u'
    and year_month between 201704 and 201706
  group by stock_number, delivery_date
  having sum(unit_count) > 0  ) e on a.control = e.stock_number
where a.post_status = 'Y'
  and
    case
      when c.account in ('124000','124001','124100','124101') then d.journal_code in ('SVI','POT')
      when c.account in (
        select gl_account
        from fin.fact_fs a
        inner join fin.dim_fs_account b on a.fs_account_key = b.fs_account_key
        inner join fin.dim_fs c on a.fs_key = c.fs_key
        inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
        inner join fin.dim_account e on b.gl_account = e.account
        where c.page = 16
          and e.department_code = 'uc'
          and e.account_type_code = '5'
          and d.store = 'ry1'
        group by gl_account) then d.journal_code in ('SVI','SCA','POT')
    end
group by control, doc, delivery_date
having sum(amount) <>  0;   

-- in the above results the ro is the doc, not control, to get sales/cogs need the control transactions
select a.*, b.control, b.doc, b.amount, c.account, c.account_type, c.department, d.journal_code,
   sum(amount) over (partition by stock_number, ro) as ro_total, c.description,
   sum(amount) over (partition by stock_number) as veh_total
from tmp_recon_ros a
inner join fin.fact_gl b on a.ro = b.control
inner join fin.dim_account c on b.account_key = c.account_key
inner join fin.dim_journal d on b.journal_key = d.journal_key
where b.post_status = 'Y'
order by a.stock_number, ro, department, account_type

asset accounts department general
124200 pt
124300 tires cogs
124400 gog sd or ql?
124500 bs inv paint
124501 bs hardware
124600 sublet
124700 sd inv wip
124701 ql inv wip
124702 cw inv wip
124703 bs wip labor
124704 sd wip labor
124723 ql wip labor
124724 cw wip labor (wash bay)
124727 cw wip labor  

select a.*, b.control, b.doc, b.amount, c.account, c.account_type, c.department, d.journal_code,
   sum(amount) over (partition by stock_number, ro) as ro_total, c.description,
   sum(amount) over (partition by stock_number) as veh_total
from tmp_recon_ros a
inner join fin.fact_gl b on a.ro = b.control
inner join fin.dim_account c on b.account_key = c.account_key
inner join fin.dim_journal d on b.journal_key = d.journal_key
where b.post_status = 'Y'
order by a.stock_number, ro, department, account_type

-- drop table if exists recon_breakdown;
-- create temp table recon_breakdown as
-- select stock_number, delivery_date, total_gross, 
--   coalesce(parts_sales, 0) as parts_sales, coalesce(parts_cogs,0) as parts_cogs, 
--     coalesce(parts_sales,0) + coalesce(parts_cogs,0) as parts_gross,
--   coalesce(service_sales, 0) as service_sales, coalesce(service_cogs,0) as service_cogs, 
--     coalesce(service_sales,0) + coalesce(service_cogs,0) as service_gross,
--   coalesce(bs_sales, 0) as bs_sales, coalesce(bs_cogs,0) as bs_cogs, 
--     coalesce(bs_sales, 0) + coalesce(bs_cogs, 0) as bs_gross,
--   coalesce(detail_sales, 0) as detail_sales, coalesce(detail_cogs,0) as detail_cogs, 
--     coalesce(detail_sales,0) + coalesce(detail_cogs) as detail_gross,
--   coalesce(parts_sales,0) + coalesce(parts_cogs,0) + coalesce(service_sales,0) + 
--     coalesce(service_cogs,0) + coalesce(detail_sales,0) + coalesce(detail_cogs,0) + 
--     coalesce(bs_sales,0) + coalesce(bs_cogs,0) as total
-- from (  
--   select a.stock_number, a.delivery_date, sum(amount) as total_gross,
--     sum(
--       case when department = 'parts' or (department = 'general' and account in ('124200','124300'))then
--         case when b.amount < 0 then amount else 0 end
--       end) as parts_sales,
--     sum(
--       case when department = 'parts' then
--         case when b.amount > 0 then amount else 0 end
--       end) as parts_cogs,
--     sum(
--       case when department = 'service' or (department = 'general' and account in ('124400','124700','124704'))then
--         case when b.amount < 0 then amount else 0 end
--       end) as service_sales,
--     sum(
--       case when department = 'service' then
--         case when b.amount > 0 then amount else 0 end
--       end) as service_cogs, 
--     sum(
--       case when department = 'body shop' or (department = 'general' and account in ('124500','124501','124703'))then
--         case when b.amount < 0 then amount else 0 end
--       end) as bs_sales,
--     sum(
--       case when department = 'body shop' then
--         case when b.amount > 0 then amount else 0 end
--       end) as bs_cogs,  
--     sum(
--       case when department = 'detail' or (department = 'general' and account in ('124702','124724','1247027'))then
--         case when b.amount < 0 then amount else 0 end
--       end) as detail_sales,
--     sum(
--       case when department = 'detail' then
--         case when b.amount > 0 then amount else 0 end
--       end) as detail_cogs,
--     sum(
--       case when department = 'general' and account = '124600' then
--         case when b.amount < 0 then amount else 0 end
--       end) as sublet_sales,     
--     sum(
--       case when department = 'general' and account = '124600' then
--         case when b.amount > 0 then amount else 0 end
--       end) as sublet_cogs
--   from tmp_recon_ros a
--   inner join fin.fact_gl b on a.ro = b.control
--   inner join fin.dim_account c on b.account_key = c.account_key
--   inner join fin.dim_journal d on b.journal_key = d.journal_key
--   where b.post_status = 'Y'
--   group by a.stock_number, a.delivery_date) x;

drop table if exists recon_breakdown;
create temp table recon_breakdown as
select stock_number, delivery_date, 
  coalesce(parts_sales, 0) as parts_sales, coalesce(parts_cogs,0) as parts_cogs, 
    coalesce(parts_sales,0) + coalesce(parts_cogs,0) as parts_gross,
  coalesce(service_sales, 0) as service_sales, coalesce(service_cogs,0) as service_cogs, 
    coalesce(service_sales,0) + coalesce(service_cogs,0) as service_gross,
  coalesce(bs_sales, 0) as bs_sales, coalesce(bs_cogs,0) as bs_cogs, 
    coalesce(bs_sales, 0) + coalesce(bs_cogs, 0) as bs_gross,
  coalesce(detail_sales, 0) as detail_sales, coalesce(detail_cogs,0) as detail_cogs, 
    coalesce(detail_sales,0) + coalesce(detail_cogs) as detail_gross,
  coalesce(parts_sales,0) + coalesce(service_sales,0) + coalesce(bs_sales,0) + coalesce(detail_sales,0) as total_sales,
  coalesce(parts_cogs,0) + coalesce(service_cogs,0) + coalesce(bs_sales,0) + coalesce(detail_cogs,0) as total_cogs,
  coalesce(parts_sales,0) + coalesce(parts_cogs,0) + coalesce(service_sales,0) + 
    coalesce(service_cogs,0) + coalesce(detail_sales,0) + coalesce(detail_cogs,0) + 
    coalesce(bs_sales,0) + coalesce(bs_cogs,0) as total_gross       
from (  
  select a.stock_number, a.delivery_date, sum(amount) as total_gross,
    sum(
      case when department = 'parts' then--or (department = 'general' and account in ('124200','124300'))then
        case when b.amount < 0 then amount else 0 end
      end) as parts_sales,
    sum(
      case when department = 'parts' then
        case when b.amount > 0 then amount else 0 end
      end) as parts_cogs,
    sum(
      case when department = 'service' then--or (department = 'general' and account in ('124400','124700','124704'))then
        case when b.amount < 0 then amount else 0 end
      end) as service_sales,
    sum(
      case when department = 'service' then
        case when b.amount > 0 then amount else 0 end
      end) as service_cogs, 
    sum(
      case when department = 'body shop' then--or (department = 'general' and account in ('124500','124501','124703'))then
        case when b.amount < 0 then amount else 0 end
      end) as bs_sales,
    sum(
      case when department = 'body shop' then
        case when b.amount > 0 then amount else 0 end
      end) as bs_cogs,  
    sum(
      case when department = 'detail' then--or (department = 'general' and account in ('124702','124724','124727'))then
        case when b.amount < 0 then amount else 0 end
      end) as detail_sales,
    sum(
      case when department = 'detail' then--or (department = 'general' and account in ('124702','124724','124727'))then
        case when b.amount > 0 then amount else 0 end
      end) as detail_cogs,
    sum(
      case when department = 'general' and account = '124600' then
        case when b.amount < 0 then amount else 0 end
      end) as sublet_sales,     
    sum(
      case when department = 'general' and account = '124600' then
        case when b.amount > 0 then amount else 0 end
      end) as sublet_cogs
  from tmp_recon_ros a
  inner join fin.fact_gl b on a.ro = b.control
  inner join fin.dim_account c on b.account_key = c.account_key
  inner join fin.dim_journal d on b.journal_key = d.journal_key
  where b.post_status = 'Y'
  group by a.stock_number, a.delivery_date) x;

select * from tmp_recon_ros where stock_number = '29524b'

select * from recon_breakdown where stock_number = '29524b'
-- 
-- select a.stock_number, a.delivery_date, account, department, journal_code, amount, description,
--   sum(amount) over (partition by department)
--   from tmp_recon_ros a
--   inner join fin.fact_gl b on a.ro = b.control
--   inner join fin.dim_account c on b.account_key = c.account_key
--   inner join fin.dim_journal d on b.journal_key = d.journal_key
--   where b.post_status = 'Y'
--   and stock_number = '29524b'
-- order by department, account  


select z.*, sum(amount) over (partition by department) as dept,
  sum(amount) over (partition by department, control) as dept_cont, 
  sum(amount) over (partition by control) as cont,
  sum(amount) over (partition by stock_number) as stk,
  sum(amount) over (partition by control, sign(amount)) as sign,
  sum(amount) over (partition by control, account_type) as cont_acct_type,
  sum(amount) over (partition by control, account) as cont_acct
from (
  select a.stock_number, a.delivery_date, account, account_type,
    case
      when c.account in ('124702','124724','124727') then 'Detail-General'
      when c.account in ('124500','124501','124703') then 'Body Shop-General'
      when c.account in ('124400','124700','124704') then 'Service-General'
      when c.account in ('124200','124300') then 'Parts-General'
      else department
    end as department, journal_code, amount, c.description as acct_desc, e.description as trans_desc, control, doc, ref
  from tmp_recon_ros a
  inner join fin.fact_gl b on a.ro = b.control
  inner join fin.dim_account c on b.account_key = c.account_key
  inner join fin.dim_journal d on b.journal_key = d.journal_key
  inner join fin.dim_gl_description e on b.gl_description_key = e.gl_description_key
  where b.post_status = 'Y'
--    and account_type in ('sale','cogs','expense')) z
    and stock_number = '29524b' and control = '16272040' and account_type in ('sale','cogs','expense')) z
order by control, account --department, sign(amount)

select * from fin.dim_fs_account where gl_account = '124703'

select * from recon_breakdown where stock_number = '30314b'

select * from tmp_recon_ros where stock_number = '30314b'

16272040:
select 150+20+36.1+127.1+15.1+8  -- net: 356.3
select 25+10+12.5+25+13.9+4.2 -- cost: 90.6
select 125+33.68+102.1+1.2+.83+2.83  -- gross: 265.64

sale: -356.46
expense: -35.65
asset: -90.82
cogs: 90.82

select a.stock_number, a.delivery_date, account, account_type, department
  journal_code, amount, c.description as acct_desc, e.description as trans_desc, control, doc, ref

select a.stock_number, a.delivery_date, string_agg(distinct control, ','),
  sum(case when department = 'service' and account_type in ('sale','expense') then amount else 0 end) as mech_sales,
  sum(case when department = 'body shop' and account_type in ('sale','expense') then amount else 0 end) as body_sales,
  sum(case when department = 'detail' and account_type in ('sale','expense') then amount else 0 end) as detail_sales,
  sum(case when department = 'parts' and account_type in ('sale','expense') then amount else 0 end) as parts_sales,
  sum(case when account_type in ('sale','expense') then amount else 0 end) as total_sales
from tmp_recon_ros a
inner join fin.fact_gl b on a.ro = b.control
inner join fin.dim_account c on b.account_key = c.account_key
inner join fin.dim_journal d on b.journal_key = d.journal_key
inner join fin.dim_gl_description e on b.gl_description_key = e.gl_description_key
where b.post_status = 'Y'
  and account_type in ('sale','cogs','expense')
  and stock_number = '29524b' 
--   and control = '16272040'
group by a.stock_number, a.delivery_date  

-- 7/15/17 everything moved to ~/Desktop/sql/ucinv_recon_2.sql
-- very comfortable including sales,cogs,expense accounts to represent sales (for comparing to estimates, this is
--   the total a customer would see on an ro) and gross: (sales + expense) - cogs
    
----------------------------------------------------------------------------  ---------------------------------------------------------------------------- 
----------------------------------------------------------------------------  ---------------------------------------------------------------------------- 
----------------------------------------------------------------------------  ---------------------------------------------------------------------------- 

select a.*, abs(total_gross - total)
from recon_breakdown a
where total_Gross <> total

select a.*, abs(total_gross - total), x.walk_recon, xx.eval_recon::integer
from recon_breakdown a
left join (
  select a.vehicleinventoryitemid, stocknumber, b.fromts::date, sum(totalpartsamount + laboramount) as walk_recon
  from ads.ext_vehicle_recon_items a
  inner join ads.ext_vehicle_inventory_items b on a.vehicleinventoryitemid = b.vehicleinventoryitemid
  where exists (
    select 1
    from ads.ext_authorized_recon_items
    where vehiclereconitemid = a.vehiclereconitemid)
    group by a.vehicleinventoryitemid, stocknumber, b.fromts::date) x on a.stock_number = x.stocknumber  
left join (
  select vehicleinventoryitemid, sum(reconmechanicalamount+reconbodyamount+reconappearanceamount
    +recontireamount+reconglassamount+reconinspectionamount) as eval_recon
  from ads.ext_vehicle_evaluations
  group by vehicleinventoryitemid) xx on x.vehicleinventoryitemid = xx.vehicleinventoryitemid
order by stock_number

-- oops shouldn't i be comparing sales to walk/eval, not gross?

select a.stock_number, eval_date, a.delivery_date, -1*coalesce(a.parts_sales,0)::integer as parts, 
  -1*coalesce(a.service_sales,0)::integer as service, 
  -1*coalesce(a.bs_sales,0)::integer as body, -1*coalesce(a.detail_sales,0)::integer as detail, 
  -1*(coalesce(a.parts_sales,0)+coalesce(a.service_sales,0)+coalesce(a.bs_sales,0)+coalesce(a.detail_sales,0))::integer as total_arkona, x.walk_recon, xx.eval_recon::integer
from recon_breakdown a
left join (
  select a.vehicleinventoryitemid, stocknumber, b.fromts::date, sum(totalpartsamount + laboramount) as walk_recon
  from ads.ext_vehicle_recon_items a
  inner join ads.ext_vehicle_inventory_items b on a.vehicleinventoryitemid = b.vehicleinventoryitemid
  where exists (
    select 1
    from ads.ext_authorized_recon_items
    where vehiclereconitemid = a.vehiclereconitemid)
    group by a.vehicleinventoryitemid, stocknumber, b.fromts::date) x on a.stock_number = x.stocknumber  
left join (
  select vehicleinventoryitemid, vehicleevaluationts::date as eval_date, sum(coalesce(reconmechanicalamount,0)+coalesce(reconbodyamount,0)+coalesce(reconappearanceamount,0)
    +coalesce(recontireamount,0)+coalesce(reconglassamount,0)+coalesce(reconinspectionamount,0)) as eval_recon
  from ads.ext_vehicle_evaluations
  group by vehicleinventoryitemid, vehicleevaluationts::date) xx on x.vehicleinventoryitemid = xx.vehicleinventoryitemid
order by eval_date


select a.stock_number, eval_date, delivery_date, -1*total_gross::integer as total_arkona_recon_gross,
  -1*(coalesce(a.parts_sales,0)+coalesce(a.service_sales,0)+coalesce(a.bs_sales,0)+coalesce(a.detail_sales,0))::integer as total_arkona_recon_sales, 
  x.walk_recon, xx.eval_recon::integer
from recon_breakdown a
left join (
  select a.vehicleinventoryitemid, stocknumber, b.fromts::date, sum(totalpartsamount + laboramount) as walk_recon
  from ads.ext_vehicle_recon_items a
  inner join ads.ext_vehicle_inventory_items b on a.vehicleinventoryitemid = b.vehicleinventoryitemid
  where exists (
    select 1
    from ads.ext_authorized_recon_items
    where vehiclereconitemid = a.vehiclereconitemid)
    group by a.vehicleinventoryitemid, stocknumber, b.fromts::date) x on a.stock_number = x.stocknumber  
left join (
  select vehicleinventoryitemid, vehicleevaluationts::date as eval_date, sum(coalesce(reconmechanicalamount,0)+coalesce(reconbodyamount,0)+coalesce(reconappearanceamount,0)
    +coalesce(recontireamount,0)+coalesce(reconglassamount,0)+coalesce(reconinspectionamount,0)) as eval_recon
  from ads.ext_vehicle_evaluations
  group by vehicleinventoryitemid, vehicleevaluationts::date) xx on x.vehicleinventoryitemid = xx.vehicleinventoryitemid
  
order by eval_date


-- 7/13
/*
for the 1st xcl thinking eval_date, del_date, dept_sales, walk_recon, eval_recon

*/

select * from recon_breakdown where stock_number = '29524b'

select distinct typ
from ads.ext_vehicle_recon_items
order by typ

drop table if exists walk_recon;
create temp table walk_recon as
select a.vehicleinventoryitemid, b.stocknumber, 
sum(case when a.typ like 'A%' or a.typ like 'P%' then totalpartsamount + laboramount else 0 end) as walk_detail,
sum(case when a.typ like 'B%' then a.totalpartsamount + a.laboramount else 0 end) as walk_body,
sum(case when a.typ like 'M%' then a.totalpartsamount + a.laboramount else 0 end) as walk_mech,
sum(a.totalpartsamount + a.laboramount) as walk_total
from ads.ext_vehicle_recon_items a
inner join ads.ext_vehicle_inventory_items b on a.vehicleinventoryitemid = b.vehicleinventoryitemid
where exists (
  select 1
  from ads.ext_authorized_recon_items
  where vehiclereconitemid = a.vehiclereconitemid)
group by a.vehicleinventoryitemid, b.stocknumber;    

drop table if exists eval_recon;
create temp table eval_recon as
select vehicleinventoryitemid, vehicleevaluationts::date as eval_date, sum(coalesce(reconmechanicalamount,0)+coalesce(reconbodyamount,0)+coalesce(reconappearanceamount,0)
  +coalesce(recontireamount,0)+coalesce(reconglassamount,0)+coalesce(reconinspectionamount,0)) as eval_recon
from ads.ext_vehicle_evaluations
group by vehicleinventoryitemid, vehicleevaluationts::date;


select c.stock_number, d.eval_date, c.delivery_date, d.eval_recon,
  d.walk_mech, d.walk_body, d.walk_detail, d.walk_total,
  c.parts_sales as ark_parts, c.service_sales as ark_mech, c.bs_sales as ark_body, c.detail_sales as ark_detail,
  c.parts_sales+c.service_sales+c.bs_sales+c.detail_sales as ark_total
from recon_breakdown c  
left join (
  select a.stocknumber, b.eval_date, b.eval_recon::integer, a.walk_detail, a.walk_body, a.walk_mech, walk_total
  from walk_recon a
  inner join eval_recon b on a.vehicleinventoryitemid = b.vehicleinventoryitemid) d on c.stock_number = d.stocknumber
where c.stock_number = '29524b'




select a.control, a.doc, a.amount, c.account, c.department, c.description, account_type, d.journal_code
-- select sum(amount)
from fin.fact_gl a
inner join fin.dim_journal b on a.journal_key = b.journal_key
inner join fin.dim_account c on a.account_key = c.account_key
inner join fin.dim_journal d on a.journal_key = d.journal_key
where b.journal_code = 'SVI'
  and a.control = '16272434'


select * from recon_breakdown where stock_number = '29524b'

select * from tmp_recon_ros where stock_number = '29524b'

-- 7/14/  uh oh, i am in mindfuck land  