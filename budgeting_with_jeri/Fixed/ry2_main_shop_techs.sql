﻿-- created from E:\DDS2\Scripts\reports\Jeri\annual_techs\techs\techs_oct_15_sep_16_honda_mainshop.sql
-- converted to pg
drop table if exists days cascade;
create temp table days as
select year_month, the_date, date_key,
  case 
    when weekday = true and holiday = false then true
    else false
  end as work_day
from dds.dim_date
where year_month between 201610 and 201709;
create unique index on days(the_date);
create unique index on days(date_key);
create index on days(year_month);



drop table if exists techs cascade;
create temp table techs as
SELECT techkey, technumber, employeenumber, techkeyfromdate, techkeythrudate, 
  laborcost
FROM ads.ext_dim_tech a
WHERE employeenumber <> 'NA'
  AND storecode = 'ry2'
  AND flagdept = 'service'
  AND EXISTS (
    SELECT 1
    FROM ads.ext_fact_repair_order m
    INNER JOIN dds.dim_date n on m.flagdatekey = n.date_key
      AND n.year_month BETWEEN 201610 AND 201709 -----------------------------------------------------------
    WHERE storecode = 'ry2'
      AND techkey = a.techkey) 
order by a.techkey;
create unique index on techs(techkey);
create index on techs(techkeyfromdate);
create index on techs(techkeythrudate);     
create index on techs(employeenumber); 

-- postgress can't do a full join on between
drop table if exists tech_days cascade;
create temp table tech_days as
SELECT a.*, b.*, c.lastname, c.firstname, c.employeekey, c.payrollclass, 
  CASE c.payrollclass
    WHEN 'Hourly' THEN c.hourlyrate::numeric(6,2)
    ELSE round(coalesce(e.previoushourlyrate, 0), 2) 
  end AS pto_rate
from techs a
inner join days b on b.the_date between a.techkeyfromdate and a.techkeythrudate
LEFT JOIN ads.ext_edw_employee_dim c on a.employeenumber = c.employeenumber
  and b.the_date BETWEEN c.employeekeyfromdate AND c.employeekeythrudate
LEFT JOIN ads.ext_tptechs d on a.employeenumber = d.employeenumber
  AND b.the_date BETWEEN d.fromdate AND d.thrudate
LEFT JOIN ads.ext_tptechvalues e on d.techkey = e.techkey
  AND b.the_date BETWEEN e.fromdate AND e.thrudate
order by b.date_key, a.techkey; 


drop table if exists flag_hours cascade;
create temp table flag_hours as
SELECT x.techkey, x.flagdatekey, x.flag_hours + coalesce(y.ptlhrs, 0) AS flag_hours
FROM (
  SELECT a.techkey, a.flagdatekey, sum(a.flaghours) AS flag_hours
  FROM ads.ext_fact_repair_order a
  INNER JOIN days b on a.flagdatekey = b.date_key
  WHERE a.flaghours <> 0
  group by a.techkey, a.flagdatekey) x
LEFT JOIN (
  SELECT c.date_key, b.techkey, round(sum(a.labor_hours), 2) AS ptlhrs
  FROM arkona.ext_sdpxtim a
  INNER JOIN ads.ext_dim_tech b on a.technician_id = b.technumber
  INNER JOIN dds.dim_date c on (select arkona.db2_integer_to_date_long(a.trans_date)) = c.the_date
  GROUP BY c.date_key, b.techkey) y on x.techkey = y.techkey AND x.flagdatekey = y.date_key;  

drop table if exists clock_hours cascade;
create temp table clock_hours as
SELECT a.datekey, c.employeenumber, c.hourlyrate, c.payrollclass,
  SUM(a.clockhours) AS clock_hours, 
  SUM(a.vacationhours + a.ptohours) AS pto_hours, 
  SUM(a.holidayhours) AS holiday_hours
FROM ads.ext_edw_clock_hours_fact a
INNER JOIN days b on a.datekey = b.date_key
INNER JOIN ads.ext_edw_employee_dim c on a.employeekey = c.employeekey
GROUP BY a.datekey, c.employeenumber, c.hourlyrate, c.payrollclass
HAVING SUM(a.clockhours + a.vacationhours + a.ptohours) > 0;

-- this looks LIKE the spreadsheet that was sent to jeri IN Oct 2016
select *
from (
  SELECT a.year_month, a.lastname, a.firstname, a.payrollclass, max(a.laborcost) AS cost,   
    coalesce(SUM(flag_hours), 0) AS flag_hours, SUM(clock_hours) AS clock_hours,
    SUM(pto_hours + holiday_hours) AS pto_hours,
    SUM(pto_hours * pto_rate)::numeric(6,2) AS pto_pay,
    coalesce(round(SUM(pto_hours * pto_rate)/SUM(flag_hours), 2), 0) AS pto_per_flag,
    round(sum(clock_hours)/SUM(CASE WHEN work_day = true then 1 ELSE 0 END), 2) AS avg_clock_day,
    coalesce(
      CASE 
        WHEN SUM(clock_hours) = 0 THEN 0
        ELSE round(100 * SUM(flag_hours)/SUM(clock_hours), 2) 
      END, 0) AS proficiency
  FROM tech_days a 
  LEFT JOIN flag_hours b on a.techkey = b.techkey
    AND a.date_key = b.flagdatekey
  LEFT JOIN clock_hours c on a.employeenumber = c.employeenumber 
    AND a.date_key = c.datekey
  GROUP BY a.year_month, a.lastname, a.firstname, a.payrollclass) x
where clock_hours is not null  
ORDER BY lastname, year_month



