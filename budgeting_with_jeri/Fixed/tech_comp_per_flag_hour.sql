﻿-- from E:\DDS2\Scripts\reports\Jeri\annual_techs\techs_oct_15_sep_16_avg_comp_flag_hour.sql
-- converted to pg

drop table if exists days;
create temp table days as
SELECT biweekly_pay_period_start_date AS from_date, biweekly_pay_period_end_date AS thru_date,
	(SELECT datekey FROM dds.day WHERE thedate = a.biweekly_pay_period_start_date) AS from_date_key,
	(SELECT datekey FROM dds.day WHERE thedate = a.biweekly_pay_period_end_date) AS thru_date_key,
    (SELECT mmdd FROM dds.day WHERE thedate = a.biweekly_pay_period_start_date)::text
	|| ' -> '
	|| (SELECT mmdd FROM dds.day WHERE thedate = a.biweekly_pay_period_end_date)::text
-- INTO #days	
FROM (
  SELECT biweekly_pay_period_start_date, biweekly_pay_period_end_date
  FROM dds.dim_date a4
  WHERE the_date BETWEEN '09/04/2016' AND '09/30/2017'
  GROUP BY biweekly_pay_period_start_date, biweekly_pay_period_end_date) a
order by from_date; 

-- techs  
DROP TABLE if exists techs;
create temp table techs as
SELECT a.storecode, a.flagdeptcode, a.techkey, a.technumber, a.employeenumber, 
  b.lastname, b.firstname
FROM ads.ext_dim_tech a
LEFT JOIN ads.ext_edw_employee_dim b on a.employeenumber = b.employeenumber
  AND b.currentrow = true
WHERE a.employeenumber <> 'NA'
  AND a.flagdeptcode IN ('am','bs','mr')
  AND EXISTS (
    SELECT 1
    FROM ads.ext_fact_repair_order m
    INNER JOIN dds.dim_date n on m.flagdatekey = n.date_key
      AND n.the_date BETWEEN '09/04/2016' AND '09/30/2017'
    WHERE techkey = a.techkey);


drop table if exists flag_hours;
create temp table flag_hours as
SELECT d.from_date, d.thru_date, SUM(c.flaghours) AS flaghours,
  c.storecode, c.employeenumber, c.flagdeptcode, c.firstname, c.lastname
FROM (	
  SELECT b.the_date, aa.employeenumber, a.flaghours,
    aa.storecode, aa.flagdeptcode, 
    aa.firstname, aa.lastname
  FROM ads.ext_fact_repair_order a
  INNER JOIN techs aa on a.techkey = aa.techkey
  INNER JOIN dds.dim_date b on a.flagdatekey = b.date_key
    AND b.the_date BETWEEN (SELECT MIN(from_date) FROM days) 
      AND (SELECT MAX(thru_date) FROM days)) c
LEFT JOIN days d on c.the_date BETWEEN d.from_date AND d.thru_date	  
GROUP BY d.from_date, d.thru_date, c.storecode, c.employeenumber, 
  c.flagdeptcode, c.firstname, c.lastname;


-- aha z_tech_gross appears to come from aqt payroll - tech gross per pay period.sql
drop table if exists gross_pay;
create temp table gross_pay as
select employee_, total_gross_pay,  
  (select arkona.db2_integer_to_date_long(payroll_start_date)) as from_date,
  (select arkona.db2_integer_to_date_long(payroll_end_date)) as thru_date
-- select count(*)  
from arkona.ext_pyptbdta a
inner join arkona.ext_pyhshdta b on a.company_number = b.company_number and a.payroll_cen_year = b.payroll_cen_year and a.payroll_run_number = b.payroll_run_number
inner join days c on  (select arkona.db2_integer_to_date_long(a.payroll_start_date)) = c.from_date
inner join techs d on b.employee_ = d.employeenumber
order by employee_, payroll_start_date;


select a.from_date, a.thru_date, a.storecode, a.flagdeptcode, a.lastname, a.firstname, 
  a.flaghours::integer, b.total_gross_pay::integer, 
  case
    when a.flaghours::integer = 0 then 0
    else (b.total_gross_pay/a.flaghours)::integer
  end as gross_per_flag_hours        
-- select *  
from flag_hours a
left join gross_pay b on a.employeenumber = b.employee_
  and a.from_Date = b.from_date
where b.total_gross_pay is not null
order by storecode, flagdeptcode, a.from_date, a.lastname;


  
