﻿select e.the_date, b.account, c.journal_code, d.description, a.control, a.doc, a.ref, a.amount
from fin.fact_gl a
inner join fin.dim_account b on a.account_key = b.account_key
inner join fin.dim_journal c on a.journal_key = c.journal_key
inner join fin.dim_gl_description d on a.gl_description_key = d.gl_description_key
inner join dds.dim_date e on a.date_key = e.date_key
where a.post_status = 'Y'
  and a.control = 'g33482'
order by amount

select b.account, c.journal_code, d.description, a.*
from fin.fact_gl a
inner join fin.dim_account b on a.account_key = b.account_key
inner join fin.dim_journal c on a.journal_key = c.journal_key
inner join fin.dim_gl_description d on a.gl_description_key = d.gl_description_key



select e.the_date, b.account, c.journal_code, d.description, a.control, a.doc, a.ref, a.amount
from fin.fact_gl a
inner join fin.dim_account b on a.account_key = b.account_key
inner join fin.dim_journal c on a.journal_key = c.journal_key
inner join fin.dim_gl_description d on a.gl_description_key = d.gl_description_key
inner join dds.dim_date e on a.date_key = e.date_key
where a.post_status = 'Y'
  and a.control = '108063'


select stock_number, vin, vehicle, tags
from vauto.ext_mike_corp_report  
where tags like '%aftermarket%'


select inpmast_stock_number
from arkona.xfm_inpmast a
where a.current_row = true
  and status = 'I'
  and type_n_u = 'N'
  and inpmast_stock_number not like 'H%'


select e.the_date, b.account, c.journal_code, d.description, a.control, a.doc, a.ref, a.amount
select a.control, a.amount
from fin.fact_gl a
inner join fin.dim_account b on a.account_key = b.account_key
inner join fin.dim_journal c on a.journal_key = c.journal_key
inner join fin.dim_gl_description d on a.gl_description_key = d.gl_description_key
inner join dds.dim_date e on a.date_key = e.date_key
inner join (
  select inpmast_stock_number
  from arkona.xfm_inpmast a
  where a.current_row = true
    and status = 'I'
    and type_n_u = 'N'
    and inpmast_stock_number not like 'H%') f on a.control = f.inpmast_stock_number
where a.post_status = 'Y'  
  and amount between 1000 and 6000
group by a.control, a.amount 
having count(*) > 1
order by a.control