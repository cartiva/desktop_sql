﻿select a.store, a.capped_date, a.deal_status, a.deal_type, a.sale_type, 
  a.sale_group, a.stock_number, a.bopmast_id, a.vin, a.leasing_source, a.lending_source,
  b.lendor_name, c.lessor_name, aa.bopmast_search_name, d.buyer, e.make, e.model


select a.capped_date, a.stock_number, coalesce(d.buyer, aa.bopmast_search_name) as name, e.make, e.model 
from sls.ext_bopmast_partial a
left join arkona.ext_bopmast aa on a.bopmast_id = aa.record_key and a.store = aa.bopmast_company_number
left join arkona.ext_boplsrc b on a.store = b.company_number
  and a.lending_source = b.key
left join arkona.ext_bopssrc c on a.store = c.company_number
  and a.leasing_source = c.key  
left join sls.deals_by_month d on a.bopmast_id = d.bopmast_id
left join arkona.xfm_inpmast e on a.vin = e.inpmast_vin
  and e.current_row = true
where a.store = 'ry2'
  and a.vehicle_type = 'U'
  and lendor_name is not null
  and b.lendor_name like 'san%'
  and a.capped_date >= '01/01/2015'
order by capped_date


select * from sls.deals_by_month where stock_number = 'H10079G'


select * from sls.ext_bopmast_partial where vin = '1FMFU16547LA70547'

select * from arkona.ext_bopmast where record_key = 9211