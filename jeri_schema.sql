﻿select b.the_date, c.account, c.description as account_description, d.journal_code, a.amount, e.description as trans_description, 
  account_type_code, account_type, department
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
inner join fin.dim_account c on a.account_key = c.account_key
inner join fin.dim_journal d on a.journal_key = d.journal_key
inner join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key
inner join fin.dim_fs f on a.fs_key = f.fs_key
where a.post_status = 'Y'
  and control = 'h9734'
  and journal_code = 'VSN'
  and account_type = 'sale'
  and department = 'new vehicle' 





drop schema jeri cascade;
create schema jeri;
comment on schema jeri is 'Jeri''s tableau playground';

drop materialized view if exists jeri.fs_page_2 cascade;
create MATERIALIZED view jeri.fs_page_2 as
select *
from fin.page_2
where store_code in ('ry1','ry2')
  and the_year > 2011;
create unique index on jeri.fs_page_2(store_code, year_month, line)

-- refresh materialized view concurrently jeri.fs;

drop materialized view if exists jeri.fs cascade;
create materialized view jeri.fs as
select year_month, page, line, col, line_label, gm_account, gl_account, store, area, department, sub_Department, amount
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
where year_month > 201200
  and store in ('ry1','ry2');
create unique index on jeri.fs(year_month,page,line,col,gl_account);  
create index on jeri.fs(year_month);
create index on jeri.fs(store);
create index on jeri.fs(area);
create index on jeri.fs(department);
create index on jeri.fs(sub_department);
create index on jeri.fs(page);
create index on jeri.fs(line);
create index on jeri.fs(gm_account);
create index on jeri.fs(gl_account);

-- refresh materialized view concurrently jeri.gl; 10 minutes
-- refresh materialized view jeri.gl; 16 minutes

drop materialized view if exists jeri.gl cascade;
create materialized view jeri.gl as
select c.store_code as store, a.trans, a.seq, b.year_month, b.the_date, a.control, a.doc, a.ref, c.account, 
  c.description as account_description, d.journal_code as journal, a.amount, e.description as trans_description, 
  account_type, department
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
inner join fin.dim_account c on a.account_key = c.account_key
  and b.the_date between c.row_from_date and c.row_thru_date
inner join fin.dim_journal d on a.journal_key = d.journal_key
inner join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key
where a.post_status = 'Y'
  and b.year_month > 201200;
create unique index on jeri.gl(trans, seq);  
create index on jeri.gl(control);
create index on jeri.gl(store);
create index on jeri.gl(account);
create index on jeri.gl(journal);
create index on jeri.gl(account_type);
create index on jeri.gl(department);


select a.line_label, a.line, a.col, b.account, b.account_description, 
  b.trans_description, b.account_type, b.control, b.amount,
  sum(b.amount) over (partition by b.control, b.account_type),
  sum(b.amount) over (partition by a.line)
from jeri.fs a
inner join jeri.gl b on a.year_month = b.year_month
  and a.store = b.store
  and a.gl_account = b.account
where a.store = 'ry1'
  and a.year_month = 201708
  and a.page = 8
order by a.line, b.control,  b.account_type


-- 9/21/17  jeri would like to have the tableau data labeled in plain speak: new cars, used cars, make, model, gross, 
not yet sure what this will look like, but is likely to be driven by a drill down path focused around count/pvr

------------ repair orders --------------------------

select a.storecode, a.ro, a.line, a.flaghours, b.the_date as open_date, 
  bb.the_date as close_date, bbb.the_date as final_close_date, c.fullname, 
  i.vin, i.make, i.model, i.modelyear, 
  f.paymenttype, g.servicetype, h.writernumber, h.name as writer,
  d.opcode, d.description as opcode_desc, e.opcode as corcode, e.description as corcode_desc
from ads.ext_fact_repair_order a
inner join dds.dim_date b on a.opendatekey = b.date_key
inner join dds.dim_date bb on a.closedatekey = bb.date_key
inner join dds.dim_date bbb on a.finalclosedatekey = bbb.date_key
inner join ads.ext_dim_customer c on a.customerkey = c.customerkey
inner join ads.ext_dim_opcode d on a.opcodekey = d.opcodekey
inner join ads.ext_dim_opcode e on a.corcodekey = e.opcodekey
inner join ads.ext_dim_payment_type f on a.paymenttypekey = f.paymenttypekey
inner join ads.ext_dim_service_type g on a.servicetypekey = g.servicetypekey
inner join ads.ext_dim_service_writer h on a.servicewriterkey = h.servicewriterkey
inner join ads.ext_dim_vehicle i on a.vehiclekey = i.vehiclekey
order by b.the_date desc 
limit 500  



select store, area, department, sub_department
from jeri.fs
group by store, area, department, sub_department
order by store, area, department


