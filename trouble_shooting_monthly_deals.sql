﻿                    select store_code, bopmast_id, stock_number, year_month, sum(unit_count)::integer as unit_count
                    from sls.deals
                    group by store_code, bopmast_id, stock_number, year_month
                    having sum(unit_count) <> 0
order by stock_number                    


select *
from sls.deals 
where stock_number in('30062b','30342x','30726b','31284x','30510b') order by stock_number, seq


select *
from sls.deals_by_month_tmp
where stock_number in('30062b','30342x','30726b','31284x','30510b')

select * from sls.xfm_Deals where stock_number = '30062b'

select *
from sls.deals_by_month a
where year_month = 201706
  and not exists (
    select 1
    from sls.deals_by_month_tmp
    where stock_number = a.stock_number
      and year_month = 201706)


the only one is 30062B
select *
from sls.deals
where notes = 'type_2_b'      
  and year_month <> (extract(year from gl_date) * 100) + extract(month from gl_date)


select *
from sls.deals a
where exists (
  select 1
  from sls.deals
  where stock_number = a.stock_number
    and notes = 'type_2_b')
order by stock_number, seq    

select *
from sls.deals
where stock_number = '30062b'

select *
from sls.xfm_deals
where stock_number = '30062b'

update sls.deals
set year_month = 201706
where stock_number = '30062B'
  and seq = 2

select md5(z::text) as hash
from (
  select store_code, bopmast_id, deal_status,
    deal_type, sale_type, sale_group, vehicle_type, stock_number, vin,
    odometer_at_sale, buyer_bopname_id, cobuyer_bopname_id,
    primary_sc, secondary_sc, fi_manager, origination_date, approved_date,
    capped_date, delivery_date, gap, service_contract,total_care,
    coalesce(gl_date, '12/31/9999'), coalesce(unit_count, 0)
  from sls.ext_deals x
  left join sls.ext_accounting_deals y on x.stock_number = y.control
    and y.gl_date = (
      select max(gl_date)
      from sls.ext_accounting_deals
      where control = y.control)
  where x.store_code = a.store_code
    and x.bopmast_id = a.bopmast_id) z


-- 6/26/17
select *
from (
  select stock_number, sum(unit_count) as unit_count
  from sls.deals
  where year_month = 201706  
  group by stock_number) a
full outer join (
  select stock_number, unit_count
  from sls.deals_by_month
  where year_month = 201706) b on a.stock_number = b.stock_number 
where a.unit_count <> b.unit_count


select *
from (
  select bopmast_id, max(stock_number), sum(unit_count) as unit_count
  from sls.deals
  where year_month = 201706  
  group by bopmast_id) a
full outer join (
  select bopmast_id, unit_count
  from sls.deals_by_month
  where year_month = 201706) b on a.bopmast_id = b.bopmast_id 
where a.unit_count <> b.unit_count

  
-- 6/27

as far as deals_by_month, leaning toward deleting rows for current month and 
regenerating current month deals daily rather than trying to update from tmp

-- class XfmDealsNewRows, these are all the rows that get excluded based on null gl_date, 
-- including 31137 which was an accounting error

                    select a.run_date, a.store_code, a.bopmast_id, a.deal_status,
                      a.deal_type, a.sale_type, a.sale_group, a.vehicle_type, a.stock_number, a.vin,
                      a.odometer_at_sale, a.buyer_bopname_id, a.cobuyer_bopname_id,
                      a.primary_sc, a.secondary_sc, a.fi_manager, a.origination_date, a.approved_date,
                      a.capped_date, a.delivery_date, a.gap, a.service_contract, a.total_care,
                      'new', 1,
                      coalesce(b.gl_date, '12/31/9999'), coalesce(b.unit_count, 0),
                      ( -- generate the hash
                        select md5(z::text) as hash
                        from (
                          select store_code, bopmast_id, deal_status,
                            deal_type, sale_type, sale_group, vehicle_type, stock_number, vin,
                            odometer_at_sale, buyer_bopname_id, cobuyer_bopname_id,
                            primary_sc, secondary_sc, fi_manager, origination_date, approved_date,
                            capped_date, delivery_date, gap, service_contract,total_care,
                            coalesce(gl_date, '12/31/9999'), coalesce(unit_count, 0)
                          from sls.ext_deals x
                          left join sls.ext_accounting_deals y on x.stock_number = y.control
                            and y.gl_date = (
                              select max(gl_date)
                              from sls.ext_accounting_deals
                              where control = y.control)
                          where x.store_code = a.store_code
                            and x.bopmast_id = a.bopmast_id) z)
                    from sls.ext_deals a
                    left join sls.ext_accounting_deals b on a.stock_number = b.control
                      and b.gl_date = (
                        select max(gl_date)
                        from sls.ext_accounting_deals
                        where control = b.control)
                    where coalesce(gl_date, '12/31/9999') >= '02/01/2017'
                      and not exists (
                        select *
                        from sls.xfm_deals
                        where store_code = a.store_code
                          and bopmast_id = a.bopmast_id) order by stock_number;


select * from sls.deals where stock_number = '31137'

select * from sls.xfm_deals where stock_number = '31137'

select * from sls.ext_deals where stock_number = 'h10275p'

select * from sls.ext_bopmast_partial where stock_number = 'h10275p'


alter table sls.deals_by_month_tmp
rename to z_unused_deals_by_month_tmp

select * from sls.deals where stock_number = 'h10275p'

  