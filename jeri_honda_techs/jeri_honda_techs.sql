﻿/*
Will you please pull the flag and clock hours for the main shop techs at Honda 
for the last 6 months along with the payroll amounts (amount hitting 224704) 
by control number for same time period? 

Tech level per month.

*/

drop table if exists pay;
create temp table pay as
select b.the_Date, a.control, a.amount as amount
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
  and b.the_date between'09/01/2017' and current_date
inner join fin.dim_account c on a.account_key = c.account_key
  and c.account = '224704'  
where post_status = 'Y';

drop table if exists employees;
create temp table employees as
select a.control as employee_number, b.employee_name
from (
  select control
  from fin.fact_gl a
  inner join dds.dim_date b on a.date_key = b.date_key
    and b.the_date between'09/01/2017' and current_date
  inner join fin.dim_account c on a.account_key = c.account_key
    and c.account = '224704'  
  left join ads.ext_dim_tech d on a.control = d.employeenumber
  where post_status = 'Y' 
    and control not like 'H%'
  group by control) a
left join arkona.xfm_pymast b on a.control = b.pymast_employee_number;

drop table if exists clock_hours;
create temp table clock_hours as
select b.employee_number, b.the_date, b.clock_hours
from employees a
left join arkona.xfm_pypclockin b on a.employee_number = b.employee_number
where b.the_date between '09/01/2017' and current_date;

drop table if exists techs;
create temp table techs as
select employeenumber as employee_number, techkey
from employees a
inner join ads.ext_dim_tech b on a.employee_number = b.employeenumber;

drop table if exists flag_hours;
create temp table flag_hours as 
select b.the_date, a.flaghours, c.employee_number
from ads.ext_fact_repair_order a
inner join dds.dim_date b on a.closedatekey = b.date_key
  and b.the_date between '09/01/2017' and current_date
inner join techs c on a.techkey = c.techkey; 

select * from dds.dim_date limit 10

select aa.employee_name, aa.employee_number, 
  (select distinct month_name || ' ' || the_year::text from dds.dim_date where year_month = aa.year_month ),
  aa.paid, bb.flag_hours, cc.clock_hours, aa.year_month
from (
  select a.employee_number, a.employee_name, c.year_month, sum(b.amount) as paid
  from employees a
  inner join pay b on a.employee_number = b.control
  inner join dds.dim_date c on b.the_date = c.the_date
  group by a.employee_number, a.employee_name, c.year_month) aa
left join (-- flag hours
  select a.employee_number, a.employee_name, c.year_month, sum(b.flaghours) as flag_hours
  from employees a
  inner join flag_hours b on a.employee_number = b.employee_number
  inner join dds.dim_date c on b.the_date = c.the_date
  group by a.employee_number, a.employee_name, c.year_month) bb on aa.employee_number = bb.employee_number
    and aa.year_month = bb.year_month
left join (-- clock hours
  select a.employee_number, a.employee_name, c.year_month, sum(b.clock_hours) as clock_hours
  from employees a
  inner join clock_hours b on a.employee_number = b.employee_number
  inner join dds.dim_date c on b.the_date = c.the_date
  group by a.employee_number, a.employee_name, c.year_month) cc on aa.employee_number = cc.employee_number
    and aa.year_month = cc.year_month
order by employee_name, year_month    
    