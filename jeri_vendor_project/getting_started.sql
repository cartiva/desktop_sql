﻿select b.the_date, c.account, a.*, d.description
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
inner join fin.dim_account c on a.account_key = c.account_key
inner join fin.dim_gl_description d on a.gl_description_key = d.gl_description_key
where a.post_status = 'Y'
  and b.the_date between '01/01/2018' and current_date
  and c.account in ('120300','220200')
  and a.control = '10307'
order by a.control



select control, 
  sum(abs(amount)) filter (where account = '120300') as ry1_amount,
  sum(abs(amount)) filter (where account = '220200') as ry2_amount
from (  
select b.the_date, c.account, a.*
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
inner join fin.dim_account c on a.account_key = c.account_key
where a.post_status = 'Y'
  and b.the_date between '01/01/2018' and current_date
  and c.account in ('120300','220200')) x
group by control  
order by control


doc 1408370

select * from fin.dim_account where account = '148204'


select a.amount, b.page, b.line, b.col, b.line_label, d.gm_account, d.gl_account, c.area, c.department, c.sub_department
from fin.fact_fs a
inner join fin.dim_Fs b on a.fs_key = b.fs_key
  and b.year_month = 201801
inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key
  and c.store = 'ry1'
inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
where gl_account = '148204'
order by page, line, col


select b.the_date, c.account, a.*, d.description
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
inner join fin.dim_account c on a.account_key = c.account_key
inner join fin.dim_gl_description d on a.gl_description_key = d.gl_description_key
where a.post_status = 'Y'
  and b.the_date between '01/01/2018' and current_date
  and c.account = '148204'
order by a.control
  
ok, time to elucidate the mind fucking that is happening
the vendor spend report is limited to accounts 120300 and 220200 which are cash in bank ?!?!?!?!
but is limited by the vendor number in glptrns (which is not scraped into fact_gl)
so, how do i translate that into page 2 semi fixed accounts?
ie, payments to vendors

may be able to do the correlation by a query against semi fixed accounts (p3 & p4 lines 18 - 39)
now which of those entries are payments to vendors?
there are vendor numbers in the glptrns rows to account 120300, but are there vendor numbers in the rows to page 3/4 accounts?

select a.amount, b.page, b.line, b.col, b.line_label, d.gm_account, d.gl_account, c.area, c.department, c.sub_department
from fin.fact_fs a
inner join fin.dim_Fs b on a.fs_key = b.fs_key
  and b.year_month = 201801
inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key
  and c.store = 'ry1'
inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
where page between 3 and 4
  and line between 18 and 39
order by page, line, col

select *
from arkona.xfm_glptrns
where account = '16804'


select *
from dds.ext_glpcust
where vendor_number = '12850'
glpcust:
  record_key: customer number (bopname_record_key)

select * from arkona.ext_bopname limit 10  

select *
from arkona.ext_glpcust
where vendor_number in ( '12850', '24970')

-- in aqt: page 4 line 26: information technology services
-- not one of these transaction have a vendor number in glptrns
-- but these are the transactions that total on page 4 line 26
select * 
from rydedata.glptrns 
where trim(gtacct) in ('16801','16802W','16802')
  and gtdate between '2018-01-01' and '2018-01-31'

-- 13600 is this a credit card (US BANK) ?
select b.the_date, c.account, a.*, c.description as acct_desc, 
  d.description as trans_descr, 
--   e.*
  e.customer_number, e.vendor_number, e.search_name
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
inner join fin.dim_account c on a.account_key = c.account_key
inner join fin.dim_gl_description d on a.gl_description_key = d.gl_description_key
left join dds.ext_glpcust e on a.control = e.vendor_number -- assuming glptrns.control = glpcust.vendor_number
  and e.company_number  = c.store_code
where a.post_status = 'Y'
  and b.the_date between '01/01/2018' and '01/31/2018'
  and c.account in ('16801','16802W','16802')
order by a.trans, a.seq

---------------------------------------------------------------------------------------------
-- the accounts
drop table if exists semi_fixed_accounts;
create temp table semi_fixed_accounts as
select distinct case coalesce(b.consolidation_grp, '1') when '2' then 'RY2' else 'RY1' end as store_code,
  a.fxmpge as page, a.fxmlne::integer as line, a.fxmcol as col, b.g_l_acct_number as gl_account,
  b.factory_account as gm_account
from arkona.ext_eisglobal_sypffxmst a
inner join arkona.ext_ffpxrefdta b on a.fxmact = b.factory_account
  and a.fxmcyy = b.factory_financial_year
where a.fxmcyy = 2018
  and trim(a.fxmcde) = 'GM'
  and coalesce(b.consolidation_grp, '1') <> '3'
  and b.factory_code = 'GM'
  and trim(b.factory_account) <> '331A'
--   and b.company_number = 'RY1'
  and b.g_l_acct_number <> ''
  and a.fxmpge between 3 and 4
  and a.fxmlne between 18 and 39;

-- line labels 
drop table if exists semi_fixed_line_labels;
create temp table semi_fixed_line_labels as
select page, line::integer, line_label
from fin.dim_fs 
where page = 2 
  and year_month = 201801
  and line between 18 and 39
  and col = 1
order by page, line;

select * from fin.dim_account limit 10
-- 13600 is this a credit card (US BANK) ?
select b.the_date, c.account, a.*, c.description as acct_desc, 
  d.description as trans_descr, 
--   e.*
  e.customer_number, e.vendor_number, e.search_name


vendor spend year to date
defined as 
    financial statement page 2 lines 18 - 39
    where the control number = vendor  number
this is not necessarily all the detail available, 
i believe we need to start with detail to determine what is necessary for categorization and rule definition    

-- this is the spreadsheet i sent out on 2/25/18
select b.year_month, b.the_date, c.store_code as store, f.line as fs_line, 
  f.line_label as fs_line_label, cc.gm_account, cc.gl_account, 
  c.description as acct_description, d.description as trans_description, c.department_code as dept,
  e.search_name as vendor, a.amount, a.control --, e.active
-- select count(*)
-- select max(c.account)
-- select a.*
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
inner join fin.dim_account c on a.account_key = c.account_key
inner join semi_fixed_accounts cc on c.account = cc.gl_account
inner join dds.ext_glpcust e on a.control = e.vendor_number 
--   and c.store_code = e.company_number
inner join fin.dim_gl_description d on a.gl_description_key = d.gl_description_key
inner join semi_fixed_line_labels f on cc.line = f.line
where a.post_status = 'Y'
  and b.the_year = 2018
order by e.search_name, c.store_code, b.the_date  
-- order by b.year_month, c.store_code, f.line, gm_account, gl_account
-- order by trans_description  

select b.the_date, c.account, a.*
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
  and b.the_date = '01/12/2018'
inner join fin.dim_Account c on a.account_key = c.account_key  
where control = '1115'

-- another view
-- all active vendors
select c.account, a.control, d.search_name, 
  sum(a.amount) filter (where b.year_month = 201701) as jan_17,
  sum(a.amount) filter (where b.year_month = 201702) as feb_17,
  sum(a.amount) filter (where b.year_month = 201703) as mar_17,
  sum(a.amount) filter (where b.year_month = 201704) as apr_17,
  sum(a.amount) filter (where b.year_month = 201705) as may_17,
  sum(a.amount) filter (where b.year_month = 201706) as juj_17,
  sum(a.amount) filter (where b.year_month = 201707) as jul_17,
  sum(a.amount) filter (where b.year_month = 201708) as aug_17,
  sum(a.amount) filter (where b.year_month = 201709) as sep_17,
  sum(a.amount) filter (where b.year_month = 201710) as oct_17,
  sum(a.amount) filter (where b.year_month = 201711) as nov_17,
  sum(a.amount) filter (where b.year_month = 201712) as dec_17,
  sum(a.amount) filter (where b.year_month = 201801) as jan_18,
  sum(a.amount) filter (where b.year_month = 201802) as feb_18
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
inner join fin.dim_account c on a.account_key = c.account_key
inner join dds.ext_glpcust d on a.control = d.vendor_number
  and d.active in ('V','B')
where post_status = 'Y'
  and b.the_year >= 2017
  and c.account_type = 'Expense'
group by c.account, a.control, d.search_name
order by d.search_name


-- add the count
-- all active vendors
-- explain analyze
select c.store_code as store, c.account, a.control as vendor_number, d.search_name as vendor, 
  (count(1) filter (where b.year_month = 201701))::text || ' / $' || ((sum(a.amount) filter (where b.year_month = 201701))::integer)::text as jan_17,
  (count(1) filter (where b.year_month = 201702))::text || ' / $' || ((sum(a.amount) filter (where b.year_month = 201702))::integer)::text as feb_17,
  (count(1) filter (where b.year_month = 201703))::text || ' / $' || ((sum(a.amount) filter (where b.year_month = 201703))::integer)::text as mar_17,
  (count(1) filter (where b.year_month = 201704))::text || ' / $' || ((sum(a.amount) filter (where b.year_month = 201704))::integer)::text as apr_17,
  (count(1) filter (where b.year_month = 201705))::text || ' / $' || ((sum(a.amount) filter (where b.year_month = 201705))::integer)::text as may_17,
  (count(1) filter (where b.year_month = 201706))::text || ' / $' || ((sum(a.amount) filter (where b.year_month = 201706))::integer)::text as juj_17,
  (count(1) filter (where b.year_month = 201707))::text || ' / $' || ((sum(a.amount) filter (where b.year_month = 201707))::integer)::text as jul_17,
  (count(1) filter (where b.year_month = 201708))::text || ' / $' || ((sum(a.amount) filter (where b.year_month = 201708))::integer)::text as aug_17,
  (count(1) filter (where b.year_month = 201709))::text || ' / $' || ((sum(a.amount) filter (where b.year_month = 201709))::integer)::text as sep_17,
  (count(1) filter (where b.year_month = 201710))::text || ' / $' || ((sum(a.amount) filter (where b.year_month = 201710))::integer)::text as oct_17,
  (count(1) filter (where b.year_month = 201711))::text || ' / $' || ((sum(a.amount) filter (where b.year_month = 201711))::integer)::text as nov_17,
  (count(1) filter (where b.year_month = 201712))::text || ' / $' || ((sum(a.amount) filter (where b.year_month = 201712))::integer)::text as dec_17,
  (count(1) filter (where b.year_month = 201801))::text || ' / $' || ((sum(a.amount) filter (where b.year_month = 201801))::integer)::text as jan_18,
  (count(1) filter (where b.year_month = 201802))::text || ' / $' || ((sum(a.amount) filter (where b.year_month = 201802))::integer)::text as feb_18
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
inner join fin.dim_account c on a.account_key = c.account_key
inner join dds.ext_glpcust d on a.control = d.vendor_number
  and d.active in ('V','B')
where post_status = 'Y'
  and b.the_year >= 2017
  and c.account_type = 'Expense'
group by c.store_code, c.account, a.control, d.search_name
order by d.search_name

explain (analyze,costs,verbose,buffers, format json)
select a.control, a.amount, b.the_date, b.year_month, c.account, d.search_name
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
inner join fin.dim_account c on a.account_key = c.account_key
inner join dds.ext_glpcust d on a.control = d.vendor_number
  and d.active in ('V','B')
where post_status = 'Y'
  and b.the_year >= 2017
  and c.account_type = 'Expense'


-- this is the spreadsheet i sent out on 2/25/18
-- greg keeps suggesting that transaction numbers are relevant
select b.year_month, b.the_date, c.store_code as store, f.line as fs_line, 
  f.line_label as fs_line_label, cc.gm_account, cc.gl_account, 
  c.description as acct_description, d.description as trans_description, c.department_code as dept,
  e.search_name as vendor, a.control as "vendor#", a.amount, a.trans, a.seq
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
inner join fin.dim_account c on a.account_key = c.account_key
inner join semi_fixed_accounts cc on c.account = cc.gl_account
inner join arkona.ext_glpcust e on a.control = e.vendor_number 
inner join fin.dim_gl_description d on a.gl_description_key = d.gl_description_key
inner join semi_fixed_line_labels f on cc.line = f.line
where a.post_status = 'Y'
  and b.the_year = 2018
order by e.search_name, c.store_code, b.the_date, a.trans, a.seq

----


select * 
from dds.ext_glpcust
limit 100

glpcust.record_key = bopname.bopname_record_key
glpcust PK = company_number, record_key
select company_number, record_key from dds.ext_glpcust group by company_number, record_key having count(*) > 1
in addition, it appears that vendor_number is unique
select vendor_number from dds.ext_glpcust group by vendor_number having count(*) > 1
select active, count(*) from dds.ext_glpcust group by active

-- 8 with no row in bopname
select a.*
-- select count(*)
from arkona.ext_glpcust a
left join arkona.ext_bopname b on a.record_key = b.bopname_record_key
  and a.company_number = b.bopname_company_number
where b.bopname_record_key is null

-- is there gl history on any of them
-- yep
select a.search_name, a.record_key, a.vendor_number,sum(c.amount), count(*), max(d.year_month)
-- select count(*)
from dds.ext_glpcust a
left join arkona.ext_bopname b on a.record_key = b.bopname_record_key
  and a.company_number = b.bopname_company_number
left join fin.fact_gl c on a.vendor_number = c.control  
left join dds.dim_date d on c.date_key = d.date_key
where b.bopname_record_key is null
  and a.vendor_number is not null
group by a.search_name, a.record_key, a.vendor_number
  

select * from dds.ext_glpcust where active = 'N'


select * from dds.ext_glpcust where active in ('B','V') order by last_statmnt_date desc limit 100


select vendor_number from dds.ext_glpcust group by vendor_number having count(*) > 1


--------------------------------------------------------------------------------
drop table if exists wtf;
create temp table wtf as
select b.year_month, a.trans, b.the_date, c.store_code as store, f.line as fs_line, 
  f.line_label as fs_line_label, cc.gm_account, cc.gl_account, 
  c.description as acct_description, d.description as trans_description, c.department_code as dept,
  e.search_name as vendor, a.control as vendor_number, sum(a.amount) as amount
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
inner join fin.dim_account c on a.account_key = c.account_key
inner join jeri.semi_fixed_accounts cc on c.account = cc.gl_account
inner join arkona.ext_glpcust e on a.control = e.vendor_number 
inner join fin.dim_gl_description d on a.gl_description_key = d.gl_description_key
inner join jeri.semi_fixed_line_labels f on cc.line = f.line
inner join fin.dim_doc_type g on a.doc_type_key = g.doc_type_key
where a.post_status = 'Y'
  and b.the_year >= 2017
group by b.year_month, a.trans, b.the_date, c.store_code, f.line, 
  f.line_label, cc.gm_account, cc.gl_account, 
  c.description, d.description, c.department_code,
  e.search_name, a.control;
create unique index on wtf (vendor_number,the_date,trans,trans_description,gl_account);


select dept, count(*) from wtf group by dept


select count(*) from wtf -- 29053
select * from wtf limit 100

select doc_type, count(*) from wtf group by doc_Type


select vendor_number, the_date, gl_account, trans, trans_description
from wtf
group by vendor_number, the_date, gl_account, trans, trans_description
having count(*) > 1

select * from wtf
where vendor_number = '12650'
  and the_date = '01/15/2018'
  and gl_account = '16006'

select * from wtf 
order by vendor_number,the_date,trans,trans_description,gl_account
limit 1000


select * from dds.dim_date limit 10  

select * from fin.fact_gl where control = '124010' and doc = '1396297' and ref in ( '220697','220698')


select store, vendor, 
  sum(amount) filter (where year_month = 201711) as nov_17,
  sum(amount) filter (where year_month = 201712) as dec_17,
  sum(amount) filter (where year_month = 201801) as jan_18,
  sum(amount) filter (where year_month = 201802) as feb_18
from wtf a
where year_month > 201710
group by store, vendor
order by vendor, store


select year_month
from dds.dim_date
where

select b.the_date, c.account, c.account_type, a.* 
select account_type, sum(amount)
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
inner join fin.dim_account c on a.account_key = c.account_key 
where control = '18057255'
  and account_type in ('sale','cogs')
group by account_type


