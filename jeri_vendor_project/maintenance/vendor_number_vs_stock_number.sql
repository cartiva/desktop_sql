﻿
the transaction (that fails the vendor_rule) shows a $2 pdq wash line going to vendor f&i reports
vendor number = 26330
stock number = 26330

  select b.year_month, b.the_date, c.store_code, f.line, 
    f.line_label as fs_line_label, cc.gm_account, cc.gl_account, 
    c.description, d.description, c.department_code,
    e.vendor_name, e.vendor_number, a.amount, a.trans, a.control, a.doc, a.ref, g.*
  from fin.fact_gl a
  inner join dds.dim_date b on a.date_key = b.date_key
--     and b.the_date between current_date - interval '3 month' and current_date
  inner join fin.dim_account c on a.account_key = c.account_key
  inner join jeri.semi_fixed_accounts cc on c.account = cc.gl_account
  inner join jeri.vendors e on a.control = e.vendor_number 
  inner join fin.dim_gl_description d on a.gl_description_key = d.gl_description_key
  inner join jeri.semi_fixed_line_labels f on cc.line = f.line
  inner join fin.dim_doc_type g on a.doc_type_key = g.doc_type_key
  where a.post_status = 'Y'
    and trans in (3823468,3841296,3842909)


first thought is to limit vendor spend based on doc_type

lets see how consistent the doc type is

  select g.doc_type_code, g.doc_type, count(*)
  from fin.fact_gl a
  inner join dds.dim_date b on a.date_key = b.date_key
    and b.the_year > 2016
  inner join fin.dim_account c on a.account_key = c.account_key
  inner join jeri.semi_fixed_accounts cc on c.account = cc.gl_account
  inner join jeri.vendors e on a.control = e.vendor_number 
  inner join fin.dim_gl_description d on a.gl_description_key = d.gl_description_key
  inner join jeri.semi_fixed_line_labels f on cc.line = f.line
  inner join fin.dim_doc_type g on a.doc_type_key = g.doc_type_key
  where a.post_status = 'Y'
  group by g.doc_type_code, g.doc_type

C;checks;619
D;deposits;2
J;conversion or gje;4369
O;invoice/po entry;27768
P;parts ticket;128
S;service tickets;284

holy shit, looks like there is a shitload of stocknumber/vendornumber overlap, maybe even some last 6 of vin/vendor number
do not yet know how to separate vendor spend vs non vendor spend
add journal

drop table if exists wtf;
create temp table wtf as
  select b.year_month, b.the_date, c.store_code, 
    f.line_label as fs_line_label, cc.page, cc.line, cc.col, cc.gm_account, cc.gl_account, 
    c.description as acct_desc, d.description as trans_desc, c.department_code,
    e.vendor_name, e.vendor_number, a.amount, a.trans, a.control, a.doc, a.ref, g.doc_type_code, g.doc_type,
    h.journal_code, h.journal
  from fin.fact_gl a
  inner join dds.dim_date b on a.date_key = b.date_key
    and b.the_year > 2016
  inner join fin.dim_account c on a.account_key = c.account_key
  inner join jeri.semi_fixed_accounts cc on c.account = cc.gl_account
  inner join jeri.vendors e on a.control = e.vendor_number 
  inner join fin.dim_gl_description d on a.gl_description_key = d.gl_description_key
  inner join jeri.semi_fixed_line_labels f on cc.line = f.line
  inner join fin.dim_doc_type g on a.doc_type_key = g.doc_type_key
  inner join fin.dim_journal h on a.journal_key = h.journal_key
  where a.post_status = 'Y';

create index on wtf(gl_account);
create index on wtf(vendor_number);
create index on wtf(vendor_name);
create index on wtf(fs_line_label);


select * from wtf where doc_type_code = 'j'
--     and g.doc_type_code in ('p','s')

select doc_type_code, doc_type, count(*)
from wtf
where journal_code = 'SVI'
group by doc_type_code, doc_type


select * 
from wtf
where doc_type_code = 'J'
  and journal_code = 'SVI'

select *
from wtf
where vendor_number = '26330'
order by trans

select gl_account, vendor_number, vendor_name, count(*)
from wtf
group by gl_account, vendor_number, vendor_name
order by vendor_number


select *
from wtf
where gl_account = '16503'


select vendor_number, vendor_name, fs_line_label, count(*)
from wtf
group by vendor_number, vendor_name, fs_line_label
order by vendor_number, vendor_name, fs_line_label

select * from wtf where vendor_number = '25800' and fs_line_label = 'advertising'


select * from wtf where trans in (3715688,3715671)

select min(the_date), max(the_date)
from arkona.xfm_glptrns
limit 100

select *
from wtf
where ref = '515518'


select *
from fin.fact_gl a
inner join fin.dim_account b on a.account_key = b.account_key
where post_status = 'Y'
  and ref = '515518'

create index on fin.fact_gl(ref)  


select * from sls.personnel where first_name = 'john'

select * from ads.ext_dim_salesperson where lastname = 'olderbak'

-- 4/12/18
ok, so if i identify the transactions by the payable or cash accounts, then use the transactions to get the
semi-fixed accounts, will that give me what i need

-- this is an example of where that approach does not work, the vendor # exists in the transaction
-- but does not exist in vendors
select *
from jeri.vendors
where vendor_number = '001123585'

so, thinking, limit gl transactions to those where vendor# exists in glpcust, then use those transaction numbers
to get the transactions against semi-fixed accounts


just talked to jeri, not sure she understood all my issues, but she suggested simply leaving out the parts & service doc types
ok, will start with that, then compare to what would be derived using the above (gtvnd# in glptrns)

so, need to truncate and refill vendor_spend_detail, excluding the parts/service transactions & modify the nightly update


select * 
from wtf
where doc_type_code in ('s','p')

select * 
from wtf
where trans_desc like 'payroll%'


