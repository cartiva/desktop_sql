﻿/*

drop table if exists arkona.xfm_inpmast cascade;
CREATE TABLE arkona.xfm_inpmast
(
  inpmast_company_number citext NOT NULL, -- IMCO# : Company Number
  inpmast_vin citext NOT NULL, -- IMVIN : VIN
  vin_last_6 citext, -- IMVIN6 : VIN LAST 6
  inpmast_stock_number citext, -- IMSTK# : Stock Number
  inpmast_document_number citext, -- IMDOC# : Document Number
  status citext, -- IMSTAT : Status
  g_l_applied citext, -- IMGTRN : G/L Applied
  type_n_u citext, -- IMTYPE : Type-N,U
  bus_off_fran_code citext, -- IMFRAN : Bus Off Fran Code
  service_fran_code citext, -- IMSFRN : Service Fran Code
  manufacturer_code citext, -- IMMANF : Manufacturer Code
  vehicle_code citext, -- IMVCOD : Vehicle Code
  year integer, -- IMYEAR : Year
  make citext, -- IMMAKE : Make
  model_code citext, -- IMMCODE : Model Code
  model citext, -- IMMODL : Model
  body_style citext, -- IMBODY : Body Style
  color citext, -- IMCOLR : Color
  "trim" citext, -- IMTRIM : Trim
  fuel_type citext, -- IMFUEL : Fuel Type
  mpg citext, -- IMMPG : MPG
  cylinders integer, -- IMCYL : Cylinders
  truck citext, -- IMTRCK : Truck
  wheel_drive4wd citext, -- IM4WD : 4 Wheel Drive
  turbo citext, -- IMTURB : Turbo
  color_code citext, -- IMCCODE : Color Code
  engine_code citext, -- IMECODE : Engine Code
  transmission_code citext, -- IMTCODE : Transmission Code
  ignition_key_code citext, -- IMIGKC : Ignition Key Code
  trunk_key_code citext, -- IMTRKC : Trunk Key Code
  keyless_code citext, -- IMKYLC : Keyless Code
  radio_code citext, -- IMRADC : Radio Code
  wheel_lock_code citext, -- IMWHLC : Wheel Lock Code
  dealer_code citext, -- IMDLRC : Dealer Code
  location citext, -- IMLOCN : Location
  odometer integer, -- IMODOM : Odometer
  date_in_invent integer, -- IMDINV : Date In Invent
  date_in_service integer, -- IMDSVC : Date In Service
  date_delivered integer, -- IMDDLV : Date Delivered
  date_ordered integer, -- IMDORD : Date Ordered
  inpmast_sale_account citext, -- IMSACT : Sale Account
  inventory_account citext, -- IMIACT : Inventory Account
  demo_name citext, -- IMDNAM : Demo Name
  warranty_months integer, -- IMWMOS : Warranty Months
  warranty_miles integer, -- IMWMIL : Warranty Miles
  warranty_deduct integer, -- IMWDED : Warranty Deduct
  list_price numeric(9,2), -- IMPRIC : List Price
  inpmast_vehicle_cost numeric(9,2), -- IMCOST : Vehicle Cost
  option_package citext, -- IMOPKG : Option Package
  license_number citext, -- IMLIC# : License Number
  gross_weight integer, -- IMGRSW : Gross Weight
  work_in_process numeric(7,2), -- IMWKIP : Work in Process
  inspection_month integer, -- IMINSM : Inspection Month
  odometer_actual citext, -- IMODOMA : Odometer Actual
  bopname_key integer, -- IMKEY : BOPNAME Key
  key_to_cap_explosion_data citext, -- IMCAP : Key to CAP explosion data
  co2_emission_code2 citext, -- IMCO2 : CO2 emission code
  registration_date1 integer, -- IMDAT1 : Registration Date
  funding_expiration_date2 integer, -- IMDAT2 : Funding Expiration Date
  inspection_date3 integer, -- IMDAT3 : Inspection Date
  drivers_side citext, -- IMDRIV : Drivers side
  free_flooring_period integer, -- IMFSTK : Free flooring period
  ordered_status citext, -- IMOSTAT : Ordered status
  publish_vehicle_info_to_web citext, -- IMPUBL : Publish vehicle info to web
  sale citext, -- IMSRC : Sale
  certified_used_car citext, -- IMCERT : Certified Used Car
  last_service_date4 integer, -- IMDAT4 : Last Service Date
  next_service_date5 integer, -- IMDAT5 : Next Service Date
  dealer_code_imdlrcd bigint, -- IMDLRCD : Dealer Code
  common_vehicle_id bigint, -- IMCVID : Common Vehicle ID
  chrome_style_id bigint, -- IMCHROMEID : Chrome Style ID
  created_user_code bigint, -- IMCRTUSR : Created User Code
  updated_user_code bigint, -- IMUPDUSR : Updated User Code
  created_timestamp timestamp with time zone, -- IMCRTTS : Created TimeStamp
  updated_timestamp timestamp with time zone, -- IMUPDTS : Updated TimeStamp
  stocked_company citext, -- IMSTKCO# : Stocked Company
  engine_hours integer, -- IMENGHRS : Engine Hours
  inpmast_key serial primary key,
  row_from_date date not null,
  row_thru_date date not null default '9999-12-31',
  current_row boolean not null,
  hash text not null
)
WITH (
  OIDS=FALSE
);
ALTER TABLE arkona.xfm_inpmast
  OWNER TO rydell;
COMMENT ON COLUMN arkona.xfm_inpmast.inpmast_company_number IS 'IMCO# : Company Number';
COMMENT ON COLUMN arkona.xfm_inpmast.inpmast_vin IS 'IMVIN : VIN';
COMMENT ON COLUMN arkona.xfm_inpmast.vin_last_6 IS 'IMVIN6 : VIN LAST 6';
COMMENT ON COLUMN arkona.xfm_inpmast.inpmast_stock_number IS 'IMSTK# : Stock Number';
COMMENT ON COLUMN arkona.xfm_inpmast.inpmast_document_number IS 'IMDOC# : Document Number';
COMMENT ON COLUMN arkona.xfm_inpmast.status IS 'IMSTAT : Status';
COMMENT ON COLUMN arkona.xfm_inpmast.g_l_applied IS 'IMGTRN : G/L Applied';
COMMENT ON COLUMN arkona.xfm_inpmast.type_n_u IS 'IMTYPE : Type-N,U';
COMMENT ON COLUMN arkona.xfm_inpmast.bus_off_fran_code IS 'IMFRAN : Bus Off Fran Code';
COMMENT ON COLUMN arkona.xfm_inpmast.service_fran_code IS 'IMSFRN : Service Fran Code';
COMMENT ON COLUMN arkona.xfm_inpmast.manufacturer_code IS 'IMMANF : Manufacturer Code';
COMMENT ON COLUMN arkona.xfm_inpmast.vehicle_code IS 'IMVCOD : Vehicle Code';
COMMENT ON COLUMN arkona.xfm_inpmast.year IS 'IMYEAR : Year';
COMMENT ON COLUMN arkona.xfm_inpmast.make IS 'IMMAKE : Make';
COMMENT ON COLUMN arkona.xfm_inpmast.model_code IS 'IMMCODE : Model Code';
COMMENT ON COLUMN arkona.xfm_inpmast.model IS 'IMMODL : Model';
COMMENT ON COLUMN arkona.xfm_inpmast.body_style IS 'IMBODY : Body Style';
COMMENT ON COLUMN arkona.xfm_inpmast.color IS 'IMCOLR : Color';
COMMENT ON COLUMN arkona.xfm_inpmast."trim" IS 'IMTRIM : Trim';
COMMENT ON COLUMN arkona.xfm_inpmast.fuel_type IS 'IMFUEL : Fuel Type';
COMMENT ON COLUMN arkona.xfm_inpmast.mpg IS 'IMMPG : MPG';
COMMENT ON COLUMN arkona.xfm_inpmast.cylinders IS 'IMCYL : Cylinders';
COMMENT ON COLUMN arkona.xfm_inpmast.truck IS 'IMTRCK : Truck';
COMMENT ON COLUMN arkona.xfm_inpmast.wheel_drive4wd IS 'IM4WD : 4 Wheel Drive';
COMMENT ON COLUMN arkona.xfm_inpmast.turbo IS 'IMTURB : Turbo';
COMMENT ON COLUMN arkona.xfm_inpmast.color_code IS 'IMCCODE : Color Code';
COMMENT ON COLUMN arkona.xfm_inpmast.engine_code IS 'IMECODE : Engine Code';
COMMENT ON COLUMN arkona.xfm_inpmast.transmission_code IS 'IMTCODE : Transmission Code';
COMMENT ON COLUMN arkona.xfm_inpmast.ignition_key_code IS 'IMIGKC : Ignition Key Code';
COMMENT ON COLUMN arkona.xfm_inpmast.trunk_key_code IS 'IMTRKC : Trunk Key Code';
COMMENT ON COLUMN arkona.xfm_inpmast.keyless_code IS 'IMKYLC : Keyless Code';
COMMENT ON COLUMN arkona.xfm_inpmast.radio_code IS 'IMRADC : Radio Code';
COMMENT ON COLUMN arkona.xfm_inpmast.wheel_lock_code IS 'IMWHLC : Wheel Lock Code';
COMMENT ON COLUMN arkona.xfm_inpmast.dealer_code IS 'IMDLRC : Dealer Code';
COMMENT ON COLUMN arkona.xfm_inpmast.location IS 'IMLOCN : Location';
COMMENT ON COLUMN arkona.xfm_inpmast.odometer IS 'IMODOM : Odometer';
COMMENT ON COLUMN arkona.xfm_inpmast.date_in_invent IS 'IMDINV : Date In Invent';
COMMENT ON COLUMN arkona.xfm_inpmast.date_in_service IS 'IMDSVC : Date In Service';
COMMENT ON COLUMN arkona.xfm_inpmast.date_delivered IS 'IMDDLV : Date Delivered';
COMMENT ON COLUMN arkona.xfm_inpmast.date_ordered IS 'IMDORD : Date Ordered';
COMMENT ON COLUMN arkona.xfm_inpmast.inpmast_sale_account IS 'IMSACT : Sale Account';
COMMENT ON COLUMN arkona.xfm_inpmast.inventory_account IS 'IMIACT : Inventory Account';
COMMENT ON COLUMN arkona.xfm_inpmast.demo_name IS 'IMDNAM : Demo Name';
COMMENT ON COLUMN arkona.xfm_inpmast.warranty_months IS 'IMWMOS : Warranty Months';
COMMENT ON COLUMN arkona.xfm_inpmast.warranty_miles IS 'IMWMIL : Warranty Miles';
COMMENT ON COLUMN arkona.xfm_inpmast.warranty_deduct IS 'IMWDED : Warranty Deduct';
COMMENT ON COLUMN arkona.xfm_inpmast.list_price IS 'IMPRIC : List Price';
COMMENT ON COLUMN arkona.xfm_inpmast.inpmast_vehicle_cost IS 'IMCOST : Vehicle Cost';
COMMENT ON COLUMN arkona.xfm_inpmast.option_package IS 'IMOPKG : Option Package';
COMMENT ON COLUMN arkona.xfm_inpmast.license_number IS 'IMLIC# : License Number';
COMMENT ON COLUMN arkona.xfm_inpmast.gross_weight IS 'IMGRSW : Gross Weight';
COMMENT ON COLUMN arkona.xfm_inpmast.work_in_process IS 'IMWKIP : Work in Process';
COMMENT ON COLUMN arkona.xfm_inpmast.inspection_month IS 'IMINSM : Inspection Month';
COMMENT ON COLUMN arkona.xfm_inpmast.odometer_actual IS 'IMODOMA : Odometer Actual';
COMMENT ON COLUMN arkona.xfm_inpmast.bopname_key IS 'IMKEY : BOPNAME Key';
COMMENT ON COLUMN arkona.xfm_inpmast.key_to_cap_explosion_data IS 'IMCAP : Key to CAP explosion data';
COMMENT ON COLUMN arkona.xfm_inpmast.co2_emission_code2 IS 'IMCO2 : CO2 emission code';
COMMENT ON COLUMN arkona.xfm_inpmast.registration_date1 IS 'IMDAT1 : Registration Date';
COMMENT ON COLUMN arkona.xfm_inpmast.funding_expiration_date2 IS 'IMDAT2 : Funding Expiration Date';
COMMENT ON COLUMN arkona.xfm_inpmast.inspection_date3 IS 'IMDAT3 : Inspection Date';
COMMENT ON COLUMN arkona.xfm_inpmast.drivers_side IS 'IMDRIV : Drivers side';
COMMENT ON COLUMN arkona.xfm_inpmast.free_flooring_period IS 'IMFSTK : Free flooring period';
COMMENT ON COLUMN arkona.xfm_inpmast.ordered_status IS 'IMOSTAT : Ordered status';
COMMENT ON COLUMN arkona.xfm_inpmast.publish_vehicle_info_to_web IS 'IMPUBL : Publish vehicle info to web';
COMMENT ON COLUMN arkona.xfm_inpmast.sale IS 'IMSRC : Sale';
COMMENT ON COLUMN arkona.xfm_inpmast.certified_used_car IS 'IMCERT : Certified Used Car';
COMMENT ON COLUMN arkona.xfm_inpmast.last_service_date4 IS 'IMDAT4 : Last Service Date';
COMMENT ON COLUMN arkona.xfm_inpmast.next_service_date5 IS 'IMDAT5 : Next Service Date';
COMMENT ON COLUMN arkona.xfm_inpmast.dealer_code_imdlrcd IS 'IMDLRCD : Dealer Code';
COMMENT ON COLUMN arkona.xfm_inpmast.common_vehicle_id IS 'IMCVID : Common Vehicle ID';
COMMENT ON COLUMN arkona.xfm_inpmast.chrome_style_id IS 'IMCHROMEID : Chrome Style ID';
COMMENT ON COLUMN arkona.xfm_inpmast.created_user_code IS 'IMCRTUSR : Created User Code';
COMMENT ON COLUMN arkona.xfm_inpmast.updated_user_code IS 'IMUPDUSR : Updated User Code';
COMMENT ON COLUMN arkona.xfm_inpmast.created_timestamp IS 'IMCRTTS : Created TimeStamp';
COMMENT ON COLUMN arkona.xfm_inpmast.updated_timestamp IS 'IMUPDTS : Updated TimeStamp';
COMMENT ON COLUMN arkona.xfm_inpmast.stocked_company IS 'IMSTKCO# : Stocked Company';
COMMENT ON COLUMN arkona.xfm_inpmast.engine_hours IS 'IMENGHRS : Engine Hours';

create unique index on arkona.xfm_inpmast(inpmast_vin,row_thru_date);
create unique index on arkona.xfm_inpmast(hash);
*/


select a.*, 
  (
    select md5(z::text) as hash
    from (
      select *
      from arkona.ext_inpmast
      where inpmast_vin = a.inpmast_vin
        and inpmast_company_number = a.inpmast_company_number)z)
from arkona.ext_inpmast a
where extract(year from updated_timestamp) = 2016
limit 100


select count(*)  --180810
from arkona.ext_inpmast

select max(created_timestamp): 2016-08-22 13:57:48.92177-05
from arkona.ext_inpmast

select max(updated_timestamp) 
from arkona.ext_inpmast


select count(*) --179317
from dds.ext_inpmast

select max(created_timestamp) -- 2017-06-30 15:44:33.947709-05
from dds.ext_inpmast

select max(updated_timestamp) -- 2017-06-30 15:44:33.947709-05
from dds.ext_inpmast

-- list of a table's fields, comma separated
SELECT string_agg(column_name, ',')
FROM information_schema.columns
WHERE table_schema = 'arkona'
  AND table_name   = 'ext_inpmast'
group by table_name 


-- 08-23-17: initial load from what currently existed in  dds.ext_inpmast (most recent scrape)
insert into arkona.xfm_inpmast (
  inpmast_company_number,inpmast_vin,vin_last_6,inpmast_stock_number,inpmast_document_number,
  status,g_l_applied,type_n_u,bus_off_fran_code,service_fran_code,manufacturer_code,vehicle_code,
  year,make,model_code,model,body_style,color,trim,fuel_type,mpg,cylinders,truck,wheel_drive4wd,
  turbo,color_code,engine_code,transmission_code,ignition_key_code,trunk_key_code,keyless_code,
  radio_code,wheel_lock_code,dealer_code,location,odometer,date_in_invent,date_in_service,
  date_delivered,date_ordered,inpmast_sale_account,inventory_account,demo_name,warranty_months,
  warranty_miles,warranty_deduct,list_price,inpmast_vehicle_cost,option_package,license_number,
  gross_weight,work_in_process,inspection_month,odometer_actual,bopname_key,
  key_to_cap_explosion_data,co2_emission_code2,registration_date1,funding_expiration_date2,
  inspection_date3,drivers_side,free_flooring_period,ordered_status,publish_vehicle_info_to_web,
  sale,certified_used_car,last_service_date4,next_service_date5,dealer_code_imdlrcd,
  common_vehicle_id,chrome_style_id,created_user_code,updated_user_code,created_timestamp,
  updated_timestamp,stocked_company,engine_hours,
  row_from_Date, current_row,hash)
select inpmast_company_number,inpmast_vin,vin_last_6,inpmast_stock_number,inpmast_document_number,
  status,g_l_applied,type_n_u,bus_off_fran_code,service_fran_code,manufacturer_code,vehicle_code,
  year,make,model_code,model,body_style,color,trim,fuel_type,mpg,cylinders,truck,wheel_drive4wd,
  turbo,color_code,engine_code,transmission_code,ignition_key_code,trunk_key_code,keyless_code,
  radio_code,wheel_lock_code,dealer_code,location,odometer,date_in_invent,date_in_service,
  date_delivered,date_ordered,inpmast_sale_account,inventory_account,demo_name,warranty_months,
  warranty_miles,warranty_deduct,list_price,inpmast_vehicle_cost,option_package,license_number,
  gross_weight,work_in_process,inspection_month,odometer_actual,bopname_key,
  key_to_cap_explosion_data,co2_emission_code2,registration_date1,funding_expiration_date2,
  inspection_date3,drivers_side,free_flooring_period,ordered_status,publish_vehicle_info_to_web,
  sale,certified_used_car,last_service_date4,next_service_date5,dealer_code_imdlrcd,
  common_vehicle_id,chrome_style_id,created_user_code,updated_user_code,created_timestamp,
  updated_timestamp,stocked_company,engine_hours,
  -- skip inpmast_key: serial
  current_date as row_from_Date,
  -- skip row_thru_date: default value
  true as current_row,
    (
      select md5(z::text) as hash
      from (
        select inpmast_company_number,inpmast_vin,vin_last_6,inpmast_stock_number,inpmast_document_number,
          status,g_l_applied,type_n_u,bus_off_fran_code,service_fran_code,manufacturer_code,vehicle_code,
          year,make,model_code,model,body_style,color,trim,fuel_type,mpg,cylinders,truck,wheel_drive4wd,
          turbo,color_code,engine_code,transmission_code,ignition_key_code,trunk_key_code,keyless_code,
          radio_code,wheel_lock_code,dealer_code,location,odometer,date_in_invent,date_in_service,
          date_delivered,date_ordered,inpmast_sale_account,inventory_account,demo_name,warranty_months,
          warranty_miles,warranty_deduct,list_price,inpmast_vehicle_cost,option_package,license_number,
          gross_weight,work_in_process,inspection_month,odometer_actual,bopname_key,
          key_to_cap_explosion_data,co2_emission_code2,registration_date1,funding_expiration_date2,
          inspection_date3,drivers_side,free_flooring_period,ordered_status,publish_vehicle_info_to_web,
          sale,certified_used_car,last_service_date4,next_service_date5,dealer_code_imdlrcd,
          common_vehicle_id,chrome_style_id,created_user_code,updated_user_code,created_timestamp,
          updated_timestamp,stocked_company,engine_hours
        from arkona.ext_inpmast
        where inpmast_company_number = a.inpmast_company_number
          and inpmast_vin = a.inpmast_vin) z)
from arkona.ext_inpmast a;


-- new rows
select count(*) --  7 rows
delete
from arkona.xfm_inpmast
where status = 'I'
  and type_n_u = 'N'
  and model_code = 'CK15906'

insert into arkona.xfm_inpmast (
  inpmast_company_number,inpmast_vin,vin_last_6,inpmast_stock_number,inpmast_document_number,
  status,g_l_applied,type_n_u,bus_off_fran_code,service_fran_code,manufacturer_code,vehicle_code,
  year,make,model_code,model,body_style,color,trim,fuel_type,mpg,cylinders,truck,wheel_drive4wd,
  turbo,color_code,engine_code,transmission_code,ignition_key_code,trunk_key_code,keyless_code,
  radio_code,wheel_lock_code,dealer_code,location,odometer,date_in_invent,date_in_service,
  date_delivered,date_ordered,inpmast_sale_account,inventory_account,demo_name,warranty_months,
  warranty_miles,warranty_deduct,list_price,inpmast_vehicle_cost,option_package,license_number,
  gross_weight,work_in_process,inspection_month,odometer_actual,bopname_key,
  key_to_cap_explosion_data,co2_emission_code2,registration_date1,funding_expiration_date2,
  inspection_date3,drivers_side,free_flooring_period,ordered_status,publish_vehicle_info_to_web,
  sale,certified_used_car,last_service_date4,next_service_date5,dealer_code_imdlrcd,
  common_vehicle_id,chrome_style_id,created_user_code,updated_user_code,created_timestamp,
  updated_timestamp,stocked_company,engine_hours,
  row_from_Date, current_row,hash)
select inpmast_company_number,inpmast_vin,vin_last_6,inpmast_stock_number,inpmast_document_number,
  status,g_l_applied,type_n_u,bus_off_fran_code,service_fran_code,manufacturer_code,vehicle_code,
  year,make,model_code,model,body_style,color,trim,fuel_type,mpg,cylinders,truck,wheel_drive4wd,
  turbo,color_code,engine_code,transmission_code,ignition_key_code,trunk_key_code,keyless_code,
  radio_code,wheel_lock_code,dealer_code,location,odometer,date_in_invent,date_in_service,
  date_delivered,date_ordered,inpmast_sale_account,inventory_account,demo_name,warranty_months,
  warranty_miles,warranty_deduct,list_price,inpmast_vehicle_cost,option_package,license_number,
  gross_weight,work_in_process,inspection_month,odometer_actual,bopname_key,
  key_to_cap_explosion_data,co2_emission_code2,registration_date1,funding_expiration_date2,
  inspection_date3,drivers_side,free_flooring_period,ordered_status,publish_vehicle_info_to_web,
  sale,certified_used_car,last_service_date4,next_service_date5,dealer_code_imdlrcd,
  common_vehicle_id,chrome_style_id,created_user_code,updated_user_code,created_timestamp,
  updated_timestamp,stocked_company,engine_hours,
  -- skip inpmast_key: serial
  current_date as row_from_date,
  -- skip row_thru_date: default value
  true as current_row,
    (
      select md5(z::text) as hash
      from (
        select inpmast_company_number,inpmast_vin,vin_last_6,inpmast_stock_number,inpmast_document_number,
          status,g_l_applied,type_n_u,bus_off_fran_code,service_fran_code,manufacturer_code,vehicle_code,
          year,make,model_code,model,body_style,color,trim,fuel_type,mpg,cylinders,truck,wheel_drive4wd,
          turbo,color_code,engine_code,transmission_code,ignition_key_code,trunk_key_code,keyless_code,
          radio_code,wheel_lock_code,dealer_code,location,odometer,date_in_invent,date_in_service,
          date_delivered,date_ordered,inpmast_sale_account,inventory_account,demo_name,warranty_months,
          warranty_miles,warranty_deduct,list_price,inpmast_vehicle_cost,option_package,license_number,
          gross_weight,work_in_process,inspection_month,odometer_actual,bopname_key,
          key_to_cap_explosion_data,co2_emission_code2,registration_date1,funding_expiration_date2,
          inspection_date3,drivers_side,free_flooring_period,ordered_status,publish_vehicle_info_to_web,
          sale,certified_used_car,last_service_date4,next_service_date5,dealer_code_imdlrcd,
          common_vehicle_id,chrome_style_id,created_user_code,updated_user_code,created_timestamp,
          updated_timestamp,stocked_company,engine_hours
        from arkona.ext_inpmast
        where inpmast_company_number = a.inpmast_company_number
          and inpmast_vin = a.inpmast_vin) z)
from arkona.ext_inpmast a
where not exists (
  select 1
  from arkona.xfm_inpmast
  where inpmast_vin = a.inpmast_vin); 



-- changed rows



1. update current row to current_row = false
2  update current row row_thru_date to current_row - 1
3  insert new row

1. create a table of vins to be changed

-- create table greg.uc_inpmast_changed_row_vins (
--   vin citext primary key);

drop table if exists changed_row_vins;
create temp table changed_row_vins as
-- wow 108 changed rows
select a.inpmast_vin
from (
  select inpmast_vin,
      (
        select md5(z::text) as hash
        from (
          select inpmast_company_number,inpmast_vin,vin_last_6,inpmast_stock_number,inpmast_document_number,
            status,g_l_applied,type_n_u,bus_off_fran_code,service_fran_code,manufacturer_code,vehicle_code,
            year,make,model_code,model,body_style,color,trim,fuel_type,mpg,cylinders,truck,wheel_drive4wd,
            turbo,color_code,engine_code,transmission_code,ignition_key_code,trunk_key_code,keyless_code,
            radio_code,wheel_lock_code,dealer_code,location,odometer,date_in_invent,date_in_service,
            date_delivered,date_ordered,inpmast_sale_account,inventory_account,demo_name,warranty_months,
            warranty_miles,warranty_deduct,list_price,inpmast_vehicle_cost,option_package,license_number,
            gross_weight,work_in_process,inspection_month,odometer_actual,bopname_key,
            key_to_cap_explosion_data,co2_emission_code2,registration_date1,funding_expiration_date2,
            inspection_date3,drivers_side,free_flooring_period,ordered_status,publish_vehicle_info_to_web,
            sale,certified_used_car,last_service_date4,next_service_date5,dealer_code_imdlrcd,
            common_vehicle_id,chrome_style_id,created_user_code,updated_user_code,created_timestamp,
            updated_timestamp,stocked_company,engine_hours
          from arkona.ext_inpmast
          where inpmast_company_number = aa.inpmast_company_number
            and inpmast_vin = aa.inpmast_vin) z)
  from arkona.ext_inpmast aa)  a
inner join arkona.xfm_inpmast b on a.inpmast_vin = b.inpmast_vin
  and a.hash <> b.hash
  and b.current_row = true;

select * from changed_row_vins

1. / 2.
update arkona.xfm_inpmast 
set current_row = false,
    row_thru_date = current_date - 1
where inpmast_vin in (select inpmast_vin from changed_row_vins)
  and current_row = true;    

3.
-- insert new copy of changed row
insert into arkona.xfm_inpmast (
  inpmast_company_number,inpmast_vin,vin_last_6,inpmast_stock_number,inpmast_document_number,
  status,g_l_applied,type_n_u,bus_off_fran_code,service_fran_code,manufacturer_code,vehicle_code,
  year,make,model_code,model,body_style,color,trim,fuel_type,mpg,cylinders,truck,wheel_drive4wd,
  turbo,color_code,engine_code,transmission_code,ignition_key_code,trunk_key_code,keyless_code,
  radio_code,wheel_lock_code,dealer_code,location,odometer,date_in_invent,date_in_service,
  date_delivered,date_ordered,inpmast_sale_account,inventory_account,demo_name,warranty_months,
  warranty_miles,warranty_deduct,list_price,inpmast_vehicle_cost,option_package,license_number,
  gross_weight,work_in_process,inspection_month,odometer_actual,bopname_key,
  key_to_cap_explosion_data,co2_emission_code2,registration_date1,funding_expiration_date2,
  inspection_date3,drivers_side,free_flooring_period,ordered_status,publish_vehicle_info_to_web,
  sale,certified_used_car,last_service_date4,next_service_date5,dealer_code_imdlrcd,
  common_vehicle_id,chrome_style_id,created_user_code,updated_user_code,created_timestamp,
  updated_timestamp,stocked_company,engine_hours,
  row_from_Date, current_row,hash)
select inpmast_company_number,inpmast_vin,vin_last_6,inpmast_stock_number,inpmast_document_number,
  status,g_l_applied,type_n_u,bus_off_fran_code,service_fran_code,manufacturer_code,vehicle_code,
  year,make,model_code,model,body_style,color,trim,fuel_type,mpg,cylinders,truck,wheel_drive4wd,
  turbo,color_code,engine_code,transmission_code,ignition_key_code,trunk_key_code,keyless_code,
  radio_code,wheel_lock_code,dealer_code,location,odometer,date_in_invent,date_in_service,
  date_delivered,date_ordered,inpmast_sale_account,inventory_account,demo_name,warranty_months,
  warranty_miles,warranty_deduct,list_price,inpmast_vehicle_cost,option_package,license_number,
  gross_weight,work_in_process,inspection_month,odometer_actual,bopname_key,
  key_to_cap_explosion_data,co2_emission_code2,registration_date1,funding_expiration_date2,
  inspection_date3,drivers_side,free_flooring_period,ordered_status,publish_vehicle_info_to_web,
  sale,certified_used_car,last_service_date4,next_service_date5,dealer_code_imdlrcd,
  common_vehicle_id,chrome_style_id,created_user_code,updated_user_code,created_timestamp,
  updated_timestamp,stocked_company,engine_hours,
  -- skip inpmast_key: serial
  current_date as row_from_date,
  -- skip row_thru_date: default value
  true as current_row,
    (
      select md5(z::text) as hash
      from (
        select inpmast_company_number,inpmast_vin,vin_last_6,inpmast_stock_number,inpmast_document_number,
          status,g_l_applied,type_n_u,bus_off_fran_code,service_fran_code,manufacturer_code,vehicle_code,
          year,make,model_code,model,body_style,color,trim,fuel_type,mpg,cylinders,truck,wheel_drive4wd,
          turbo,color_code,engine_code,transmission_code,ignition_key_code,trunk_key_code,keyless_code,
          radio_code,wheel_lock_code,dealer_code,location,odometer,date_in_invent,date_in_service,
          date_delivered,date_ordered,inpmast_sale_account,inventory_account,demo_name,warranty_months,
          warranty_miles,warranty_deduct,list_price,inpmast_vehicle_cost,option_package,license_number,
          gross_weight,work_in_process,inspection_month,odometer_actual,bopname_key,
          key_to_cap_explosion_data,co2_emission_code2,registration_date1,funding_expiration_date2,
          inspection_date3,drivers_side,free_flooring_period,ordered_status,publish_vehicle_info_to_web,
          sale,certified_used_car,last_service_date4,next_service_date5,dealer_code_imdlrcd,
          common_vehicle_id,chrome_style_id,created_user_code,updated_user_code,created_timestamp,
          updated_timestamp,stocked_company,engine_hours
        from arkona.ext_inpmast
        where inpmast_company_number = a.inpmast_company_number
          and inpmast_vin = a.inpmast_vin) z)
from arkona.ext_inpmast a
where exists (
  select 1
  from changed_row_vins
  where inpmast_vin = a.inpmast_vin);


select *
-- select a.inpmast_stock_number, a.inpmast_vin, a.year, a.make, a.model, a.body_style, a.color, 
--   a.trim, a.list_price, a.inpmast_vehicle_cost, a.work_in_process
from arkona.xfm_inpmast a
where inpmast_vin in (
  select inpmast_vin
  from arkona.xfm_inpmast
  group by inpmast_vin
  having count(*) > 2)
order by inpmast_vin, inpmast_key


-- add accounting costs
-- may not be necessary, DT help says cost is dynamic
select a.inpmast_stock_number, a.inpmast_vin, a.year, a.make, a.model, 
  a.model_code, chrome_style_id, a.body_style as body_trim, a.color, 
  a.list_price, a.inpmast_vehicle_cost, a.work_in_process,
  b.holdback, b.flooring, b.internet_price, b.rebate
from arkona.xfm_inpmast a
left join (
  select company_number, vin_number, 
    sum(case when seq_number = 4 then num_field_value else 0 end) as holdback,
    sum(case when seq_number = 5 then num_field_value else 0 end) as flooring,
    sum(case when seq_number = 9 then num_field_value else 0 end) as internet_price,
    sum(case when seq_number = 12 then num_field_value else 0 end) as rebate
  from arkona.ext_inpoptf
  where seq_number in (4,5,9,12)
  group by company_number, vin_number) b on a.inpmast_company_number = b.company_number
  and a.inpmast_vin = b.vin_number
-- where a.inpmast_vin = '1G1ZE5ST9HF211440'
where status = 'I'
  and type_n_u = 'N'
  and current_row = true
  and left(inpmast_stock_number, 1) <> 'H'
order by inpmast_Stock_number

select *
from arkona.ext_inpoptf
where vin_number = '1G1ZE5ST9HF211440'

select company_number, vin_number, 
from arkona.ext_inpoptf
where vin_number = '1G1ZE5ST9HF211440'
group by company_number, vin_number

select company_number, seq_number, count(*)
from arkona.ext_inpoptf
group by company_number, seq_number
order by seq_number

3,4,5,9,12 are the only ones of interest

select *
from arkona.ext_inpoptf a
where exists (
  select 1
  from arkona.ext_inpoptf
  where vin_number = a.vin_number
    and seq_number = '19')
order by vin_number, seq_number    
limit 100


select company_number, vin_number, 
  sum(case when seq_number = 4 then num_field_value else 0 end) as holdback,
  sum(case when seq_number = 5 then num_field_value else 0 end) as flooring,
  sum(case when seq_number = 9 then num_field_value else 0 end) as internet_price,
  sum(case when seq_number = 12 then num_field_value else 0 end) as rebate
from arkona.ext_inpoptf
where seq_number in (4,5,9,12)
group by company_number, vin_number


select *
from arkona.xfm_inpmast
where inpmast_Stock_number = '30008'



select b.the_date, c.account, c.description, d.journal_code, a.amount, e.description, 
  account_type_code, account_type, department
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
inner join fin.dim_account c on a.account_key = c.account_key
inner join fin.dim_journal d on a.journal_key = d.journal_key
inner join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key
where a.post_status = 'Y'
  and control = '30008'
  and journal_code = 'VSN'
  and account_type = 'sale'
  and department = 'new vehicle' 

-- 9/17/17
-- anomalies  

-- multiple vins with same stock number
select *
from arkona.xfm_inpmast
where inpmast_stock_number in (
select inpmast_Stock_number
from arkona.xfm_inpmast
where current_row = true
group by inpmast_stock_number
having count(*) > 1)
order by inpmast_Stock_number, row_From_Date

-- exclude the junk stock numbers from the scrape
delete
from arkona.xfm_inpmast
where inpmast_stock_number in ('18','2','K','U', '3532');
-- 2 goofy ones
delete 
from arkona.xfm_inpmast
where inpmast_vin in ('1FMCU0H98DUB23818','1GCEK19037Z506985')