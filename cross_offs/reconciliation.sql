﻿drop table if exists nov;
create temp table nov as
select a.board_id,
  case store_key
    when 39 then 'GM'
    when 40 then 'HN'
    when 41 then 'Outlet'
  end::citext as location, 
  a.boarded_ts::date as boarded, d.vehicle_type, a.stock_number, a.vin, d.vehicle_make, d.vehicle_model,
  b.last_name as boarded_by, c.board_type, c.board_sub_type,
  e.fromts::date as vii_from, e.thruts::date as vii_thru, i.status as tool_status,
  a.is_deleted, a.deleted_Ts, a.deleted_by
from board.sales_board a
inner join nrv.users b on a.boarded_by = b.user_key
inner join board.board_types c on a.board_type_key = c.board_type_key
inner join board.daily_board d on a.board_id = d.board_id
left join ads.ext_vehicle_inventory_items e on a.stock_number = e.stocknumber
left join (
  select g.vehicleinventoryitemid, max(g.status) as status
  from ads.ext_vehicle_inventory_item_statuses g
  where g.fromts::date > current_date - 30
    and g.status not like '%Recon%'
    and g.fromts = (
      select max(h.fromts)
      from ads.ext_vehicle_inventory_item_statuses h
      where h.vehicleinventoryitemid = g.vehicleinventoryitemid
        and h.status not like '%Recon%')
  group by g.vehicleinventoryitemid) i on e.vehicleinventoryitemid = i.vehicleinventoryitemid
where a.is_deleted = false
--   and a.boarded_ts::date > '10/31/2017'
-- order by a.board_id  
order by   
  case store_key
    when 39 then 'GM'
    when 40 then 'HN'
    when 41 then 'Outlet'
  end, d.vehicle_type, a.boarded_ts::date, board_type;


select *
from nov
where location = 'GM'
  and boarded = '11/10/2017'
order by board_type, board_sub_type, vehicle_type, stock_number  


select *
from nov
where location = 'outlet'
order by boarded, board_type, vehicle_type, stock_number  


select * from nov where stock_number = '31861a'

need evals
that was the distraction, 
are evals being extracted to pg?


-- anomaly queries
select *
from nov
where stock_number in (
  select stock_number
  from nov
  where board_type = 'deal'
    and status not in('RawMaterials_Delivered','RawMaterials_Sold'))


  


select *
from board.sales_board
where stock_number = '32071XY'


select *
from board.daily_board
where board_id = 75

select b.vin, a.*
from ads.ext_vehicle_evaluations a
left join ads.ext_vehicle_items b on a.vehicleitemid = b.vehicleitemid
order by vehicleevaluationts desc 
limit 109


select a.*, b.status, c.*
from nov a
left join arkona.xfm_inpmast b on a.stock_number = b.inpmast_stock_number
  and b.current_row = true
left join sls.xfm_deals c on a.stock_number = c.stock_number  
where a.boarded = '11/06/2017'
  and a.location = 'HN'


-- h9954a from inventorty to sold to back on
select *
from arkona.xfm_inpmast
where inpmast_stock_number = 'H9954A'


select * 
from sls.xfm_deals
order by capped_date asc, approved_date desc
limit 100


select * 
from sls.xfm_deals
where stock_number = '31135c'

-- don't know if this will be useful or not
-- ie, 32431XX in transit, auction purchase before those transactions were boarded (paper board)
select *
from arkona.xfm_inpmast a
where date_in_invent > 20171100
  and type_n_u = 'U'
  and status = 'I'
  and not exists (
    select *
    from board.sales_board
    where stock_number = a.inpmast_Stock_number)


H10653A eval finished not booked 11-8    
11-4 32273xxxa not in tool


dpsVSeries sp getVehiclesFromStockNumberSearch, getVehiclesFromVinSearch

move evaluations to scheduled task


select *
from board.sales_board a
left join board.daily_board b on a.board_id = b.board_id
where is_deleted

select *
from nov
where stock_number = '32058B';

select *
from board.sales_board
where stock_number = '32058A'

select *
from board.sales_board
where stock_number = '32058B'



select *
from board.daily_board

-- 11-19 starting over, what am i trying to accomplish
primary: the tool is current with salesboard and arkona
secondary: salesboard agrees with inpmast/bopmast

new cars: reconcile to inpmast/bopmast/sls.deals

-- board data 460 rows
select a.board_id, a.boarded_ts, d.arkona_store_key, c.last_name as boarded_by, b.board_type, b.board_sub_type,
  e.vehicle_type, a.deal_number, a.stock_number, a.vin, 
  case when a.is_deleted then 'DELETED' else null end as deleted, 
  case when a.is_backed_on then 'BACK_ON' else null end as back_on,
  f.status, f.chrome_style_id, f.date_in_invent, f.date_delivered
-- select *
from board.sales_board a
left join board.board_types b on a.board_type_key = b.board_type_key
left join nrv.users c on a.boarded_by = c.user_key
left join onedc.stores d on a.store_key = d.store_key
left join board.daily_board e on a.board_id = e.board_id
left join arkona.xfm_inpmast f on a.vin = f.inpmast_vin
  and f.current_row = true
where a.stock_number = '32020a' order by boarded_ts


-- attributes sales_board.board_type_key and and sales_board.is_backed_on are not consistently in synch
select a.is_backed_on, b.board_type
from board.sales_board a
inner join board.board_types b on a.board_type_key = b.board_type_key
where b.board_type = 'Back-on'


select * 
from board.sales_board
where stock_number = '32020a'


SELECT *
FROM ads.ext_vehicle_inventory_items
where fromts::Date > '10/31/2017'

select *
from sls.ext_bopmast_partial
where stock_number like '32558%'

-- most recent row in sls.deals
drop table if exists deals;
create temp table deals as
select a.*
from sls.deals a
inner join (
  select store_code, bopmast_id, max(seq) as seq
  from sls.deals b
  group by store_code, bopmast_id) c on a.store_code = c.store_code and a.bopmast_id = c.bopmast_id and a.seq = c.seq


-- board data 460 rows
select a.board_id, a.boarded_ts, d.arkona_store_key, c.last_name as boarded_by, b.board_type, b.board_sub_type,
  e.vehicle_type, a.deal_number, a.stock_number, a.vin, 
  case when a.is_deleted then 'DELETED' else null end as deleted, 
  case when a.is_backed_on then 'BACK_ON' else null end as back_on,
  f.status, f.chrome_style_id, f.date_in_invent, f.date_delivered,
  g.*
-- select *
from board.sales_board a
left join board.board_types b on a.board_type_key = b.board_type_key
left join nrv.users c on a.boarded_by = c.user_key
left join onedc.stores d on a.store_key = d.store_key
left join board.daily_board e on a.board_id = e.board_id
left join arkona.xfm_inpmast f on a.vin = f.inpmast_vin
  and f.current_row = true
left join deals g on a.deal_number = g.bopmast_id::citext
order by a.stock_number

select * from deals where stock_number = 'h10671G'

select *
from sls.xfm_deals
where stock_number = 'h10671g'

select *
from sls.deals
where bopmast_id in (14129,14182)

select * 
from board.sales_board a
inner join board.daily_board b on a.board_id = b.board_id
where stock_number = '32558L'


-- dealer trade accounting
select *
from fin.fact_gl a
inner join fin.dim_account b on a.account_key = b.account_key
inner join fin.dim_gl_description c on a.gl_description_key = c.gl_description_key
where control = '32015'

-- lets focus on tool, used cars, sold, acquired, backed on

select a.board_id, a.boarded_ts, d.arkona_store_key, c.last_name as boarded_by, b.board_type, b.board_sub_type,
  e.vehicle_type, a.deal_number, a.stock_number, a.vin, 
  case when a.is_deleted then 'DELETED' else null end as deleted, 
  case when a.is_backed_on then 'BACK_ON' else null end as back_on,
  f.status, f.date_in_invent as inpmast_date_in_inv, f.date_delivered as inpmast_date_deliv
-- select *
from board.sales_board a
left join board.board_types b on a.board_type_key = b.board_type_key
left join nrv.users c on a.boarded_by = c.user_key
left join onedc.stores d on a.store_key = d.store_key
left join board.daily_board e on a.board_id = e.board_id
left join arkona.xfm_inpmast f on a.vin = f.inpmast_vin
  and f.current_row = true

order by a.stock_number





select *
from board.sales_board a
inner join board.board_types b on a.board_type_key = b.board_type_key
where a.stock_number = 'H10696p'



select *
from board.sales_board 



-- used car transactions that should be boarded but are not
select a.inpmast_stock_number, a.inpmast_vin, a.status, a.date_in_invent, a.date_delivered, 
  b.*
from arkona.xfm_inpmast a
left join board.sales_board b on a.inpmast_stock_number = b.stock_number
where a.current_row = true
  and a.date_in_invent > 20171100
  and a.type_n_u = 'u'
  and b.board_id is null




select *
from board.sales_board a
inner join board.board_types b on a.board_type_key = b.board_type_key

  
-- board data 487 rows
-- fuck, this is not what i wanted, this is all board data left joined to inpmast
-- so it does not tell me what is missing from board
drop table if exists board_data;
create temp table board_data as
select a.board_id, a.boarded_ts, d.arkona_store_key, c.last_name as boarded_by, b.board_type, b.board_sub_type,
  e.vehicle_type, a.deal_number, a.stock_number, a.vin, 
  case when a.is_deleted then 'DELETED' else null end as deleted, 
  case when a.is_backed_on then 'BACK_ON' else null end as back_on,
  f.status as inpmast_status, f.date_in_invent as inpmast_in, f.date_delivered as inpmast_del
-- select *
from board.sales_board a
left join board.board_types b on a.board_type_key = b.board_type_key
left join nrv.users c on a.boarded_by = c.user_key
left join onedc.stores d on a.store_key = d.store_key
left join board.daily_board e on a.board_id = e.board_id
left join arkona.xfm_inpmast f on (a.vin = f.inpmast_vin or a.stock_number = f.inpmast_stock_number)
  and f.current_row = true
-- order by board_type, board_sub_type, a.stock_number
order by a.stock_number

so change board_data to just board_data
select *
from board_data
where board_type in ('Addition','Back-on')
  and boarded_ts < current_Date

-- so it does not tell me what is missing from board
drop table if exists board_data;
create temp table board_data as
select a.board_id, a.boarded_ts::date as board_date, d.arkona_store_key, c.last_name as boarded_by, b.board_type, b.board_sub_type,
  e.vehicle_type, a.deal_number, a.stock_number, a.vin, 
  case when a.is_deleted then 'DELETED' else null end as deleted, 
  case when a.is_backed_on then 'BACK_ON' else null end as back_on
-- select *
from board.sales_board a
left join board.board_types b on a.board_type_key = b.board_type_key
left join nrv.users c on a.boarded_by = c.user_key
left join onedc.stores d on a.store_key = d.store_key
left join board.daily_board e on a.board_id = e.board_id;


-- is the tool at least contain the exising board stuff?
select *
from board_data a
left join ads.ext_vehicle_inventory_items b on a.stock_number = b.stocknumber
where a.board_type in ('Addition','Back-on')
  and a.board_date < current_Date
order by board_type, board_sub_type


32058B missing from tool * arkona
31981B or 31981YB


-- 11/22 
-- having a suprisingly tough time coming up with a reconciliation plan, everything seems like a one off
-- part of the problem is the lack of targets, what am i checking. seems like everything is partial and unreliable
-- daily , a set of tables for generating reports?
-- board, inpmast, sls.deals, vehicleevaluations, statuses

/*
need a series of tests/assertions
start with generalization
what am i trying to ascertain

1. tool is current
  A) all acquisitions
    a. trades
    b. purchases
  B)  all sales
  
*/

1. used car acquisitions
  A. trades & non trades
     a. all trades listed listed on board are in the tool
     b. all inventory in inpmast is in tool
     c. all inventory in inpmast is on board

data required 
  board data
  tool data
  inpmast data
  
drop table if exists board_data;
create temp table board_data as
select a.board_id, a.boarded_ts::date as board_date, d.arkona_store_key, c.last_name as boarded_by, b.board_type, b.board_sub_type,
  e.vehicle_type, a.deal_number, a.stock_number, a.vin, 
  case when a.is_deleted then 'DELETED' else null end as deleted, 
  case when a.is_backed_on then 'BACK_ON' else null end as back_on, 
  e.sale_code
-- select *
from board.sales_board a
left join board.board_types b on a.board_type_key = b.board_type_key
left join nrv.users c on a.boarded_by = c.user_key
left join onedc.stores d on a.store_key = d.store_key
left join board.daily_board e on a.board_id = e.board_id;  
create index on board_data(stock_number);
create index on board_data(vin);
create index on board_data(board_type);


-- this seems like it should be all the acquisition data, but where are the purchases?
select *
from board_data
where board_date > '10/31/2017'
  and vehicle_type = 'U'
  and board_type = 'Addition'

select *
from board_data
where stock_number like '%L'  


drop table if exists inpmast;
create temp table inpmast as
select a.inpmast_stock_number, a.inpmast_vin, a.status, a.type_n_u, 
  a.year, a.make, a.model, a.body_style, a.color, a.date_in_invent, a.date_delivered
from arkona.xfm_inpmast a
where a.current_row = true;
create index on inpmast(inpmast_stock_number);
create index on inpmast(inpmast_vin);
--   and (a.date_in_invent > 20171100 or a.date_delivered  > 20171100)
--   and a.date_delivered < 20180000
--   and a.date_in_invent < 20180000



select *
from board_data a
full outer join (
  select *
  from inpmast 
  where not (type_n_u = 'N' and status = 'I')) b on a.stock_number = b.inpmast_stock_number
order by a.stock_number

select * from inpmast where inpmast_stock_number = '31668XA'

-- back on, doesn't make it to inpmast
select a.inpmast_stock_number, a.inpmast_vin, a.status, a.type_n_u, 
  a.year, a.make, a.model, a.body_style, a.color, a.date_in_invent, a.date_delivered,
  a.row_from_date
from arkona.xfm_inpmast a 
where inpmast_stock_number = '31668XA'
order by a.row_from_date


select * from arkona.xfm_inpmast limit 10 


select currentstatus, count(*) from ads.ext_vehicle_evaluations group by currentstatus

drop table if exists evals;
create temp table evals as
select b.vin, a.vehicleevaluationts::date as eval_date, 
  substring(currentstatus, position('_' in currentstatus) + 1, 12) as eval_status, 
  d.stocknumber
from ads.ext_vehicle_evaluations a
inner join ads.ext_vehicle_items b on a.vehicleitemid = b.vehicleitemid
left join ads.ext_vehicle_inventory_items d on a.vehicleinventoryitemid = d.vehicleinventoryitemid;
create index on evals(stocknumber);
create index on evals(vin);


drop table if exists acqs;
create temp table acqs as
select b.vin, a.fromts::date, substring(c.typ, position('_' in c.typ) + 1, 12) as source, a.stocknumber
from ads.ext_Vehicle_inventory_items a
inner join ads.ext_vehicle_items b on a.vehicleitemid = b.vehicleitemid
inner join ads.ext_Vehicle_acquisitions c on a.vehicleinventoryitemid = c.vehicleinventoryitemid;
create index on acqs(vin);
create index on acqs(stocknumber);

select *
from board_data a
left outer join acqs b on a.stock_number = b.stocknumber or a.vin = b.vin
where a.board_type = 'addition'
  and b.vin is null

select * from ads.ext_Vehicle_inventory_items where stocknumber = '31802b'

select * from board_data where stock_number like '%Q'

select *
from board_data a
left outer join evals b on a.stock_number = b.stocknumber or a.vin = b.vin
where a.board_type = 'addition'
  and b.vin is null

select * from ads.ext_Vehicle_items where vin = '1GKS2BKC4FR233118'

select *
from ads.ext_vehicle_evaluations
where vehicleitemid = 'bde9c92a-fb66-3646-860e-90a9b4eab129'

select *
from ads.ext_vehicle_acquisitions
where vehicleinventoryitemid = '159eb8e7-b23b-4670-b6db-6158f3e19861'

32478L lease purchase, no eval, but has an acquisition
so, evals and acquisitions separately

there are no booked evaluations for which an acquisition does not exist
select *
from ads.ext_vehicle_evaluations a
left join ads.ext_vehicle_acquisitions b on a.vehicleinventoryitemid = b.vehicleinventoryitemid
where a.currentstatus = 'VehicleEvaluation_Booked'
  and b.vehicleinventoryitemid is null

select * 
from ads.ext_vehicle_acquisitions a
left join ads.ext_vehicle_inventory_items b on a.vehicleinventoryitemid = b.vehicleinventoryitemid
where b.vehicleinventoryitemid is null

eval --> acq --> vii

substring(c.typ, position('_' in c.typ) + 1, 12) as source, 



select *
from board_data 
where stock_number = '32532'



-- this looks potentially usable
-- inventoried vehicles not in tool
select *
from inpmast a
left join acqs b on a.inpmast_stock_number = b.stocknumber
left join evals c on a.inpmast_vin = c.vin
where a.status = 'I'
  and a.type_n_u = 'U'
  and b.vin is null
  and a.inpmast_Stock_number not in ('31560B', '31615CC') -- motorcycles



select * from acqs where stocknumber like '32270%'

select *
from ops.ads_extract
where task like 'uc_inv%'


-- november deals not in tool
select a.run_date, a.store_code, a.bopmast_id, a.deal_status_code, a.stock_number, a.vin, b.*
-- select *
from sls.deals a
left join ads.ext_vehicle_inventory_items b on a.stock_number = b.stocknumber
where a.year_month = 201711
  and a.vehicle_type_code = 'U'
  and a.stock_number not in ('32154AA') -- motorcycle
  and b.stocknumber is null
order by a.stock_number, a.seq


-- november deals not sold in tool
-- need current status for this
-- and figure out how to decode back ons
select a.run_date, a.store_code, a.bopmast_id, a.deal_status_code, a.stock_number, a.vin, b.*
-- select *
from sls.deals a
left join ads.ext_vehicle_inventory_items b on a.stock_number = b.stocknumber
where a.year_month = 201711
  and a.vehicle_type_code = 'U'
  and a.stock_number not in ('32154AA') -- motorcycle
  and b.thruts::date > current_date
order by a.stock_number, a.seq


-- november deals
select a.run_date, a.store_code, a.bopmast_id, a.deal_status_code, a.stock_number, a.vin, c.*
-- select *
from sls.deals a
left join ads.ext_vehicle_inventory_items b on a.stock_number = b.stocknumber
left join ads.ext_vehicle_inventory_item_statuses c on b.vehicleinventoryitemid = c.vehicleinventoryitemid
  and c.thruts::date > current_date
  and c.category not like '%Recon%'
where a.year_month = 201711
  and a.vehicle_type_code = 'U'
  and a.stock_number not in ('32154AA') -- motorcycle
  and b.thruts::date > current_date
order by a.stock_number, a.seq, c.fromts



select * from board_data where stock_number = '32098xx'

select * from sls.deals where stock_number = '32098xx'

select * from ads.ext_dim_customer where bnkey = 297171

select * from board_data where sale_code = 'NCC'


