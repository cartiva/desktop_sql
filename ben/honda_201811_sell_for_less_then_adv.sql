﻿select *
from sls.deals a
where year_month = 201811
  and store_code = 'ry2'
  and sale_type_code <> 'W'
  and vin in (
    select vin
    from sls.deals
    where year_month = 201811
      and store_code = 'ry2'
    group by vin
    having sum(unit_count) > 0)
order by vin    

select *
from sls.ext_bopmast_partial
where store = 'RY2'
  and bopmast_id = 16150

select *
from arkona.ext_bopmast
where bopmast_company_number = 'RY2'
  and record_key = 16150  

select a.bopmast_Stock_number, a.bopmast_vin, a.sale_type, a.record_key, a.lease_price, 
  a.retail_price, a.trade_allowance, a.trade_acv, a.date_capped, a.delivery_Date, a.vehicle_cost,
  b.year, b.make, b.model
  
select a.delivery_Date, a.bopmast_Stock_number, b.year, b.make, b.model, a.sale_type,
  c.price as web_price, 
  case 
    when a.sale_Type = 'R' then a.retail_price::integer
    when a.sale_type = 'L' then a.lease_price::integer
  end as price_sold_at,
  a.trade_acv::integer, a.trade_allowance::integer
from arkona.ext_bopmast a
left join arkona.xfm_inpmast b on a.bopmast_stock_number = b.inpmast_stock_number
  and b.current_row
left join (
  select *
  from (
    select stock_number, price, row_number() over (partition by stock_number order by lookup_date desc), lookup_date
    from scrapes.daily_rydell_websites 
    where website_Title in ('gfhonda','gfnissan')) a
  where row_number = 1) c on a.bopmast_stock_number = c.stock_number
where bopmast_company_number = 'RY2'
  and record_key in (
    select bopmast_id
    from sls.deals a
    where year_month = 201811
      and store_code = 'ry2'
      and sale_type_code <> 'W'
      and vin in (
        select vin
        from sls.deals
        where year_month = 201811
          and store_code = 'ry2'
        group by vin
        having sum(unit_count) > 0)) 
order by a.bopmast_Stock_number        

select*
from scrapes.daily_rydell_websites  
where vin = '19XZE4F92KE015308'

select *
from (
  select stock_number, price, row_number() over (partition by stock_number order by lookup_date desc), lookup_date
  from scrapes.daily_rydell_websites 
  where website_Title = 'rydellautocenter') a
where row_number = 1


select distinct website_title from scrapes.daily_rydell_websites 

