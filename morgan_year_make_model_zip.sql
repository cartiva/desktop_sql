﻿select  *
from sls.deals_by_month
order by year_month

select bb.year, bb.make, bb.model, cc.zip_code, count(*) as sales
from (
  select vin, buyer_bopname_id
  from sls.deals a
  where a.vehicle_type_code = 'n'
    and a.year_month between 201707 and 201806
  group by year_month, vin, buyer_bopname_id) aa
left join arkona.xfm_inpmast bb on aa.vin = bb.inpmast_vin
  and bb.current_row = true  
left join arkona.xfm_bopname cc on aa.buyer_bopname_id = cc.bopname_record_key
  and cc.current_row = true  
group by bb.year, bb.make, bb.model, cc.zip_code  