﻿report of all new vehicles sold in last 12 months that include aftermarket and/or service contracts
retail deals
in order by lastname
include first & lastname, stock#, afm/sc indicator

-- this is what i sent him
select buyer, stock_number, 
  case when service_contract = 1 then 'X' else null end as service_contract,
  case when afm <> 0 then 'X' else null end as aftermarket
from (  
  select e.buyer, e.stock_number, service_contract, coalesce(f.amount, 0) as afm
  from (
    select a.year_month, a.stock_number, a.buyer, a.service_contract
    -- select *
    from sls.deals_by_month a
    where a.unit_count = 1
      and a.sale_type in ('retail','lease')
      and a.year_month > 201707
      and a.vehicle_type = 'new'
      and a.store_code = 'ry1') e
  left join (
    select b.the_date, a.*
    from fin.fact_gl a
    inner join dds.dim_date b on a.date_key = b.date_key
      and b.year_month > 201707
    inner join fin.dim_Account c on a.account_key = c.account_key
      and c.account in ('144500','144502')) f on e.stock_number = f.control
  where service_contract = 1 or f.the_date is not null    
  group by  e.buyer, e.stock_number, service_contract, coalesce(f.amount, 0)) g
order by buyer






select *
from sls.ext_bopmast_partial
where total_care <> 0
  and capped_Date > '07/31/2017'
  and vehicle_type = 'N'
  and store = 'ry1'
order by stock_number  



select b.the_date, c.account, c.description, a.*
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
  and b.year_month > 201707
inner join fin.dim_Account c on a.account_key = c.account_key
where control = 'g33372r'
order by a.amount

