﻿drop table if exists jon.walk_notes;
create table jon.walk_notes (
  walk_date date not null,
  stock_number citext primary key,
  vin citext not null,
  make citext,
  model citext,
  trim citext,
  body_style citext,
  color citext,
  notes citext);

select b.*,
  c.record_Status, c.record_type, c.sale_type, c.bopmast_search_name, 
  c.trade_allowance, c.trade_acv, c.retail_price, c.date_capped, c.delivery_date
from (
  select a.stock_number, a.vin 
  from jon.walk_notes a) b
inner join arkona.ext_bopmast c on b.stock_number = c.bopmast_stock_number
order by c.delivery_date


select a.control, a.amount, b.account, b.account_type, b.department_code, c.journal_code
from fin.fact_gl a
inner join fin.dim_account b on a.account_key = b.account_key
  and b.current_row = true
  and b.account_Type in ('Sale','COGS')
inner join fin.dim_journal c on a.journal_key = c.journal_key
where a.control in (
  select stock_number
  from jon.walk_notes)


select a.control, 
  sum(case when b.department_code = 'uc' then -a.amount else 0 end) as front_gross,
  sum(case when b.department_code = 'fi' then -a.amount else 0 end) as fi_gross,
  sum(-a.amount) as total_gross
from fin.fact_gl a
inner join fin.dim_account b on a.account_key = b.account_key
  and b.current_row = true
  and b.account_Type in ('Sale','COGS')
inner join fin.dim_journal c on a.journal_key = c.journal_key
where a.control in (
  select stock_number
  from jon.walk_notes)
group by a.control  


select * from jon.walk_notes

  
select max(replace(b.notes, ',',';')) as notes, b.walk_date, b.stock_number, b.vin, b.make, b.model, b.trim,
  b.body_style, b.color,
  case when c.sale_type = 'R' then 'Retail' else 'Whlsl' end as sale_type,
  c.delivery_date as sale_date, 
  sum(case when d.department_code = 'uc' then -d.amount else 0 end) as front_gross,
  sum(case when d.department_code = 'fi' then -d.amount else 0 end) as fi_gross,
  sum(-d.amount) as total_gross
from jon.walk_notes b
inner join arkona.ext_bopmast c on b.stock_number = c.bopmast_stock_number
left join (
  select a.control, a.amount, b.account, b.account_type, c.journal_code, b.department_code
  from fin.fact_gl a
  inner join fin.dim_account b on a.account_key = b.account_key
    and b.current_row = true
    and b.account_Type in ('Sale','COGS')
  inner join fin.dim_journal c on a.journal_key = c.journal_key
  where a.post_Status = 'Y')  d on b.stock_number = d.control
group by b.walk_date, b.stock_number, b.vin, b.make, b.model, b.trim,
  b.body_style, b.color,
  case when c.sale_type = 'R' then 'Retail' else 'Whlsl' end ,
  c.delivery_date 




select a.control, a.amount, b.account, b.account_type, c.journal_code, b.department_code, a.post_status
-- select department_code, sum(amount)
from fin.fact_gl a
inner join fin.dim_account b on a.account_key = b.account_key
  and b.current_row = true
  and b.account_Type in ('Sale','COGS')
inner join fin.dim_journal c on a.journal_key = c.journal_key
where control = '29070a'
group by department_code

select *
from fin.fact_gl
where control = '29070a'




