﻿

-- similar to ucinv_tmp_avail_1
drop table if exists greg.uc_base_vehicles;
create table greg.uc_base_vehicles (
  store_code citext not null,
  stock_number citext not null,
  vin citext not null,
  date_acquired date not null,
  sale_date date not null,
  vehicle_inventory_item_id citext primary key,
  vehicle_item_id citext not null,
  hash citext not null);
create unique index on greg.uc_base_vehicles(stock_number);
create unique index on greg.uc_base_vehicles(hash);
create index on greg.uc_base_vehicles(vin);
create index on greg.uc_base_vehicles(vehicle_item_id);
create index on greg.uc_base_vehicles(date_acquired);
create index on greg.uc_base_vehicles(sale_date);  
create index on greg.uc_base_vehicles(store_code);
comment on table greg.uc_base_vehicles is 'Base used car identifiers and dates. All vehicles from tool where sale date > 12/31/2011';
comment on column greg.uc_base_vehicles.stock_number is 'from ads::dpsvseries.vehicleinventoryitems.stocknumber';
comment on column greg.uc_base_vehicles.vin is 'from ads::dpsvseries.vehicleitems.vin';
comment on column greg.uc_base_vehicles.date_acquired is 'from ads::dpsvseries.vehicleinventoryitems.fromts';
comment on column greg.uc_base_vehicles.sale_date is 'from ads::dpsvseries.vehiclessales.soldts';
comment on column greg.uc_base_vehicles.vehicle_inventory_item_id is 'from ads::dpsvseries.vehicleinventoryitems.vehicleinventoryitemid';
comment on column greg.uc_base_vehicles.vehicle_item_id is 'from ads::dpsvseries.vehicleitems.vechicleitemid';

insert into greg.uc_base_vehicles
select 
  CASE a.owninglocationid
    WHEN 'B6183892-C79D-4489-A58C-B526DF948B06' THEN 'RY1'
    WHEN '4CD8E72C-DC39-4544-B303-954C99176671' THEN 'RY2'
    WHEN '1B6768C6-03B0-4195-BA3B-767C0CAC40A6' THEN 'RY3'
    ELSE 'XXX'
  END AS store_code,
  a.stocknumber, b.vin, a.fromts::date as date_acquired, 
  case
    when extract(year from d.soldts::date) = 1910 then a.thruts::Date
    else coalesce(d.soldts::date, '12/31/9999'::date) 
  end AS sale_date,
  a.vehicleinventoryitemid, b.vehicleitemid,
  ( -- generate the hash
    select md5(z::text) as hash
    from (
      select 
        CASE trim(aa.owninglocationid)
          WHEN 'B6183892-C79D-4489-A58C-B526DF948B06' THEN 'RY1'
          WHEN '4CD8E72C-DC39-4544-B303-954C99176671' THEN 'RY2'
          WHEN '1B6768C6-03B0-4195-BA3B-767C0CAC40A6' THEN 'RY3'
          ELSE 'XXX'
        END AS store_code,
        aa.stocknumber, bb.vin, aa.fromts::date as date_acquired, 
        case
          when extract(year from dd.soldts::date) = 1910 then aa.thruts::Date
          else coalesce(dd.soldts::date, '12/31/9999'::date) 
        end AS sale_date,
        aa.vehicleinventoryitemid, bb.vehicleitemid
      from ads.ext_vehicle_inventory_items aa
      left join ads.ext_vehicle_items bb on aa.vehicleitemid = bb.vehicleitemid
      left join ads.ext_vehicle_sales dd on aa.vehicleinventoryitemid = dd.vehicleinventoryitemid
        AND dd.status = 'VehicleSale_Sold'
      where aa.vehicleinventoryitemid = a.vehicleinventoryitemid)z)  
from ads.ext_vehicle_inventory_items a
left join ads.ext_vehicle_items b on a.vehicleitemid = b.vehicleitemid
left join ads.ext_vehicle_sales d on a.vehicleinventoryitemid = d.vehicleinventoryitemid
  AND d.status = 'VehicleSale_Sold'
where coalesce(d.soldts::date, '12/31/9999'::date) > '12/31/2011';


select -- new rows
  CASE a.owninglocationid
    WHEN 'B6183892-C79D-4489-A58C-B526DF948B06' THEN 'RY1'
    WHEN '4CD8E72C-DC39-4544-B303-954C99176671' THEN 'RY2'
    WHEN '1B6768C6-03B0-4195-BA3B-767C0CAC40A6' THEN 'RY3'
    ELSE 'XXX'
  END AS store_code,
  a.stocknumber, b.vin, a.fromts::date as date_acquired, 
  case
    when extract(year from d.soldts::date) = 1910 then a.thruts::Date
    else coalesce(d.soldts::date, '12/31/9999'::date) 
  end AS sale_date,
  a.vehicleinventoryitemid, b.vehicleitemid,
  ( -- generate the hash
    select md5(z::text) as hash
    from (
      select 
        CASE trim(aa.owninglocationid)
          WHEN 'B6183892-C79D-4489-A58C-B526DF948B06' THEN 'RY1'
          WHEN '4CD8E72C-DC39-4544-B303-954C99176671' THEN 'RY2'
          WHEN '1B6768C6-03B0-4195-BA3B-767C0CAC40A6' THEN 'RY3'
          ELSE 'XXX'
        END AS store_code,
        aa.stocknumber, bb.vin, aa.fromts::date as date_acquired, 
        case
          when extract(year from dd.soldts::date) = 1910 then aa.thruts::Date
          else coalesce(dd.soldts::date, '12/31/9999'::date) 
        end AS sale_date,
        aa.vehicleinventoryitemid, bb.vehicleitemid
      from ads.ext_vehicle_inventory_items aa
      left join ads.ext_vehicle_items bb on aa.vehicleitemid = bb.vehicleitemid
      left join ads.ext_vehicle_sales dd on aa.vehicleinventoryitemid = dd.vehicleinventoryitemid
        AND dd.status = 'VehicleSale_Sold'
      where aa.vehicleinventoryitemid = a.vehicleinventoryitemid)z)  
from ads.ext_vehicle_inventory_items a
left join ads.ext_vehicle_items b on a.vehicleitemid = b.vehicleitemid
left join ads.ext_vehicle_sales d on a.vehicleinventoryitemid = d.vehicleinventoryitemid
  AND d.status = 'VehicleSale_Sold'
where coalesce(d.soldts::date, '12/31/9999'::date) > '12/31/2011'
  and not exists (
    select 1
    from greg.uc_base_vehicles
    where vehicle_inventory_item_id = a.vehicleinventoryitemid)  
union
select x.*
from (
  select -- changed rows
    CASE a.owninglocationid
      WHEN 'B6183892-C79D-4489-A58C-B526DF948B06' THEN 'RY1'
      WHEN '4CD8E72C-DC39-4544-B303-954C99176671' THEN 'RY2'
      WHEN '1B6768C6-03B0-4195-BA3B-767C0CAC40A6' THEN 'RY3'
      ELSE 'XXX'
    END AS store_code,
    a.stocknumber, b.vin, a.fromts::date as date_acquired, 
    case
      when extract(year from d.soldts::date) = 1910 then a.thruts::Date
      else coalesce(d.soldts::date, '12/31/9999'::date) 
    end AS sale_date,
    a.vehicleinventoryitemid, b.vehicleitemid,
    ( -- generate the hash
      select md5(z::text) as hash
      from (
        select 
          CASE trim(aa.owninglocationid)
            WHEN 'B6183892-C79D-4489-A58C-B526DF948B06' THEN 'RY1'
            WHEN '4CD8E72C-DC39-4544-B303-954C99176671' THEN 'RY2'
            WHEN '1B6768C6-03B0-4195-BA3B-767C0CAC40A6' THEN 'RY3'
            ELSE 'XXX'
          END AS store_code,
          aa.stocknumber, bb.vin, aa.fromts::date as date_acquired, 
          case
            when extract(year from dd.soldts::date) = 1910 then aa.thruts::Date
            else coalesce(dd.soldts::date, '12/31/9999'::date) 
          end AS sale_date,
          aa.vehicleinventoryitemid, bb.vehicleitemid
        from ads.ext_vehicle_inventory_items aa
        left join ads.ext_vehicle_items bb on aa.vehicleitemid = bb.vehicleitemid
        left join ads.ext_vehicle_sales dd on aa.vehicleinventoryitemid = dd.vehicleinventoryitemid
          AND dd.status = 'VehicleSale_Sold'
        where aa.vehicleinventoryitemid = a.vehicleinventoryitemid)z)  
  from ads.ext_vehicle_inventory_items a
  left join ads.ext_vehicle_items b on a.vehicleitemid = b.vehicleitemid
  left join ads.ext_vehicle_sales d on a.vehicleinventoryitemid = d.vehicleinventoryitemid
    AND d.status = 'VehicleSale_Sold'
  where coalesce(d.soldts::date, '12/31/9999'::date) > '12/31/2011') x
inner join greg.uc_base_vehicles xx on x.vehicleinventoryitemid = xx.vehicle_inventory_item_id
  and x.hash <> xx.hash  


------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------

-- 9/10/17 start over using financial statement as base for history
-- don't yet know what current inventory looks like, probably inpmast (arkona.xfm_inpmast)

-- from python projects/sales_pay_plan/sql/financial_statement_unit_count.sql
-- sold vehicles thru 8/31/2017
drop table if exists step_1;
create temp table step_1 as
-- include stocknumber on each row
select year_month, store, page, line, line_label, control, the_date, sum(unit_count) as unit_count
from ( -- h
  select b.year_month, d.store, d.page, d.line, d.line_label, d.description, d.gl_account, 
    a.control, a.amount,
    case when a.amount < 0 then 1 else -1 end as unit_count, b.the_date
  from fin.fact_gl a   
  inner join dds.dim_date b on a.date_key = b.date_key
    and b.year_month between 201201 and 201708
  inner join fin.dim_account c on a.account_key = c.account_key
  inner join ( -- d: fs gm_account page/line/acct description
    select b.year_month, f.store, d.gl_account, b.page, b.line, b.line_label, e.description
    from fin.fact_fs a
    inner join fin.dim_fs b on a.fs_key = b.fs_key
      and b.year_month = 201708
      and b.page = 16 
      and b.line between 1 and 14
    inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
    inner join fin.dim_account e on d.gl_account = e.account
      and e.account_type_code = '4'
    inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key) d on c.account = d.gl_account
  inner join fin.dim_journal aa on a.journal_key = aa.journal_key 
    and aa.journal_code in ('VSN','VSU')   
    where a.post_status = 'Y') h
group by year_month, store, page, line, line_label, control, the_date
order by year_month, store, page, line;

create index on step_1(control);

-- this looks reasonably good
drop table if exists step_2;
create temp table step_2 as
select a.year_month, a.the_date as acct_sale_date, a.store as store_code, a.control
from step_1 a
where a.unit_count <> 0
group by a.year_month, a.store, a.control, a.the_date
having sum(unit_count) = 1;
create index on step_2(year_month);
create index on step_2(store_code);
create index on step_2(control);

--</ in accounting missing from tool ---------------------------------------------------------------------------------------
select a.*, c.* -- 82 not in tool by stocknumber
from step_2 a
left join ads.ext_vehicle_inventory_items b on a.control = b.stocknumber
left join sls.deals c on a.control = c.stock_number
where b.stocknumber is null
order by a.year_month

-- fixed some stocknumbers in tool, using vin from inpmast
select a.*, c.*-- 82 not in tool by stocknumber
from step_2 a
left join ads.ext_vehicle_inventory_items b on a.control = b.stocknumber
left join arkona.xfm_inpmast c on a.control = c.inpmast_stock_number
  and c.current_row = true
where b.stocknumber is null


some can be dealt with as in/out/lease purchase where acq date = sale date, 
the rest,
exclude non auto stuff: kawasaki,yamaha,polaris, etc
whatever is left, just fake as far as statuses/prices


--/> in accounting missing from tool ---------------------------------------------------------------------------------------

-- multiple instances of stock numbers, the year month is an accounting date of sale
select a.*
from step_2 a
inner join (
  select b.control
  from step_2 b
  group by b.control
  having count(*) > 1) c on a.control = c.control
order by a.control


ok, dates, 
yuck
the problem with using step_2 is that it is sold vehicles only, not current inventory
-- from /Desktop/sql/ucinv/base_vehicle_dates.sql
-- tool
select a.*, b.fromts::date as vii_from, coalesce(b.thruts, '12/31/9999')::date as vii_thru
from step_2 a
left join ads.ext_vehicle_inventory_items b on a.control = b.stocknumber

-- -- -- what is interesting here, 31135c shows a from date, but these should all be the ones that do not
-- -- -- exist in the tool, but those with dates are backons that are currently in inventory
-- -- select *
-- --   from (
-- --   select a.*, b.fromts::date as vii_from, coalesce(b.thruts, '12/31/9999')::date as vii_thru
-- --   from step_2 a
-- --   left join ads.ext_vehicle_inventory_items b on a.control = b.stocknumber) x 
-- -- where vii_thru > current_Date


-- so, current inventory
inpmast: status I
fact_gl: sum of inventory account > 0

-- inpmast -- 486 rows
drop table if exists inpmast;
create temp table inpmast as
select a.inpmast_stock_number, a.inpmast_vin, 
  case
    when a.date_in_invent = 0 then null
    else
      (left(a.date_in_invent::text,4) || '-' || 
        substring(a.date_in_invent::text from 5 for 2) || '-' || 
        substring(a.date_in_invent::text from 7 for 2))::date 
  end as date_in_invent
from arkona.xfm_inpmast a
where a.status = 'I'
  and a.type_n_u = 'U'
  and a.current_row = true
order by a.inpmast_stock_number

select * from inpmast order by inpmast_stock_number
  
-- 484 rows, some are obviously not cars: control 21, HGJ70812
create temp table fact_gl as
select a.control, c.account, sum(a.amount)
from fin.fact_gl a
inner join fin.dim_account c on a.account_key = c.account_key
where a.post_status = 'Y'
  and account in (
  select account
  from fin.dim_account
  where department_code = 'uc'
    and account_type = 'asset'
    and description not like '%PAC%') 
group by a.control, c.account
having sum(amount) > 0

select * from fact_gl

-- current inventory inpmast vs fact_gl
-- many of the diffs are honda deals not yet processed, including WSIM
select *
from inpmast a
full outer join fact_gl b on a.inpmast_stock_number = b.control
where a.inpmast_stock_number is null
  or b.control is null


select a.*, b.the_date, c.*
from fin.fact_gl a
inner join dds.dim_Date b on a.date_key = b.date_key
inner join fin.dim_account c on a.account_key = c.account_key
where a.post_status = 'Y'
  and account in (
  select account
  from fin.dim_account
  where department_code = 'uc'
    and account_type = 'asset') 
and control = '31272b'     
order by b.the_date

-- tool vs inpmast
select *
from ( -- tool 468 
  select a.stocknumber, 'bin', a.fromts::date as vii_from
  from ads.ext_vehicle_inventory_items a
--   inner join ads.ext_vehicle_items b on a.vehicleitemid = b.vehicleitemid
  where a.thruts > now()) a
full outer join inpmast b on a.stocknumber = b.inpmast_stock_number
where a.stocknumber is null 
  or b.inpmast_stock_number is null 


create temp table play as
    select aa.acct_sale_date, a.*, 
      sum(amount) over (partition by a.control order by a.control, the_date) as total
    from greg.uc_base_vehicle_accounting a
    inner join step_2 aa on a.control = aa.control
    where a.page = 1
-- sale date vs max acct date
select a.*,
  max(the_date) over (partition by control)
from play a
order by control, the_date
offset 100000
limit 5000


-- getting distracted
-- ok, focus, date acquired for sold vehicles.
-- 1.
-- from /Desktop/sql/ucinv/base_vehicle_dates.sql
-- tool, inpmast, fact_gl
drop table if exists acq_dates;
create temp table acq_dates as
select a.year_month, a.acct_sale_date, a.store_code, a.control, bb.vin, b.fromts::date as vii_from, 
  case
    when c.date_in_invent = 0 then null
    else
      (left(c.date_in_invent::text,4) || '-' || 
        substring(c.date_in_invent::text from 5 for 2) || '-' || 
        substring(c.date_in_invent::text from 7 for 2))::date 
  end as date_in_invent_stk, 
  case
    when d.date_in_invent = 0 then null
    else
      (left(d.date_in_invent::text,4) || '-' || 
        substring(d.date_in_invent::text from 5 for 2) || '-' || 
        substring(d.date_in_invent::text from 7 for 2))::date 
  end as date_in_invent_vin,
  acct_acq_date 
from step_2 a
left join ads.ext_vehicle_inventory_items b on a.control = b.stocknumber
left join ads.ext_vehicle_items bb on b.vehicleitemid = bb.vehicleitemid
left join arkona.ext_inpmast c on a.control = c.inpmast_stock_number
left join arkona.ext_inpmast d on bb.vin = d.inpmast_vin
left join (-- accounting acquisition date
  select control, min(the_date) as acct_acq_date
  from (
    select aa.acct_sale_date, a.*, 
      sum(amount) over (partition by a.control order by a.control, the_date) as total
    from greg.uc_base_vehicle_accounting a
    inner join step_2 aa on a.control = aa.control
    where a.page = 1) x
  group by control) e on a.control = e.control;
  
create index on acq_dates(control);
create index on acq_dates(vii_from);
create index on acq_dates(date_in_invent_stk);
create index on acq_dates(date_in_invent_vin);
create index on acq_dates(acct_acq_date);

drop table if exists t1;
create temp table t1 as
select a.*
from acq_dates a
left join ( -- 12325, all 3 dates are the same
  select b.*
  from acq_dates b
  where (b.vii_from = b.date_in_invent_stk or b.vii_from = b.date_in_invent_vin)
    and b.vii_from = b.acct_acq_date) c on a.control = c.control
where c.control is null;  

select x.*,
  count(*) over (partition by x.diff), 
  date_in_invent_stk = acct_acq_date
from ( 
  select a.*, abs(a.vii_from - a.acct_acq_date) as diff
  from t1 a 
  order by abs(a.vii_from - a.acct_acq_date)) x

drop table if exists t2;
create temp table t2 as
select c.*
from t1 c
left join ( --7965 abs(a.vii_from - a.acct_acq_date) < 6 or inpmast = acct
  select a.*
  from t1 a 
  where abs(a.vii_from - a.acct_acq_date) < 6
    or date_in_invent_stk = acct_acq_date
    or date_in_invent_vin = acct_acq_date) d on c.control = d.control
where d.control is null;  

-- this leaves 477 rows with ambiguous acq dates
select *
from t2
where vii_from  = date_in_invent_stk
  or vii_from = date_in_invent_vin

-- sold before tool acq: mostly intra market vehicles (g,x)
select *
from acq_dates
where vii_from > acct_sale_date  

-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--</ 9/12 ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
/*
ok, each day restate the necessary prerequisites and summarize state
*/
-- step 1, from fact_gl, uc sold 201201 thru 201708
-- 1 row per control/date/unit_count
drop table if exists step_1;
create temp table step_1 as
-- include stocknumber on each row
select year_month, store, page, line, line_label, control, the_date, sum(unit_count) as unit_count
from ( -- h
  select b.year_month, d.store, d.page, d.line, d.line_label, d.description, d.gl_account, 
    a.control, a.amount,
    case when a.amount < 0 then 1 else -1 end as unit_count, b.the_date
  from fin.fact_gl a   
  inner join dds.dim_date b on a.date_key = b.date_key
    and b.year_month between 201201 and 201708
  inner join fin.dim_account c on a.account_key = c.account_key
  inner join ( -- d: fs gm_account page/line/acct description
    select b.year_month, f.store, d.gl_account, b.page, b.line, b.line_label, e.description
    from fin.fact_fs a
    inner join fin.dim_fs b on a.fs_key = b.fs_key
      and b.year_month = 201708
      and b.page = 16 
      and b.line between 1 and 14
    inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
    inner join fin.dim_account e on d.gl_account = e.account
      and e.account_type_code = '4'
    inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key) d on c.account = d.gl_account
  inner join fin.dim_journal aa on a.journal_key = aa.journal_key 
    and aa.journal_code in ('VSN','VSU')   
    where a.post_status = 'Y') h
group by year_month, store, page, line, line_label, control, the_date
order by year_month, store, page, line;
create unique index on step_1(control,the_date,unit_count);
create index on step_1(control);

-- step_2
-- narrows down step_1 to 1 row per control/date
-- assert that the date is the sale date per accounting
drop table if exists step_2;
create temp table step_2 as
select a.year_month, a.the_date as acct_sale_date, a.store as store_code, a.control
from step_1 a
where a.unit_count <> 0
group by a.year_month, a.store, a.control, a.the_date
having sum(unit_count) = 1;
-- anomaly persisted  because of different dates, fix it here
delete 
from step_2
where store_code = 'ry2'
  and control in ('17827X','17977X','18372X');

create unique index on step_2(control, acct_sale_date);
create index on step_2(year_month);
create index on step_2(store_code);
create index on step_2(control);

-- not in tool
-- these will require separate processing, 
drop table if exists not_in_tool;
create temp table not_in_tool as
select a.* -- 62 not in tool by stocknumber
from step_2 a
left join ads.ext_vehicle_inventory_items b on a.control = b.stocknumber
where b.stocknumber is null;

-- ok, focus, date acquired for sold vehicles.
-- 1.
-- from /Desktop/sql/ucinv/base_vehicle_dates.sql
-- includes acq dates from tool, inpmast, fact_gl
-- 1 row per control acct_sale_date
drop table if exists acq_dates;
create temp table acq_dates as
select a.year_month, a.acct_sale_date, a.store_code, a.control, bb.vin, b.fromts::date as vii_from, 
  case
    when c.date_in_invent = 0 then null
    else
      (left(c.date_in_invent::text,4) || '-' || 
        substring(c.date_in_invent::text from 5 for 2) || '-' || 
        substring(c.date_in_invent::text from 7 for 2))::date 
  end as date_in_invent_stk, 
  case
    when d.date_in_invent = 0 then null
    else
      (left(d.date_in_invent::text,4) || '-' || 
        substring(d.date_in_invent::text from 5 for 2) || '-' || 
        substring(d.date_in_invent::text from 7 for 2))::date 
  end as date_in_invent_vin,
  acct_acq_date 
from step_2 a
left join ads.ext_vehicle_inventory_items b on a.control = b.stocknumber
left join ads.ext_vehicle_items bb on b.vehicleitemid = bb.vehicleitemid
left join arkona.xfm_inpmast c on a.control = c.inpmast_stock_number
  and c.current_row = true
left join arkona.xfm_inpmast d on bb.vin = d.inpmast_vin
  and d.current_row = true
left join (-- accounting acquisition date
  select control, min(the_date) as acct_acq_date
  from (
    select aa.acct_sale_date, a.*, 
      sum(amount) over (partition by a.control order by a.control, the_date) as total
    from greg.uc_base_vehicle_accounting a
    inner join step_2 aa on a.control = aa.control
    where a.page = 1) x
  group by control) e on a.control = e.control
left join not_in_tool f on a.control = f.control
where f.control is null;
create unique index on acq_dates(control, acct_sale_date);
create index on acq_dates(control);
create index on acq_dates(vii_from);
create index on acq_dates(date_in_invent_stk);
create index on acq_dates(date_in_invent_vin);
create index on acq_dates(acct_acq_date);

-- probably good enuf for acquisition dates on sold vehicles
select x.*
from (
  select a.*,
    case 
      when (a.vii_from = a.acct_acq_date -- all dates equal
        and (a.vii_from = a.date_in_invent_stk or a.vii_from = a.date_in_invent_vin)) then vii_from
      when abs(a.vii_from - a.acct_acq_date) < 6 then acct_acq_date -- tool within 5 days of accounting
      when a.date_in_invent_stk = a.acct_acq_date 
        or a.date_in_invent_vin = a.acct_acq_date then a.acct_acq_date -- inpmast = accounting
      when a.acct_sale_date = a.acct_acq_date -- sale date = acq date
        or a.acct_sale_date = a.vii_from
        or a.acct_sale_date = a.date_in_invent_stk
        or a.acct_sale_Date = a.date_in_invent_vin then a.acct_sale_date
      when a.vii_from = a.date_in_invent_stk or a.vii_from = a.datE_in_invent_vin then a.vii_from
      -- down to 213 - going arbitrary
      when abs(a.vii_from - a.acct_acq_date) < 21 then acct_acq_date
      else vii_from
    end as acq_date
  from acq_dates a) x
-- where acq_date is null 
order by control


-- let's look at sale dates now
-- include the acq dates -------------------------------------------------------

since i am going with acct_sale_date be default, do not need either multiple_sales or unwind_unsold
at least for now
-- -- -- -- multiple sales on a stocknumber appears to be a separate case, neither inpmast nor bopmast will
-- -- -- -- show these, only the tool and accounting, so, for the initial cut of sale_dates, exclude 
-- -- -- -- stocknumbers with multiple sales
-- -- -- 
-- -- -- drop table if exists multiple_sales;
-- -- -- create temp table multiple_sales as
-- -- -- select a.control
-- -- -- from step_2 a
-- -- -- group by a.control
-- -- -- having count(*) > 1;
-- -- -- create unique index on multiple_sales(control);
-- -- -- 
-- -- -- -- need to exclude current inventory, if sold, unwound and still in inventory
-- -- -- -- there will be and accounting sale date, but vii and inp will be null,
-- -- -- -- since i am dealing with sales thru 8/31, this needs to accomodate that
-- -- -- drop table if exists unwind_unsold;
-- -- -- create temp table unwind_unsold as
-- -- -- select a.control
-- -- -- from step_2 a
-- -- -- left join not_in_tool b on a.control = b.control
-- -- -- inner join ads.ext_vehicle_inventory_items c on a.control = c.stocknumber
-- -- --   and c.thruts::date > '08/31/2017'
-- -- -- where b.control is null;

-- the conclusion to all this was, fuck it, go with the acct_sale_date
-- -- drop table if exists sale_dates;
-- -- create temp table sale_dates as
-- -- select a.year_month, a.acct_sale_date, a.store_code, a.control,
-- --   bb.vin, b.thruts::date as vii_thru, 
-- --   case
-- --     when c.date_delivered = 0 then null
-- --     else
-- --       (left(c.date_delivered::text,4) || '-' || 
-- --         substring(c.date_delivered::text from 5 for 2) || '-' || 
-- --         substring(c.date_delivered::text from 7 for 2))::date 
-- --   end as inp_stk,   
-- --   case
-- --     when d.date_delivered = 0 then null
-- --     else
-- --       (left(d.date_delivered::text,4) || '-' || 
-- --         substring(d.date_delivered::text from 5 for 2) || '-' || 
-- --         substring(d.date_delivered::text from 7 for 2))::date 
-- --   end as inp_vin,    
-- --   e.delivery_date as bopmast
-- -- --   g.acq_date
-- -- from step_2 a
-- -- left join ads.ext_vehicle_inventory_items b on a.control = b.stocknumber
-- -- left join ads.ext_vehicle_items bb on b.vehicleitemid = bb.vehicleitemid
-- -- left join arkona.xfm_inpmast c on a.control = c.inpmast_stock_number
-- --   and c.current_row = true
-- --   and c.status = 'C'
-- -- left join arkona.xfm_inpmast d on bb.vin = d.inpmast_vin
-- --   and d.current_row = true
-- --   and d.status = 'C'
-- -- left join (
-- --   select bopmast_id, stock_number, vin, vehicle_type, sale_type,
-- --     buyer_bopname_id, capped_date, delivery_date
-- --   from sls.ext_bopmast_partial
-- --   where left(stock_number, 1) <> 'C'
-- --     and stock_number not in ('13744XX','17161A','18227XA','21081A','22500BB','23055A','23991A')) e on a.control = e.stock_number
-- -- left join not_in_tool f on a.control = f.control
-- -- left join multiple_sales g on a.control = g.control
-- -- left join unwind_unsold h on a.control = h.control
-- -- where f.control is null -- exclude not_in_tool
-- --   and g.control is null -- exclude multiple_sales
-- --   and h.control is null -- exclude unwinds still in inventory
-- --   and bb.bodystyle not like '%motor%'
-- --   and bb.model <> 'Other'  
-- -- 
-- -- -- -- aha, sales_dates generation query creates some multiples (7 of them)
-- -- -- select control from sale_dates a group by control having count(*) >  1
-- -- -- -- fixed in above query, exclude relevant stocknumbers
-- -- 
-- -- select x.*, abs(acct_sale_date - vii_thru) as tool, abs(acct_sale_date - bopmast) as bopmast,
-- --   abs(acct_sale_date - inp_stk) as inp_stk, abs(acct_sale_date - inp_vin) as inp_vin
-- -- from (
-- --   select a.*, 
-- -- --     case 
-- -- --       when a.acct_sale_date = a.vii_thru -- all dates equal
-- -- --         and a.acct_sale_date = a.bopmast
-- -- --         and (a.acct_sale_date = a.inp_stk or a.acct_sale_date = a.inp_vin) then a.acct_sale_date
-- -- --       when a.acct_sale_date = a.vii_thru -- acct = vii = bopmast
-- -- --         and a.acct_sale_date = a.bopmast then a.acct_sale_Date
-- -- --       when a.acct_sale_date = a.bopmast -- acct = inpmast = bopmast
-- -- --         and (a.acct_sale_date = a.inp_stk or a.acct_sale_date = a.inp_vin) then a.acct_sale_date
-- -- --       when a.acct_sale_date = a.vii_thru -- acct = vii = inpmast
-- -- --         and (a.acct_sale_date = a.inp_stk or a.acct_sale_date = a.inp_vin) then a.acct_sale_date
-- -- --       when a.acct_sale_date = a.vii_thru then a.acct_sale_date -- acct = vii
-- -- --       when abs(a.vii_thru - a.acct_sale_date) < 11 then a.acct_sale_date
-- -- --       when abs(a.acct_sale_date - a.bopmast) < 20 then a.acct_sale_date
-- --       -- down to 92
-- --       -- fuckit
-- -- --       else 
-- -- --     end as sale_date
-- --   from sale_dates a) x
-- --  where x.sale_date is null --and (acct_sale_date = bopmast or acct_sale_date = vii_thru or acct_sale_date = inp_stk)
-- -- --    and x.control in ('23505a','h7745g')
-- -- order by abs(acct_sale_date - bopmast)


-- reference /intranet sqlscripts/crayon report/postgres/ucinv_potentia_gross.sql
-- determine acq date by journal
select a.year_month, a.acct_sale_date, a.store_code, a.control, min(c.the_date) as acq_date
from not_in_tool a
left join greg.uc_base_vehicle_accounting c on a.control = c.control
  and c.journal_code in ('PVU', 'VSN', 'PVI', 'VSU')
group by a.year_month, a.acct_sale_date, a.store_code, a.control;


-- multiple sales
select a.*, c.*
from step_2 a
inner join multiple_sales aa on a.control = aa.control
left join ads.ext_vehicle_inventory_items b on a.control = b.stocknumber
left join ads.ext_vehicle_sales c on b.vehicleinventoryitemid = c.vehicleinventoryitemid
  and a.acct_sale_date between  c.soldts::date -2 and c.soldts::date + 2
order by a.control

select x.*, ark_array = tool_array
from (
  select a.store_code, a.control, 
    array_agg(a.acct_sale_date) as ark_array, array_agg(c.soldts::date) as tool_array,
    string_agg(a.acct_sale_date::text, ',' order by a.acct_sale_date) acct, 
    string_agg(c.soldts::date::text, ',' order by c.soldts::date) as tool
  from step_2 a
  inner join multiple_sales aa on a.control = aa.control
  left join ads.ext_vehicle_inventory_items b on a.control = b.stocknumber
  left join ads.ext_vehicle_sales c on b.vehicleinventoryitemid = c.vehicleinventoryitemid
    and a.acct_sale_date between  c.soldts::date -2 and c.soldts::date + 2
  group by a.store_code, a.control) x
order by ark_array = tool_array

single sales
h5147a 



-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--/> 9/12 -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--</ 9/13 ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
/*
uh oh, compared end result to greg.uc_base_vehicles and missing like 600 vehicles, fuck
the issue is in the account generation, fell into the same old trap of basing it on a
single financial statement, which thereby leaves out any accounts for which there were no
sales in that month
*/
-- step 1, from fact_gl, uc sold 201201 thru 201708
-- 1 row per control/date/unit_count
drop table if exists step_1;
create temp table step_1 as
-- include stocknumber on each row
select year_month, store_code, control, the_date, sum(unit_count) as unit_count
from ( -- h
  select b.year_month, c.store_code, d.g_l_acct_number, 
    a.control, a.amount,
    case when a.amount < 0 then 1 else -1 end as unit_count, b.the_date
  from fin.fact_gl a   
  inner join dds.dim_date b on a.date_key = b.date_key
    and b.year_month between 201201 and 201708
  inner join fin.dim_account c on a.account_key = c.account_key
    and c.account_type = 'Sale'
  inner join (
    select distinct g_l_acct_number
    from arkona.ext_eisglobal_sypffxmst a
    inner join (
      select factory_financial_year, g_l_acct_number, factory_account, fact_account_
      from arkona.ext_ffpxrefdta) b on a.fxmcyy = b.factory_financial_year and a.fxmact = b.factory_account
    where a.fxmcyy = 2017
      and fxmpge = 16
      and fxmlne between 1 and 14) d on c.account = d.g_l_acct_number
  inner join fin.dim_journal aa on a.journal_key = aa.journal_key 
    and aa.journal_code in ('VSN','VSU')   
  where a.post_status = 'Y') h
group by year_month, store_code, control, the_date;
create unique index on step_1(control,the_date,unit_count);
create index on step_1(control);



-- step_2
-- narrows down step_1 to 1 row per control/date
-- assert that the date is the sale date per accounting
drop table if exists step_2;
create temp table step_2 as
select a.year_month, a.the_date as acct_sale_date, a.store_code, a.control
from step_1 a
where a.unit_count <> 0
group by a.year_month, a.store_code, a.control, a.the_date
having sum(unit_count) > 0;
-- anomaly persisted  because of different dates, fix it here
delete 
from step_2
where store_code = 'ry2'
  and control in ('17827X','17977X','18372X','17980X');

create unique index on step_2(control, acct_sale_date);
create index on step_2(year_month);
create index on step_2(store_code);
create index on step_2(control);



-- not in tool
-- these will require separate processing, 
drop table if exists not_in_tool;
create temp table not_in_tool as
select a.* -- 62 not in tool by stocknumber
from step_2 a
left join ads.ext_vehicle_inventory_items b on a.control = b.stocknumber
where b.stocknumber is null;


-- from /Desktop/sql/ucinv/base_vehicle_dates.sql
-- includes acq dates from tool, inpmast, fact_gl
-- 1 row per control acct_sale_date
-- excludes not_in_tool
drop table if exists acq_dates;
create temp table acq_dates as
select a.year_month, a.acct_sale_date, a.store_code, a.control, bb.vin, b.fromts::date as vii_from, 
  case
    when c.date_in_invent = 0 then null
    else
      (left(c.date_in_invent::text,4) || '-' || 
        substring(c.date_in_invent::text from 5 for 2) || '-' || 
        substring(c.date_in_invent::text from 7 for 2))::date 
  end as date_in_invent_stk, 
  case
    when d.date_in_invent = 0 then null
    else
      (left(d.date_in_invent::text,4) || '-' || 
        substring(d.date_in_invent::text from 5 for 2) || '-' || 
        substring(d.date_in_invent::text from 7 for 2))::date 
  end as date_in_invent_vin,
  acct_acq_date 
from step_2 a
left join ads.ext_vehicle_inventory_items b on a.control = b.stocknumber
left join ads.ext_vehicle_items bb on b.vehicleitemid = bb.vehicleitemid
left join arkona.xfm_inpmast c on a.control = c.inpmast_stock_number
  and c.current_row = true
left join arkona.xfm_inpmast d on bb.vin = d.inpmast_vin
  and d.current_row = true
left join (-- accounting acquisition date
  select control, min(the_date) as acct_acq_date
  from (
    select aa.acct_sale_date, a.*, 
      sum(amount) over (partition by a.control order by a.control, the_date) as total
    from greg.uc_base_vehicle_accounting a
    inner join step_2 aa on a.control = aa.control
    where a.page = 1) x
  group by control) e on a.control = e.control
left join not_in_tool f on a.control = f.control
where f.control is null;
create unique index on acq_dates(control, acct_sale_date);
create index on acq_dates(control);
create index on acq_dates(vii_from);
create index on acq_dates(date_in_invent_stk);
create index on acq_dates(date_in_invent_vin);
create index on acq_dates(acct_acq_date);


-- probably good enuf for acquisition dates on sold vehicles
-- this query used to derive the acq_dates
-- -- select x.*
-- -- from (
-- --   select a.*,
-- --     case 
-- --       when (a.vii_from = a.acct_acq_date -- all dates equal
-- --         and (a.vii_from = a.date_in_invent_stk or a.vii_from = a.date_in_invent_vin)) then vii_from
-- --       when abs(a.vii_from - a.acct_acq_date) < 6 then acct_acq_date -- tool within 5 days of accounting
-- --       when a.date_in_invent_stk = a.acct_acq_date 
-- --         or a.date_in_invent_vin = a.acct_acq_date then a.acct_acq_date -- inpmast = accounting
-- --       when a.acct_sale_date = a.acct_acq_date -- sale date = acq date
-- --         or a.acct_sale_date = a.vii_from
-- --         or a.acct_sale_date = a.date_in_invent_stk
-- --         or a.acct_sale_Date = a.date_in_invent_vin then a.acct_sale_date
-- --       when a.vii_from = a.date_in_invent_stk or a.vii_from = a.datE_in_invent_vin then a.vii_from
-- --       -- down to 213 - going arbitrary
-- --       when abs(a.vii_from - a.acct_acq_date) < 21 then acct_acq_date
-- --       else vii_from
-- --     end as acq_date
-- --   from acq_dates a) x
-- -- -- where acq_date is null 
-- -- order by control

drop table if exists acq_dates_2;
create temp table acq_dates_2 as
select a.year_month, a.acct_sale_date, a.store_code, a.control, 
  case 
    when (a.vii_from = a.acct_acq_date -- all dates equal
      and (a.vii_from = a.date_in_invent_stk or a.vii_from = a.date_in_invent_vin)) then vii_from
    when abs(a.vii_from - a.acct_acq_date) < 6 then acct_acq_date -- tool within 5 days of accounting
    when a.date_in_invent_stk = a.acct_acq_date 
      or a.date_in_invent_vin = a.acct_acq_date then a.acct_acq_date -- inpmast = accounting
    when a.acct_sale_date = a.acct_acq_date -- sale date = acq date
      or a.acct_sale_date = a.vii_from
      or a.acct_sale_date = a.date_in_invent_stk
      or a.acct_sale_Date = a.date_in_invent_vin then a.acct_sale_date
    when a.vii_from = a.date_in_invent_stk or a.vii_from = a.datE_in_invent_vin then a.vii_from
    -- down to 213 - going arbitrary
    when abs(a.vii_from - a.acct_acq_date) < 21 then a.acct_acq_date
    else a.vii_from
  end as acq_date
from acq_dates a;


-- acq_date for not_in_tool
-- reference /intranet sqlscripts/crayon report/postgres/ucinv_potentia_gross.sql
-- determine acq date by journal
-- -- select a.year_month, a.acct_sale_date, a.store_code, a.control, min(c.the_date) as acq_date
-- -- from not_in_tool a
-- -- left join greg.uc_base_vehicle_accounting c on a.control = c.control
-- --   and c.journal_code in ('PVU', 'VSN', 'PVI', 'VSU')
-- -- group by a.year_month, a.acct_sale_date, a.store_code, a.control;


drop table if exists step_3;
create temp table step_3 as
select a.*, coalesce(b.acq_date, c.acq_date) as acq_Date
from step_2 a
left join acq_dates_2 b on a.control = b.control
  and a.acct_sale_date = b.acct_sale_date
left join ( -- acq_date for not in tool
  select a.year_month, a.acct_sale_date, a.store_code, a.control, min(c.the_date) as acq_date
  from not_in_tool a
  left join greg.uc_base_vehicle_accounting c on a.control = c.control
    and c.journal_code in ('PVU', 'VSN', 'PVI', 'VSU')
  group by a.year_month, a.acct_sale_date, a.store_code, a.control) c on a.control = c.control and a.acct_sale_date = c.acct_sale_date;
create unique index on step_3(control, acct_sale_date);


create schema uc;
comment on schema uc is 'destination for refactoring of greg.uc_* tables';

-- -- fix constraint
update step_3
set acq_date = '08/07/2013'
-- select * from step_3
where control = '20529X'

-- similar to ucinv_tmp_avail_1
drop table if exists uc.base_vehicles cascade;
create table uc.base_vehicles (
  store_code citext not null,
  stock_number citext primary key,
  vin citext not null,
  date_acquired date not null,
  sale_date date not null,
  vehicle_inventory_item_id citext not null,
  vehicle_item_id citext not null,
  hash citext not null
  check (date_acquired <= sale_date));
create unique index on uc.base_vehicles(hash);
create index on uc.base_vehicles(vin);
create index on uc.base_vehicles(vehicle_item_id);
create index on uc.base_vehicles(date_acquired);
create index on uc.base_vehicles(sale_date);  
create index on uc.base_vehicles(store_code);
comment on table uc.base_vehicles is 'Base used car identifiers and dates. All vehicles from accounting where sale date > 12/31/2011';
comment on column uc.base_vehicles.stock_number is 'from ads::dpsvseries.vehicleinventoryitems.stocknumber';
comment on column uc.base_vehicles.vin is 'from ads::dpsvseries.vehicleitems.vin';
comment on column uc.base_vehicles.date_acquired is 'from a combination of tool, inpmast, and accounting';
comment on column uc.base_vehicles.sale_date is 'from fin::fact_gl';
comment on column uc.base_vehicles.vehicle_inventory_item_id is 'from ads::dpsvseries.vehicleinventoryitems.vehicleinventoryitemid';
comment on column uc.base_vehicles.vehicle_item_id is 'from ads::dpsvseries.vehicleitems.vechicleitemid';


insert into uc.base_vehicles --21076 rows
select a.store_code, a.control, coalesce(c.vin, d.inpmast_vin, 'none') as vin,
  a.acq_date, max(a.acct_sale_date) as sale_date, 
  coalesce(b.vehicleinventoryitemid, 'none') as vehicle_inventory_item_id,
  coalesce(b.vehicleitemid, 'none') as vehicle_item_id,
  ( -- generate the hash
    select md5(z::text) as hash
    from (
      select aa.store_code, aa.control, coalesce(cc.vin, dd.inpmast_vin, 'none') as vin,
        aa.acq_date, max(aa.acct_sale_date) as sale_date, 
        coalesce(bb.vehicleinventoryitemid, 'none') as vehicle_inventory_item_id,
        coalesce(bb.vehicleitemid, 'none') as vehicle_item_id
      from step_3 aa
      left join ads.ext_vehicle_inventory_items bb on aa.control = bb.stocknumber
      left join ads.ext_vehicle_items cc on bb.vehicleitemid = cc.vehicleitemid
      left join arkona.xfm_inpmast dd on aa.control = dd.inpmast_stock_number
      where aa.control = a.control
      group by aa.store_code, aa.control, coalesce(cc.vin, dd.inpmast_vin, 'none'),
        aa.acq_date, coalesce(bb.vehicleinventoryitemid, 'none'),
        coalesce(bb.vehicleitemid, 'none')) z)    
from step_3 a
left join ads.ext_vehicle_inventory_items b on a.control = b.stocknumber
left join ads.ext_vehicle_items c on b.vehicleitemid = c.vehicleitemid
left join arkona.xfm_inpmast d on a.control = d.inpmast_stock_number
group by a.store_code, a.control, coalesce(c.vin, d.inpmast_vin, 'none'),
  a.acq_date, coalesce(b.vehicleinventoryitemid, 'none'),
  coalesce(b.vehicleitemid, 'none');


-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--/> 9/13 ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- todo: not in tool, multiple sales, 
-- unwinds, not sure what i mean, but unwind in same month should be no sale
-- current inventory
-- unwinds in inventory, status will have raw materials after raw materials sold/delivered: 31202b: sold in august, unw in sep
-- base_vehicles: check constraint: acq_date <= sale_date

-- fact_gl : used cars
select control, b.the_date, c.account, c.description, d.journal_code, a.amount, e.description, 
  account_type_code, account_type, department
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
inner join fin.dim_account c on a.account_key = c.account_key
inner join fin.dim_journal d on a.journal_key = d.journal_key
inner join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key
where a.post_status = 'Y'
  and control in ('28194XX')
order by control, the_date