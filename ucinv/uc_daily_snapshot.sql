﻿
-- vehicles
-- all the attributes
drop table if exists vehicles;
create temp table vehicles as
select a.store_code, a.stock_number, a.vin, a.date_acquired, a.sale_date,
  b.make, b.model, b.yearmodel, b.bodystyle, b.trim, b.interiorcolor, 
  b.exteriorcolor, b.engine, b.transmission, 
  CASE
    WHEN BodyStyle LIKE  '%King%'
      OR BodyStyle LIKE '%Access%'
      OR BodyStyle LIKE '%Ext Cab%'
      OR BodyStyle LIKE '%Supercab%'
      or BodyStyle like '%super cab%'
      OR BodyStyle LIKE '%Club%'
      OR BodyStyle LIKE '%xtra%'
      OR BodyStyle LIKE '%ext%'
      OR BodyStyle LIKE '%Double%' THEN 'Extended Cab'
    WHEN BodyStyle LIKE '%Reg%' THEN 'Regular Cab'
    WHEN BodyStyle LIKE '%crew%'
      OR BodyStyle LIKE '%quad%'
      OR BodyStyle LIKE 'mega%' THEN 'Crew Cab'
  END::citext AS Cab,
  CASE
    WHEN BodyStyle LIKE '%AWD%' THEN 'AWD'
    WHEN BodyStyle LIKE '%FWD%' THEN 'FWD'
    WHEN BodyStyle LIKE '%4WD%' OR BodyStyle LIKE '%4X4%' THEN '4WD'
  END::citext AS drive,  
  c.shape, c.size, c.shape_size, 
  coalesce(right(d.typ, length(d.typ) - position('_' in d.typ)), 'N/A') as sale_type,
  f.miles,
  e.status as sold_from_status,
  a.vehicle_inventory_item_id
from greg.uc_base_vehicles a
left join ads.ext_vehicle_items b on a.vehicle_item_id = b.vehicleitemid
left join greg.uc_makes_models c on b.make = c.make
  and b.model = c.model
left join ads.ext_vehicle_sales d on a.vehicle_inventory_item_id = d.vehicleinventoryitemid
  and d.typ = 'VehicleSale_Sold'
left join greg.uc_base_Vehicle_statuses e on a.vehicle_inventory_item_id = e.vehicle_inventory_item_id
  and a.sale_date = e.the_date
left join greg.uc_base_vehicle_miles f on a.vehicle_inventory_item_id = f.vehicle_inventory_item_id;


-- -- -- creates a priced_thru attribute for tracking days since priced
-- -- -- required for vehicle days
-- -- -- no longer required, included priced_thru in greg.uc_base_vehicle_prices
-- -- drop table if exists greg.tmp_pricing_days cascade;
-- -- create table greg.tmp_pricing_days (
-- --   vehicle_inventory_item_id citext not null,
-- --   date_priced date not null,
-- --   invoice numeric(12,2) not null,
-- --   best_price numeric(12,2) not null,
-- --   priced_thru date not null,
-- --   primary key (vehicle_inventory_item_id,date_priced));
-- -- create index on greg.tmp_pricing_days(best_price);
-- -- insert into greg.tmp_pricing_days 
-- -- select vehicle_inventory_item_id,date_priced,invoice,best_price,
-- --   coalesce(lead(date_priced) over (partition by vehicle_inventory_item_id order by date_priced) - 1, '12/31/9999') as priced_thru
-- -- from greg.uc_base_vehicle_prices;

-- vehicle days
-- 8/18 includes pricing, status
-- drop table if exists vehicle_days;
-- create temp table vehicle_days as
-- select aa.sale_date, a.vehicle_inventory_item_id, aa.stock_number, a.the_date, 
--   coalesce(b.date_priced, '12/31/9999') as date_priced, coalesce(b.invoice, -1)::integer as invoice, 
--   coalesce(b.best_price, -1)::integer as best_price, 
--   case when date_priced is not null then a.the_date - b.date_priced else 0 end as days_since_priced, 
--   coalesce(c.price_band, 'Not Priced'),
--   (a.the_date - aa.date_acquired) + 1 as days_owned,
--   d.status, 
--   coalesce(d.days_avail, 0) as days_avail
-- from greg.uc_base_vehicle_days a
-- inner join greg.uc_base_vehicles aa on a.vehicle_inventory_item_id = aa.vehicle_inventory_item_id
-- left join greg.uc_base_vehicle_prices b on a.vehicle_inventory_item_id = b.vehicle_inventory_item_id
--     and a.the_date between b.date_priced and b.priced_thru
-- left join greg.uc_price_bands c on b.best_price between c.price_from and c.price_thru 
-- left join greg.uc_base_vehicle_statuses d on a.vehicle_inventory_item_id = d.vehicle_inventory_item_id
--   and a.the_date = d.the_date;


-- drop table if exists greg.uc_recon_by_day cascade;
-- create table greg.uc_recon_by_day (
--   stock_number citext not null,
--   the_date date not null,
--   recon_sales numeric(12,2) not null,
--   recon_cogs numeric(12,2) not null,
--   recon_gross numeric(12,2) not null,
--   primary key (stock_number,the_date));
-- insert into greg.uc_recon_by_day  
-- select stock_number, the_date, 
--   sum(case when account_type = 'sale' then -1 * amount else 0 end) as recon_sales,
--   sum(case when account_type = 'cogs' then amount else 0 end) as recon_cogs,
--   sum(-1 * amount) as recon_gross
-- from greg.uc_recon_details a
-- group by stock_number, the_date;




-- vehicle days
-- 8/18 includes pricing, status
-- 8/19 add recon
-- -- 3/7/19 removed days_avail
-- drop table if exists vehicle_days;
-- create temp table vehicle_days as
-- select aa.sale_date, aa.stock_number, a.vehicle_inventory_item_id, a.the_date, 
--   coalesce(b.date_priced, '12/31/9999') as date_priced, coalesce(b.invoice, -1)::integer as invoice, 
--   coalesce(b.best_price, -1)::integer as best_price, 
--   case when date_priced is not null then a.the_date - b.date_priced else 0 end as days_since_priced, 
--   coalesce(c.price_band, 'Not Priced') as price_band,
--   (a.the_date - aa.date_acquired) + 1 as days_owned,
--   d.status, 
-- --   coalesce(d.days_avail, 0) as days_avail,
--   e.recon_sales, e.recon_cogs, e.recon_gross,
--   e.post_sale_recon_sales, e.post_sale_recon_cogs, e.post_sale_recon_gross
-- from greg.uc_base_vehicle_days a
-- inner join greg.uc_base_vehicles aa on a.vehicle_inventory_item_id = aa.vehicle_inventory_item_id
--   and aa.date_acquired > '12/31/2012'
-- left join greg.uc_base_vehicle_prices b on a.vehicle_inventory_item_id = b.vehicle_inventory_item_id
--     and a.the_date between b.date_priced and b.priced_thru
-- left join greg.uc_price_bands c on b.best_price between c.price_from and c.price_thru 
-- left join greg.uc_base_vehicle_statuses d on a.vehicle_inventory_item_id = d.vehicle_inventory_item_id
--   and a.the_date = d.the_date
-- left join greg.uc_recon_by_day e on aa.stock_number = e.stock_number
--   and a.the_date = e.the_date; 
-- create unique index on vehicle_days(stock_number,the_date);
-- 
-- 
-- select stock_number, the_date, date_priced, invoice, best_price, days_since_priced, 
--   price_band, days_owned, status, -- days_avail,
--   coalesce(recon_sales,0) as recon_sales, 
--   coalesce(recon_cogs,0) as recon_cogs, 
--   coalesce(recon_gross,0) as recon_gross,
--   sum(coalesce(1 * recon_sales, 0)) over (partition by stock_number order by the_date) as recon_sales_running,
--   sum(coalesce(1 * recon_cogs, 0)) over (partition by stock_number order by the_date) as recon_cogs_running,
--   sum(coalesce(1 * recon_gross, 0)) over (partition by stock_number order by the_date) as recon_gross_running,
--   coalesce(post_sale_recon_sales,0) as post_sale_recon_sales, 
--   coalesce(post_sale_recon_cogs,0) as post_sale_recon_cogs, 
--   coalesce(post_sale_recon_gross,0) as post_sale_recon_gross
-- -- select *
-- from vehicle_days
-- where stock_number = '25751A'
-- order by stock_number, the_date
-- limit 2000


-- 8/24 need to add inventory_by_day (cost) just like recon

drop table if exists vehicle_days;
create temp table vehicle_days as
select aa.sale_date, aa.stock_number, a.vehicle_inventory_item_id, a.the_date, 
  coalesce(b.date_priced, '12/31/9999') as date_priced, coalesce(b.invoice, -1)::integer as invoice, 
  coalesce(b.best_price, -1)::integer as best_price, 
  case when date_priced is not null then a.the_date - b.date_priced else 0 end as days_since_priced, 
  coalesce(c.price_band, 'Not Priced') as price_band,
  (a.the_date - aa.date_acquired) + 1 as days_owned,
  d.status, 
  d.days_in_status, 
  e.recon_sales, e.recon_cogs, e.recon_gross,
  e.post_sale_recon_sales, e.post_sale_recon_cogs, e.post_sale_recon_gross,
  f.inventory_cost, f.post_sale_cost
from greg.uc_base_vehicle_days a
inner join greg.uc_base_vehicles aa on a.vehicle_inventory_item_id = aa.vehicle_inventory_item_id
  and aa.date_acquired > '12/31/2012'
left join greg.uc_base_vehicle_prices b on a.vehicle_inventory_item_id = b.vehicle_inventory_item_id
    and a.the_date between b.date_priced and b.priced_thru
left join greg.uc_price_bands c on b.best_price between c.price_from and c.price_thru 
left join greg.uc_base_vehicle_statuses d on a.vehicle_inventory_item_id = d.vehicle_inventory_item_id
  and a.the_date = d.the_date
left join greg.uc_recon_by_day e on aa.stock_number = e.stock_number
  and a.the_date = e.the_date
left join (
  select a.stock_number,
    case 
      when a.the_date < b.date_acquired then b.date_acquired
      when a.the_date <= b.sale_date then a.the_date
      else b.sale_date
    end as the_date,
    sum(case when a.the_date  <= b.sale_date then amount else 0 end) as inventory_cost,
    sum(case when  a.the_date  > b.sale_date then amount else 0 end) as post_sale_cost
  from greg.uc_cost_details a
  left join greg.uc_base_vehicles b on a.stock_number = b.stock_number
  group by a.stock_number, 
    case 
      when a.the_date < b.date_acquired then b.date_acquired
      when a.the_date <= b.sale_date then a.the_date
      else b.sale_date
    end) f on aa.stock_number = f.stock_number
        and a.the_Date = f.the_date;
       
create unique index on vehicle_days(stock_number,the_date);


select a.stock_number, the_date, date_priced, invoice, best_price, days_since_priced, 
  price_band, days_owned, status, -- days_avail,
  coalesce(recon_sales,0) as recon_sales, 
  coalesce(recon_cogs,0) as recon_cogs, 
  coalesce(recon_gross,0) as recon_gross,
  sum(coalesce(1 * recon_sales, 0)) over (partition by a.stock_number order by the_date) as recon_sales_running,
  sum(coalesce(1 * recon_cogs, 0)) over (partition by a.stock_number order by the_date) as recon_cogs_running,
  sum(coalesce(1 * recon_gross, 0)) over (partition by a.stock_number order by the_date) as recon_gross_running,
  coalesce(post_sale_recon_sales,0) as post_sale_recon_sales, 
  coalesce(post_sale_recon_cogs,0) as post_sale_recon_cogs, 
  coalesce(post_sale_recon_gross,0) as post_sale_recon_gross,
  coalesce(inventory_cost, 0) as cost,
  sum(coalesce(inventory_cost, 0)) over (partition by a.stock_number order by the_date) as inventory_cost_running,
  coalesce(post_sale_cost,0) as post_sale_cost
-- select *
from vehicle_days a
where exists (
  select 1
  from greg.uc_base_vehicles
  where stock_number = a.stock_number
    and date_acquired > '02/25/2019')
order by stock_number, the_date


select a.*
from vehicle_days a
inner join greg.uc_base_vehicles aa on a.stock_number = aa.stock_number
where aa.date_acquired > '12/31/2016'
order by a.stock_number, a.the_date
limit 4000


select *
from greg.uc_cost_details
where stock_number = '25751A'
order by stock_number, the_date

-- actually, very few with an internet_price, oh well, need to get v-auto prices
select a.inpmast_stock_number, a.inpmast_vin, a.year, a.make, a.model, 
  a.model_code, chrome_style_id, a.body_style as body_trim, a.color, 
  a.list_price, a.inpmast_vehicle_cost, a.work_in_process,
  b.holdback, b.flooring, b.internet_price, b.rebate
from arkona.xfm_inpmast a
left join (
  select company_number, vin_number, 
    sum(case when seq_number = 4 then num_field_value else 0 end) as holdback,
    sum(case when seq_number = 5 then num_field_value else 0 end) as flooring,
    sum(case when seq_number = 9 then num_field_value else 0 end) as internet_price,
    sum(case when seq_number = 12 then num_field_value else 0 end) as rebate
  from arkona.ext_inpoptf
  where seq_number in (4,5,9,12)
  group by company_number, vin_number) b on a.inpmast_company_number = b.company_number
  and a.inpmast_vin = b.vin_number
-- where a.inpmast_vin = '1G1ZE5ST9HF211440'
where status = 'I'
  and type_n_u = 'U'
  and current_row = true
  and left(inpmast_stock_number, 1) <> 'H'
order by inpmast_Stock_number


drop table if exists running_days;
create temp table running_days as
select b.the_date, a.store_code, a.stock_number, a.vin, a.date_acquired, a.sale_date, a.make, 
  a.model, a.yearmodel as year, a.bodystyle,
  a.trim, a.exteriorcolor as color, a.cab, a.drive, a.shape, a.size, a.shape_size, a.miles,
  b.date_priced, b.best_price, b.days_since_priced, b.price_band, b.days_owned, b.status, 
  b.days_in_status,
  coalesce(recon_sales,0) as recon_sales, 
  coalesce(recon_cogs,0) as recon_cogs, 
  coalesce(recon_gross,0) as recon_gross,
  sum(coalesce(1 * recon_sales, 0)) over (partition by a.stock_number order by the_date) as recon_sales_running,
  sum(coalesce(1 * recon_cogs, 0)) over (partition by a.stock_number order by the_date) as recon_cogs_running,
  sum(coalesce(1 * recon_gross, 0)) over (partition by a.stock_number order by the_date) as recon_gross_running,
  coalesce(post_sale_recon_sales,0) as post_sale_recon_sales, 
  coalesce(post_sale_recon_cogs,0) as post_sale_recon_cogs, 
  coalesce(post_sale_recon_gross,0) as post_sale_recon_gross,
  coalesce(inventory_cost, 0) as inventory_cost,
  sum(coalesce(inventory_cost, 0)) over (partition by a.stock_number order by the_date) as inventory_cost_running,
  coalesce(post_sale_cost,0) as post_sale_cost
-- select *
from vehicles a
inner join vehicle_days b on a.stock_number = b.stock_number

select * from running_days where stock_number = '29720a'
select * from vehicle_days where stock_number = '29720a'

-- list of column names for temp table
SELECT string_agg(column_name, ',')
FROM information_schema.columns
-- WHERE table_schema = 'sls'
where table_name   = 'daily_alpha_1'
group by table_name  



-- what's missing
-- pot lot gross can do in query
-- store done
-- dup rows H10374G, 31389XX both in vehicles - fixed in vehicles
-- current inventory
select the_date,store_code,stock_number,vin,date_acquired,sale_date,make,model,
  year,bodystyle,trim,color,cab,drive,shape,size,shape_size,miles,date_priced,
  best_price,days_since_priced,price_band,days_owned,status,days_in_status,
  recon_sales_running,recon_gross_running,
  inventory_cost_running,
  case
    when best_price = -1 then best_price
    when inventory_cost_running = 0 then 0
    else best_price - inventory_cost_running 
  end as pot_lot_gross
-- select *  
from running_days
where sale_date > current_Date
  and the_date = current_date

add front and back sales/cogs


select *
from greg.uc_base_vehicle_accounting a
inner join greg.uc_base_vehicles b on a.control = b.stock_number
where b.sale_date = '07/15/2017'

create temp table daily_alpha_1 as
select a.*, 
  case
    when best_price = -1 then best_price
    when inventory_cost_running = 0 then 0
    else best_price - inventory_cost_running 
  end as pot_lot_gross,
  aa.front_gross_at_sale, aa.front_gross_post_sale, aa.back_gross_at_sale, aa.back_gross_post_sale
from running_days a
left join (
  select a.stock_number, a.sale_date,
    sum(case when b.department_code = 'uc' and b.the_date <= a.sale_date then -1 * amount else 0 end) as front_gross_at_sale,
    sum(case when b.department_code = 'uc' and b.the_date > a.sale_date then amount else 0 end) as front_gross_post_sale,
    sum(case when b.department_code = 'fi' and b.the_date <= a.sale_date then -1 * amount else 0 end) as back_gross_at_sale,
    sum(case when b.department_code = 'fi' and b.the_date > a.sale_date then amount else 0 end) as back_gross_post_sale
  from greg.uc_base_vehicles a
  left join greg.uc_base_vehicle_accounting b on a.stock_number = b.control
  -- where sale_date between '07/01/2017' and '07/31/2017'
  --   and b.store_code = 'ry1' 
  where sale_Date <= current_date
    and left(stock_number, 1) <> 'C'
  group by a.stock_number, a.sale_date) aa on a.stock_number = aa.stock_number and a.the_date = aa.sale_date


select *
from daily_alpha_1
where the_date = current_date - 730
  and sale_date > current_date -730

drop table if exists greg.uc_daily_snapshot_beta_1 cascade;
create table greg.uc_daily_snapshot_beta_1 (
  the_date date,
  store_code citext,
  stock_number citext,
  vin citext,
  date_acquired date,
  sale_date date,
  make citext,
  model citext,
  year citext,
  bodystyle citext,
  trim citext,
  color citext,
  cab citext,
  drive citext,
  shape citext,
  size citext,
  shape_size citext,
  miles integer,
  date_priced date,
  best_price integer,
  days_since_priced integer,
  price_band citext,
  days_owned integer,
  status citext,
  days_in_status integer,
  recon_sales integer,
  recon_cogs integer,
  recon_gross integer,
  recon_sales_running integer,
  recon_cogs_running integer,
  recon_gross_running integer,
  post_sale_recon_sales integer,
  post_sale_recon_cogs integer,
  post_sale_recon_gross integer,
  inventory_cost integer,
  inventory_cost_running integer,
  post_sale_cost integer,
  sale_type citext,
  pot_lot_gross integer,
  front_gross_at_sale integer,
  front_gross_post_sale integer,
  back_gross_at_sale integer,
  back_gross_post_sale integer,
  primary key(the_date,stock_number));

create index on greg.uc_daily_snapshot_beta_1(vin);
create index on greg.uc_daily_snapshot_beta_1(status);
create index on greg.uc_daily_snapshot_beta_1(shape);
create index on greg.uc_daily_snapshot_beta_1(size);
create index on greg.uc_daily_snapshot_beta_1(make);
create index on greg.uc_daily_snapshot_beta_1(model);
create index on greg.uc_daily_snapshot_beta_1(sale_type);

delete from greg.uc_daily_snapshot_beta_1;
insert into greg.uc_daily_snapshot_beta_1
select a.*, c.sale_type,
  case
    when best_price = -1 then best_price
    when inventory_cost_running = 0 then 0
    else best_price - inventory_cost_running 
  end as pot_lot_gross,
  aa.front_gross_at_sale, aa.front_gross_post_sale, aa.back_gross_at_sale, aa.back_gross_post_sale
from running_days a
left join ( -- aa
  select a.stock_number, a.sale_date,
    sum(case when b.department_code = 'uc' and b.the_date <= a.sale_date then -1 * amount else 0 end) as front_gross_at_sale,
    sum(case when b.department_code = 'uc' and b.the_date > a.sale_date then amount else 0 end) as front_gross_post_sale,
    sum(case when b.department_code = 'fi' and b.the_date <= a.sale_date then -1 * amount else 0 end) as back_gross_at_sale,
    sum(case when b.department_code = 'fi' and b.the_date > a.sale_date then amount else 0 end) as back_gross_post_sale
  from greg.uc_base_vehicles a
  left join greg.uc_base_vehicle_accounting b on a.stock_number = b.control
  where sale_Date <= current_date
    and left(stock_number, 1) <> 'C'
  group by a.stock_number, a.sale_date) aa on a.stock_number = aa.stock_number and a.the_date = aa.sale_date
left join (
select a.stock_number, substring(b.typ, position('_' in typ) + 1, length(typ) - position('_' in typ)) as sale_type
from greg.uc_base_Vehicles a
left join ads.ext_vehicle_sales b on a.vehicle_inventory_item_id = b.vehicleinventoryitemid
  AND b.status = 'VehicleSale_Sold') c on a.stock_number = c.stock_number;






select a.*, c.sale_type,
  case
    when best_price = -1 then best_price
    when inventory_cost_running = 0 then 0
    else best_price - inventory_cost_running 
  end as pot_lot_gross,
  aa.front_gross_at_sale, aa.front_gross_post_sale, aa.back_gross_at_sale, aa.back_gross_post_sale
from running_days a
left join ( -- aa
  select a.stock_number, a.sale_date,
    sum(case when b.department_code = 'uc' and b.the_date <= a.sale_date then -1 * amount else 0 end) as front_gross_at_sale,
    sum(case when b.department_code = 'uc' and b.the_date > a.sale_date then amount else 0 end) as front_gross_post_sale,
    sum(case when b.department_code = 'fi' and b.the_date <= a.sale_date then -1 * amount else 0 end) as back_gross_at_sale,
    sum(case when b.department_code = 'fi' and b.the_date > a.sale_date then amount else 0 end) as back_gross_post_sale
  from greg.uc_base_vehicles a
  left join greg.uc_base_vehicle_accounting b on a.stock_number = b.control
  where sale_Date <= current_date
    and a.stock_number = '29720A'
  group by a.stock_number, a.sale_date) aa on a.stock_number = aa.stock_number and a.the_date = aa.sale_date
  left join (
    select a.stock_number, substring(b.typ, position('_' in typ) + 1, length(typ) - position('_' in typ)) as sale_type
    from greg.uc_base_Vehicles a
    left join ads.ext_vehicle_sales b on a.vehicle_inventory_item_id = b.vehicleinventoryitemid
      AND b.status = 'VehicleSale_Sold'
    where a.stock_number = '29720A') c on a.stock_number = c.stock_number
where a.stock_number = '29720a'     

select * from running_Days where stock_number = '29720a'
select * from vehicle_days where stock_number = '29720a' -- has inventory_cost and post_sale_cost
gross is figured in the greg.uc_daily_snapshot_beta_1 query based on department code and sale date

where does post_sale_cost come from
pot_lot_gross
front_Gross_at_sale
Front_gross_post_sale











  
select * from greg.uc_base_vehicle_accounting limit 100

select store_code, control, page, line, sum(amount) from greg.uc_base_Vehicle_accounting where year_month = 201711 group by store_code,control,page,line

select * from greg.uc_base_vehicle_accounting where store_code = 'ry1' and year_month = 201711 and page = 16 and line = 5 order by control

select page, line, sum(amount) from greg.uc_base_vehicle_accounting where control = '29720a' group by page, line

select min(the_date), max(the_date) from running_days limit 10
  
select *
from greg.uc_daily_snapshot_beta_1
where stock_number = '29720a'

select a.stock_number, substring(b.typ, position('_' in typ) + 1, length(typ) - position('_' in typ)) as sale_type
from greg.uc_base_Vehicles a
left join ads.ext_vehicle_sales b on a.vehicle_inventory_item_id = b.vehicleinventoryitemid
  AND b.status = 'VehicleSale_Sold'

-- current inventory
select the_date,store_code,stock_number,vin,date_acquired,sale_date,make,model,
  year,bodystyle,trim,color,cab,drive,shape,size,shape_size,miles,date_priced,
  best_price,days_since_priced,price_band,days_owned,status,days_in_status,
  recon_sales_running,recon_gross_running,
  inventory_cost_running,pot_lot_gross
from greg.uc_daily_snapshot_beta_1
where the_date = current_date
  and sale_date > current_date

-- inventory 7 days ago
select the_date,store_code,stock_number,vin,date_acquired,sale_date,make,model,
  year,bodystyle,trim,color,cab,drive,shape,size,shape_size,miles,date_priced,
  best_price,days_since_priced,price_band,days_owned,status,days_in_status,
  recon_sales_running,recon_gross_running,
  inventory_cost_running,pot_lot_gross
from greg.uc_daily_snapshot_beta_1
where the_date = current_date -7
  and sale_date > current_date -7
  


select the_date,store_code,stock_number,vin,date_acquired,sale_date,make,model,
  year,bodystyle,trim,color,cab,drive,shape,size,shape_size,miles,date_priced,
  best_price,days_since_priced,price_band,days_owned,status,days_in_status,
  recon_sales_running,recon_gross_running,
  inventory_cost_running,front_gross_at_sale, front_gross_post_sale,
  back_gross_at_sale, back_gross_post_sale
from greg.uc_daily_snapshot_beta_1
where the_date = sale_date
  and sale_date between '02/01/2017' and '02/28/2017'
  and sale_type = 'wholesale'
  and store_code = 'ry1'

select  *
from greg.uc_base_vehicles
limit 100



select status, count(*)
from greg.uc_daily_snapshot_beta_1
group by status



    
days_avail needs to be days_in_status  - done

figure out post sale cost vs post sale recon

post_sale_cost from inventory vs cogs

front gross fi gross
