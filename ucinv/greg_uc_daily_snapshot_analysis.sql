﻿
-- 3/7/19 201902 ATWR uc sales, 245 (per FS), greg shows 235
drop table if exists test_1;
create temp table test_1 as
select *
from (
  select stock_number as greg_stock_number
  from greg.uc_daily_snapshot_beta_1
  where store_code = 'ry1'
    and sale_date between '02/01/2019' and '02/28/2019'
    and the_date = sale_date
    and sale_type = 'retail') a
full outer join(
  select stock_number as deals_stock_number,
    (select distinct sale_date from greg.uc_daily_snapshot_beta_1 where stock_number = a.stock_number and sale_date = the_date) as greg_sale_date
  from sls.deals_by_month a
  where year_month = 201902
    and vehicle_type = 'used'
    and store_code = 'ry1'
    and sale_type = 'retail') b on a.greg_stock_number = b.deals_stock_number
where a.greg_stock_number is null  or b.deals_stock_number is null;


select *
from test_1

drop table if exists greg_feb_sales;
create temp table greg_feb_sales as
  select *
  from greg.uc_daily_snapshot_beta_1
  where store_code = 'ry1'
    and sale_date between '02/01/2019' and '02/28/2019'
    and the_date = sale_date
    and sale_type = 'retail' 
--------------------------------------------------------------------
--/> 3/7/19 
--------------------------------------------------------------------
change G36006YC to G36006C in ads, see if it persists
select *
from greg.uc_Base_vehicles
where vin = '1G1ZK57B39F127026'

select *
from sls.deals
where stock_number = 'g34617aa'

change some sale dates
select * from greg.uc_daily_snapshot_beta_1
where stock_number = 'g34801ra'

delete from greg.uc_daily_snapshot_beta_1
where stock_number = 'g34801ra'
and the_date between '02/28/2019' and '03/03/2019';
update greg.uc_daily_snapshot_beta_1
set the_date = '02/28/2019'
where stock_number = 'g34801ra'
  and the_Date = '03/04/2019';
update greg.uc_daily_snapshot_beta_1
set sale_date = '02/28/2019'
where stock_number = 'g34801ra';

select * from greg.uc_daily_snapshot_beta_1
where stock_number = 'g35826a'

delete from greg.uc_daily_snapshot_beta_1
where stock_number = 'g35826a'
and the_date between '02/27/2019' and '03/03/2019';

update greg.uc_daily_snapshot_beta_1
set the_date = '02/28/2019'
where stock_number = 'g35826a'
  and the_Date = '03/04/2019';

update greg.uc_daily_snapshot_beta_1
set sale_date = '02/28/2019'
where stock_number = 'g35826a';  

select * from greg.uc_daily_snapshot_beta_1
where stock_number = 'g36300x'

delete from greg.uc_daily_snapshot_beta_1
where stock_number = 'g36300x'
and the_date between '02/27/2019' and '03/01/2019';

update greg.uc_daily_snapshot_beta_1
set the_date = '02/28/2019'
where stock_number = 'g36300x'
  and the_Date = '03/02/2019';

update greg.uc_daily_snapshot_beta_1
set sale_date = '02/28/2019'
where stock_number = 'g36300x';  

select * from greg.uc_daily_snapshot_beta_1
where stock_number = 'g33260ra'

delete from greg.uc_daily_snapshot_beta_1
where stock_number = 'g33260ra'
and the_date between '02/11/2019' and '02/28/2019';

update greg.uc_daily_snapshot_beta_1
set the_date = '02/11/2019'
where stock_number = 'g33260ra'
  and the_Date = '03/01/2019';

update greg.uc_daily_snapshot_beta_1
set sale_date = '02/11/2019'
where stock_number = 'g33260ra';    

select * from greg.uc_daily_snapshot_beta_1
where stock_number = 'g35160c'

delete from greg.uc_daily_snapshot_beta_1
where stock_number = 'g35160c'
and the_date between '02/09/2019' and '02/28/2019';

update greg.uc_daily_snapshot_beta_1
set the_date = '02/09/2019'
where stock_number = 'g35160c'
  and the_Date = '03/01/2019';

update greg.uc_daily_snapshot_beta_1
set sale_date = '02/09/2019'
where stock_number = 'g35160c';   