﻿select min(the_date), max(the_date) -- 1/1/14 -> 1/22/17
from greg.used_Vehicle_daily_Snapshot

select count(*) -- 578,546
from greg.used_Vehicle_daily_Snapshot

select *
from greg.used_Vehicle_daily_Snapshot
where extract(year from the_date) = 2017

select *
from greg.used_Vehicle_daily_Snapshot
where sale_date = '01/07/2017'
  and sale_date = the_date
order by stocknumber  

select *
from greg.ucinv_tmp_avail_6
where thedate = '01/07/2017'

select *
from greg.ucinv_tmp_avail_4
where sale_date = '01/07/2017'
  and sale_date = thedate
order by stocknumber  


select *
from greg.ucinv_tmp_avail_disposition
where thedate = '01/07/2015'


select *
from greg.ucinv_tmp_sales_activity
where sale_date = '01/07/2017'


select *
from greg.ucinv_tmp_sales_by_status
limit 100





select x.*,
  sum(the_count) over () as total, 
  round((100*the_count)/sum(the_count) over (), 3) as perc_of_total
from (
  select shape_and_size, price_band, count(*) as the_count
  from greg.used_Vehicle_daily_Snapshot
  where sale_date = the_date
    and sale_type = 'retail'
  group by shape_and_size, price_band) x
order by the_count desc



select xx.*, perc_of_total - lag(perc_of_total, 1) over (order by the_count)
from (
  select x.*,
    sum(the_count) over () as total, 
    round((100*the_count)/sum(the_count) over (), 3) as perc_of_total
  from (
    select shape_and_size, price_band, count(*) as the_count
    from greg.used_Vehicle_daily_Snapshot
    where sale_date = the_date
      and sale_type = 'retail'
    group by shape_and_size, price_band) x) xx
order by the_count desc



select x.*,
  sum(the_count) over () as total, 
  round((100*the_count)/sum(the_count) over (), 3) as perc_of_total
from (
  select shape_and_size, price_band, count(*) as the_count
  from greg.used_Vehicle_daily_Snapshot
  where sale_date = the_date
    and sale_type = 'retail'
  group by shape_and_size, price_band) x
where price_band = '00-6k' or shape_and_size = 'Pickup - Large'
order by the_count desc
