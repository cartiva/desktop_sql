﻿
-- need recon gross (thru-put)
-- mimicing intranet sql scripts/crayon report/fixed_gross.sql

select *
from greg.uc_base_vehicles a
inner join fin.fact_gl b on a.stock_number = b.control
inner join fin.dim_account c on b.account_key = c.account_key
inner join fin.dim_fs_account d on c.account = d.gl_account
  and d.gm_account in ('240','241') -- inventory accounts
inner join fin.dim_journal e on b.journal_key = e.journal_key
  and e.journal_code in ('svi','pot')
limit 500


select * from fin.dim_Account

-- fs : accounts for fs page/line
select *
from arkona.ext_eisglobal_sypffxmst a
-- -- *a*
left join (
  select factory_financial_year, 
  case
    when coalesce(consolidation_grp, '1')  = '1' then 'RY1'::citext
    when coalesce(consolidation_grp, '1')  = '2' then 'RY2'::citext
    else 'XXX'::citext
  end as store_code, g_l_acct_number, factory_account, fact_account_
  from arkona.ext_ffpxrefdta) b on a.fxmcyy = b.factory_financial_year and a.fxmact = b.factory_account
where a.fxmcyy = 2017
  and fxmpge = 1
  and fxmlne between 25 and 26
order by fxmpge, fxmlne, fxmcol


-- inventory accounts
select *
from fin.dim_fs_account
where gm_account in ('240','241')

-- from Desktop/ucinv_recon_2.sql
-- this gives me ros
drop table if exists tmp_recon_ros;
create temp table tmp_recon_ros as
select control as stock_number, doc as ro  
from fin.fact_gl a
inner join fin.dim_account c on a.account_key = c.account_key
inner join fin.dim_journal d on a.journal_key = d.journal_key
inner join greg.uc_base_vehicles e on a.control = e.stock_number
where a.post_status = 'Y'
  and
    case
      when c.account in (select gl_account from fin.dim_fs_Account where gm_account in ('240','241')) then d.journal_code in ('SVI','POT')
      when c.account in ( -- i don't get this part, why just cogs accounts?
        select gl_account
        from fin.fact_fs a
        inner join fin.dim_fs_account b on a.fs_account_key = b.fs_account_key
        inner join fin.dim_fs c on a.fs_key = c.fs_key
        inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
        inner join fin.dim_account e on b.gl_account = e.account
        where c.page = 16
          and e.department_code = 'uc'
          and e.account_type_code = '5'
--           and d.store = 'ry1'
        group by gl_account) then d.journal_code in ('SVI','SCA','POT')
    end
group by control, doc
having sum(amount) <>  0;   



drop table if exists greg.uc_recon_ros;
create table greg.uc_recon_ros (
  stock_number citext not null,
  ro citext not null,
  primary key(stock_number,ro));
insert into greg.uc_recon_ros  
select a.control as stock_number, a.doc as ro  
from fin.fact_gl a
inner join fin.dim_account c on a.account_key = c.account_key
inner join fin.dim_journal d on a.journal_key = d.journal_key
inner join greg.uc_base_vehicles e on a.control = e.stock_number
where a.post_status = 'Y'
  and
    case
      when c.account in (select gl_account from fin.dim_fs_Account where gm_account in ('240','241')) then d.journal_code in ('SVI','POT')
      when c.account in ( -- i don't get this part, why just cogs accounts?, because dept uc gets billed not credited?
        select gl_account
        from fin.fact_fs a
        inner join fin.dim_fs_account b on a.fs_account_key = b.fs_account_key
        inner join fin.dim_fs c on a.fs_key = c.fs_key
        inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
        inner join fin.dim_account e on b.gl_account = e.account
        where c.page = 16
          and e.department_code = 'uc'
          and e.account_type_code = '5'
--           and d.store = 'ry1'
        group by gl_account) then d.journal_code in ('SVI','SCA','POT')
    end
  and exists (
    select 1
    from ads.ext_fact_repair_order
    where ro = a.doc)   
group by control, doc
having sum(amount) <>  0; 

select count(*) from greg.uc_Recon_ros  --83199

CREATE TABLE greg.uc_recon (
  the_date date NOT NULL,
  stock_number citext NOT NULL,
  sales numeric(12,2) NOT NULL,
  cogs numeric(12,2) NOT NULL,
  gross numeric(12,2) NOT NULL,
  hash citext NOT NULL,
  CONSTRAINT uc_recon_pkey PRIMARY KEY (the_date, stock_number));

insert into greg.uc_recon
select d.the_date, a.stock_number, 
  sum(case when c.account_type in ('sale','expense') then -b.amount else 0 end) as sales,
  sum(case when c.account_type = 'cogs' then b.amount else 0 end) as cogs,
  sum(-b.amount) as gross,
  (
    select md5(z::text) as hash
    from (
      select dd.the_date, aa.stock_number, 
        sum(case when cc.account_type in ('sale','expense') then -b.amount else 0 end) as sales,
        sum(case when cc.account_type = 'cogs' then b.amount else 0 end) as cogs,
        sum(-b.amount) as gross
      from greg.uc_recon_ros aa
      inner join fin.fact_gl b on aa.ro = b.control
      inner join fin.dim_account cc on b.account_key = cc.account_key
      inner join dds.dim_date dd on b.date_key = dd.date_key
      where b.post_status = 'Y'
        and cc.account_type in ('sale','cogs','expense')
        and dd.the_date = d.the_date
        and aa.stock_number = a.stock_number
      group by aa.stock_number, dd.the_date) z)    
from greg.uc_recon_ros a
inner join fin.fact_gl b on a.ro = b.control
inner join fin.dim_account c on b.account_key = c.account_key
inner join dds.dim_date d on b.date_key = d.date_key
where b.post_status = 'Y'
  and account_type in ('sale','cogs','expense')
group by a.stock_number, d.the_date;


select *
from fin.fact_gl a
inner join greg.uc_recon_ros b on a.doc = b.ro and a.doc <> a.control
where a.post_status <> 'Y'


select *
from greg.uc_recon_ros
where stock_number = '28958b'

drop table if exists test;
create temp table test as
select d.the_date, a.stock_number, c.account_type, b.amount, b.doc, b.trans, b.seq, b.post_status
from greg.uc_recon_ros a
inner join fin.fact_gl b on a.ro = b.control
inner join fin.dim_account c on b.account_key = c.account_key
inner join dds.dim_date d on b.date_key = d.date_key
where b.post_status = 'Y'
  and account_type in ('sale','cogs','expense')

select *
from test
where doc = '16252534'
order by post_status


select d.the_date, a.post_status, a.trans,seq, a.control, a.doc, a.amount, b.journal_code, c.account, c.account_type, c.description
from fin.fact_gl a
inner join fin.dim_journal b on a.journal_key = b.journal_key
inner join fin.dim_account c on a.account_key = c.account_key
inner join dds.dim_Date d on a.date_key = d.date_key
-- where control = '28958B' order by journal_code
where doc = '16252534'

select * 
from greg.uc_recon
where stock_number = '28958b'

-- recon_ros where trans is now void
select d.the_date, a.post_status, a.trans, a.seq, a.control, a.doc, a.amount, b.journal_code, c.account, c.account_type, c.description
from fin.fact_gl a
inner join fin.dim_journal b on a.journal_key = b.journal_key
inner join fin.dim_account c on a.account_key = c.account_key
inner join dds.dim_Date d on a.date_key = d.date_key
-- where control = '28958B' order by journal_code
inner join test e on a.control = e.stock_number
  and a.doc = e.doc
where a.post_status <> 'Y'

do the uc_recon query without the grouping keeping the ro
ok, that is the temp table test generated above

select * 
from test
where stock_number = '28958b'


select *
from fin.fact_gl
where post_status <> 'Y'
  and amount <> 0

select * 
from fin.fact_gl
where control = 'h3362'

-- starting over recon_ros -> recon_details -> some aggregate
-- making FK update on cascade: if a transaction gets changed to void in fact_gl, it will alsl
-- be changed to void in uc_recon_detail
CREATE TABLE greg.uc_recon_detail (
  the_date date not null,
  stock_number citext not null,
  account_type citext not null,
  amount numeric(12,2) NOT NULL,
  trans integer NOT NULL,
  seq integer NOT NULL,
  post_status citext NOT NULL,  
  hash citext NOT NULL,
  foreign key (trans,seq,post_status) references fin.fact_gl(trans,seq,post_status) on update cascade,
  primary key (trans,seq,post_status));
create index on greg.uc_recon_detail(hash);  
create index on greg.uc_recon_detail(stock_number);
create index on greg.uc_recon_detail(account_type);

insert into greg.uc_recon_detail
select d.the_date, a.stock_number, c.account_type, b.amount, 
  b.trans, b.seq, b.post_status, 
  (
    select md5(z::text) as hash
    from (
      select d.the_date, a.stock_number, c.account_type, bb.amount, 
        bb.trans, bb.seq, bb.post_status
      from greg.uc_recon_ros a
      inner join fin.fact_gl bb on a.ro = bb.control
      inner join fin.dim_account c on bb.account_key = c.account_key
      inner join dds.dim_date d on bb.date_key = d.date_key
      where b.post_status = 'Y'
        and account_type in ('sale','cogs','expense')
        and bb.trans = b.trans
        and bb.seq = b.seq
        and bb.post_status = b.post_status)z )    
from greg.uc_recon_ros a
inner join fin.fact_gl b on a.ro = b.control
inner join fin.dim_account c on b.account_key = c.account_key
inner join dds.dim_date d on b.date_key = d.date_key
where b.post_status = 'Y'
  and account_type in ('sale','cogs','expense')
  and a.ro not in ( -- ros (9) for mult stock numbers
    select ro
    from (
    select ro, stock_number
    from greg.uc_recon_ros
    group by ro, stock_number) x 
    group by ro having count(*) > 1)  

select trans,seq,post_status
from fin.fact_gl
group by trans,seq,post_status
having count(*) > 1


      select trans,seq,post_status
select *       
      from greg.uc_recon_ros a
      inner join fin.fact_gl bb on a.ro = bb.control
      inner join fin.dim_account c on bb.account_key = c.account_key
      inner join dds.dim_date d on bb.date_key = d.date_key
where trans = 1996378 and seq = 1
      
      group by trans,seq,post_status
      having count(*) > 1

select ro
from (
select ro, stock_number
from greg.uc_recon_ros
group by ro, stock_number) x group by ro having count(*) > 1

select * from greg.uc_recon_ros where ro = '16194102'

select min(the_date)
from ads.ext_Fact_repair_order a
inner join dds.dim_date b on a.opendatekey = b.date_key

--     select md5(z::text) as hash
--     from (
--       select dd.the_date, aa.stock_number, 
--         sum(case when cc.account_type in ('sale','expense') then -b.amount else 0 end) as sales,
--         sum(case when cc.account_type = 'cogs' then b.amount else 0 end) as cogs,
--         sum(-b.amount) as gross
--       from greg.uc_recon_ros aa
--       inner join fin.fact_gl b on aa.ro = b.control
--       inner join fin.dim_account cc on b.account_key = cc.account_key
--       inner join dds.dim_date dd on b.date_key = dd.date_key
--       where b.post_status = 'Y'
--         and cc.account_type in ('sale','cogs','expense')
--         and dd.the_date = d.the_date
--         and aa.stock_number = a.stock_number
--       group by aa.stock_number, dd.the_date) z)  