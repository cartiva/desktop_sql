﻿-- Function: board.json_is_boarded(citext, citext)

-- DROP FUNCTION board.json_is_boarded(citext, citext);

CREATE OR REPLACE FUNCTION board.json_is_boarded(
    _stock_number citext,
    _action citext)
  RETURNS SETOF json AS
$BODY$

/*
select board.json_is_boarded('afton', 'Addition')
*/

begin 
if exists (
select 1
from board.sales_board
where stock_number = _stock_number
and is_deleted = false
)
then if exists (
select 1
where _action = 'Back-on'::citext)
then 
return query 
select row_to_json(A)
from (
  select case when count(*) = 0 then false else true end as does_exist, max(boarded_ts) as boarded_ts
  from board.sales_board
  where stock_number = _stock_number
  and board_type_key in (11)
  and is_deleted = false
) A;
else if exists (
select 1
where _action = 'Addition'::citext)
then 
return query 
select row_to_json(A)
from (
  select case when count(*) = 0 then false else true end as does_exist, max(boarded_ts) as boarded_ts
  from board.sales_board
  where stock_number <> _stock_number
  --and board_type_key not in (11)
  and is_deleted = false
) A;

else if exists (
select 1
where _action = 'Vehicle'::citext
)
then
return query 
select row_to_json(A)
from (
  select case when count(*) = 0 then false else true end as does_exist, max(boarded_ts) as boarded_ts
  from board.sales_board a
  inner join (
  select max(boarded_ts) as max_boarded_date
  from board.sales_board 
  where stock_number = _stock_number
  and is_deleted = false
  ) b on a.boarded_ts = b.max_boarded_date
  inner join board.board_types c on a.board_type_key = c.board_type_key
  where stock_number = _stock_number
  --and board_type_key not in (11)
  and c.board_type not in('Addition', 'Back-on')
  and is_deleted = false
) A;

else if exists (
select 1
where _action = 'Deal'::citext
)
then
return query 
select row_to_json(A)
from (
  select case when count(*) = 0 then false else true end as does_exist, max(boarded_ts) as boarded_ts
  from board.sales_board a
  inner join (
  select max(boarded_ts) as max_boarded_date
  from board.sales_board 
  where stock_number = _stock_number
  and is_deleted = false
  ) b on a.boarded_ts = b.max_boarded_date
  inner join board.board_types c on a.board_type_key = c.board_type_key
  where stock_number = _stock_number
  --and board_type_key not in (11)
  and c.board_type not in('Addition', 'Back-on')
  and is_deleted = false
) A;

-- select row_to_json(A)
-- from (
--   select case when count(*) = 0 then false else true end as does_exist, max(boarded_ts) as boarded_ts
--   from board.sales_board
--   where stock_number = _stock_number
--   and board_type_key not in (4,11)
--   and is_deleted = false
--   and stock_number not in (
--     select stock_number
--     from board.sales_board
--     where board_type_key  in (4,11)
--     and is_deleted = false) 
-- ) A;

else 
return query 
select row_to_json(A)
from (
  select false as does_exist, null as boarded_ts
) A;

end if;

end if;
end if; 
end if;
else 
return query 
select row_to_json(A)
from (
  select false as does_exist, null as boarded_ts
) A;

end if;

end


  $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION board.json_is_boarded(citext, citext)
  OWNER TO rydell;
