﻿drop table if exists unit_count;
create temp table unit_count as
select year_month, store, page, line, line_label, control, sum(unit_count) as unit_count
from ( -- h
  select d.year_month, d.store, d.page, d.line, d.line_label, d.description, d.gl_account, 
    a.control, a.amount,
    case when a.amount < 0 then 1 else -1 end as unit_count
  from fin.fact_gl a   
  inner join dds.dim_date b on a.date_key = b.date_key
    and b.year_month > 201700
  inner join fin.dim_account c on a.account_key = c.account_key
  inner join ( -- d: fs gm_account page/line/acct description
    select b.year_month, f.store, d.gl_account, b.page, b.line, b.line_label, e.description
    from fin.fact_fs a
    inner join fin.dim_fs b on a.fs_key = b.fs_key
      and b.year_month > 201700
      -- used cars only
      and b.page = 16 
      and b.line between 1 and 14
    inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
    inner join fin.dim_account e on d.gl_account = e.account
      and e.account_type_code = '4'
    inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key) d on c.account = d.gl_account and b.year_month = d.year_month
  inner join fin.dim_journal aa on a.journal_key = aa.journal_key 
    and aa.journal_code = 'VSU'
    where a.post_status = 'Y' order by control) h
group by year_month, store, page, line, line_label, control
order by year_month, store, page, line;


select year_month, store, page, line, line_label, sum(unit_count)
from (
  select *
  from unit_count) a
group by year_month, store, page, line, line_label
order by year_month, store, page, line, line_label


select * from sls.deals_accounts_routes

-- year_months, accounts, lines
drop table if exists accounts_lines;
create temp table accounts_lines as
select a.fxmcyy, 
  case b.consolidation_grp when '2' then 'RY2' else 'RY1' end as store_code,
  a.fxmpge as the_page, a.fxmlne::integer as line, trim(b.g_l_acct_number) as gl_account
from arkona.ext_eisglobal_sypffxmst a
inner join arkona.ext_ffpxrefdta b on a.fxmact = b.factory_account
  and a.fxmcyy = b.factory_financial_year
where a.fxmcyy > 2011
  and trim(a.fxmcde) = 'GM'
  and coalesce(b.consolidation_grp, '1') <> '3'
  and b.factory_code = 'GM'
  and trim(b.factory_account) <> '331A'
  and b.company_number = 'RY1'
  and b.g_l_acct_number <> ''
  and a.fxmpge = 16 
  and a.fxmlne between 1 and 14;

select * from accounts_lines
select * from fin.fact_Gl limit 10

-- want journal, gl_desc, trans date
-- want trade indicator
drop table if exists uc_trans;
create table uc_trans as
select d.store_code::citext as store, b.the_date, a.control, a.trans, a.seq, b.year_month, 
  d.gl_account::citext, aa.journal_code, c.account_type, d.line, a.amount,
  case 
    when c.account_type = 'sale' and aa.journal_code in ('VSN','VSU') then
      case
        when a.amount < 0 then 1
        else
          case
            when a.amount > 0 then -1
            else 0
          end
      end
    else 0
  end as unit_count,
  e.description as trans_desc,
  case
    when right(trim(a.control),1)::citext in ('a','b','c','d','e','f') then true
    else false
  end as is_trade
from fin.fact_gl a   
inner join dds.dim_date b on a.date_key = b.date_key
  and b.the_year > 2011
inner join fin.dim_account c on a.account_key = c.account_key
inner join accounts_lines d on b.the_year = d.fxmcyy
  and c.account = d.gl_account
inner join fin.dim_journal aa on a.journal_key = aa.journal_key 
inner join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key
where a.post_status = 'Y';
create unique index on uc_trans(trans,seq);

select * from uc_trans limit 100

-- gross matches fs
select store, year_month, sum(amount)
from uc_trans
group by store, year_month
order by store, year_month

-- count matches fs
select store, year_month, line, sum(unit_count)
from uc_trans
group by store, year_month, line
order by store, year_month, line

-- ok, good, matches fs
-- but what i want are actual delivered vehicles and the gross relevant to those vehicles


select *
from uc_trans a
where unit_count <> 0
  and exists (
    select 1
    from uc_trans
    where control = a.control
      and unit_count = -1)
order by control

select store, year_month, count(*)
from (
  select store, control, year_month, line, amount
  from uc_trans
  group by store, control, year_month, line, amount
  having sum(unit_count) > 1) a
group by store, year_month


-- units with count > 2 ?????
-- only if i include amount in the grouping
-- looks like it is vehicles with smallish (adj or mistake) negative amounts
-- in sale account in addition to the actual sale
select a.*,
  sum(unit_count) over (partition by control)
from uc_trans a
where control in (
  select control
  from uc_trans
  group by store, control, year_month, line --,amount
  having sum(unit_count) > 1)
order by control  

select *
from uc_trans a
inner join (
  select year_month, control
  from uc_trans
  where account_type = 'sale'
    and amount < 0
  group by year_month, control
  having count(*) > 1) b on a.control = b.control and a.year_month = b.year_month
order by a.control, a.year_month, a.account_type

select *
from uc_trans
where control in (
select stock_number
from (
select bopmast_id, stock_number
from sls.deals
group by bopmast_id,stock_number) a
group by stock_number
having count(*) > 1)
order by control, the_date


select * from sls.deals where stock_number = '30285xxc'


select line, sum(amount)
from uc_trans
where year_month = 201611
  and store = 'ry1'
group by line
order by line

select year_month, count(*)
from uc_trans
where store = 'ry1'
  and line = 1
  and unit_count = -1
group by year_month


  
select *
from uc_trans
where store = 'ry1'
  and year_month = '201611'
  and account_type = 'sale'
  and line = 5
order by control 


select sum(unit_count) 
from uc_trans
where store = 'ry1'
  and line = 1
  and year_month = 201702

select *
from uc_trans
where year_month = 201702
  and store = 'ry1'
  and line = 1
  and account_type = 'sale'
order by control

-- units with only non-sale transactions in a month
select *
from uc_trans a
where store = 'ry1'
  and year_month > 201700
  and not exists (
    select 1
    from uc_trans 
    where control = a.control
      and year_month = a.year_month
      and account_type = 'sale')


-- what i am trying to accomplish is to generate those transaction that represent an actual vehicle delivery

going about it  backwards, identify the non_delivery transactions
ok, this seems to work maybe, 
eg 30048xx, sold in 201701, 201702 sale in/out 
17273a 201208 sold to zahui, 201209 unw zahui, 201210 sold hajicek
DOES NOT WORK 31242X 201705 sold to demars 201706 unw demars 207016 sold adesa, should show as sale in 201706
select year_month, control
from uc_trans
where store = 'ry1'
  and account_type = 'sale'
group by year_month, control
having sum(unit_count) < 1

so, though i hate to do it, trans_desc is going to be required

looks pretty good
works for 31242x: identifies 201706/demars
17273a:201209/zahui
30048xx: 201702/whirlwind
select year_month, control, trans_desc
from uc_trans
where store = 'ry1'
  and account_type = 'sale'
group by year_month, control, trans_desc
having sum(unit_count) < 1
order by year_month
    
-- so, now lets reverse it and see if we can actually generate just the deliveries
-- sum(unit_count) = 1
select year_month, control, trans_desc
from uc_trans
where store = 'ry1'
  and account_type = 'sale'
group by year_month, control, trans_desc
having sum(unit_count) = 1
order by year_month

select *
from uc_trans a
inner join (
  select year_month, control, trans_desc
  from uc_trans
  where store = 'ry1'
    and account_type = 'sale'
  group by year_month, control, trans_desc
  having sum(unit_count) = 1) b on a.year_month = b.year_month and a.control = b.control and a.trans_desc = b.trans_desc
where a.account_type = 'sale'  

-- this is the base query for deliveries in a month

drop table if exists delivered_trans;
create temp table delivered_trans as
select a.store, a.control,
--   case
--     when right(trim(a.control), 1)::citext in ('a','b','c','d','e','f') then true
--     else false
--   end as is_trade, 
  a.year_month, a.line, max(a.trans_desc) as trans_desc, sum(a.amount) as amount
from uc_trans a
inner join (
  select store, year_month, control, trans_desc
  from uc_trans
  where account_type = 'sale'
  group by store, year_month, control, trans_desc
  having sum(unit_count) = 1) b on a.store = b.store
    and a.year_month = b.year_month 
    and a.control = b.control 
    and a.trans_desc = b.trans_desc
where a.account_type = 'sale'
group by a.store, a.control, a.year_month, a.line; 

select * from delivered_trans limit 100

select *
from delivered_trans a
left join sls.deals b on a.control = b.stock_number
where a.store = 'ry1'
  and a.year_month = 201706
  and a.line = 1
order by a.control  
 
ok, looking good
201706
ry1
line 1
fs: 17
deliveries: 18

31130xx: 
  201705/sold/lawson
  201706/unw/lawson
  201707/sold/fettig

listing all the rows:
select *
from uc_trans
where store = 'ry1'
  and year_month = 201706
  and line = 1
  and account_type = 'sale'
lists 19 rows, including 31130XX  with unit_count = -1
fs = sums unit count = (18 * 1) + (1 * -1) = 17
18 actual deliveries: rows with unit count = 1



ok, now also give an example of why gross <> fs
for 201706, transactions that are not for a delivered deal
201706 line 1
  fs gross: 16,520
  query gross: 19589
  diff: 3069

-- fs:
select a.*,
  sum(amount) over () as total_gross,
  sum(amount) over (partition by account_type) as total_sales_cogs,
  sum(amount) over (partition by control) as unit_gross
from uc_trans a
  where a.year_month = 201706
    and a.store = 'ry1'
    and a.line = 1
order by control, account_type

-- query:
select a.*,
  sum(a.amount) over ()::integer as total_gross,
  sum(a.amount) over (partition by a.account_type) as total_sales_cogs,
  sum(a.amount) over (partition by a.control) as unit_gross
from uc_trans a
inner join (
  select *
  from delivered_trans
  where store = 'ry1'
    and year_month = 201706
    and line = 1) b on a.store = b.store and a.control = b.control and a.trans_desc = b.trans_desc
where a.year_month = 201706
  and a.store = 'ry1'
  and a.line = 1
order by a.control, a.account_type

-- gross on the statement that is not for deliveries in 201706
select x.*, 
  sum(x.amount) over () as gross
from uc_trans x
inner join (
select store, control, year_month, line, trans_desc
from uc_trans a
  where year_month = 201706
    and store = 'ry1'
    and line = 1
  except  
  select store, control, year_month, line, trans_desc
  from delivered_trans a
  where year_month = 201706
    and store = 'ry1'
    and line = 1) xx on x.store = xx.store and x.control = xx.control and x.year_month = xx.year_month and x.line = xx.line and x.trans_desc = xx.trans_Desc



select a.store, a.year_month,
  sum(case when a.account_type = 'sale' and a.line between 1 and 5 then a.unit_count else 0 end) as units_retailed,
  sum(case when a.account_type = 'sale' and a.line between 8 and 10 then a.unit_count else 0 end) as units_wholesaled,
  -1 * sum(case when a.line between 1 and 5 then amount else 0 end)::integer as total_retail_used_car_gross,
  -1 * sum(case when a.line between 8 and 10 then amount else 0 end)::integer as total_wholesale_used_car_gross,
  -(sum(case when a.line between 1 and 5 then amount else 0 end)/sum(case when a.account_type = 'sale' and a.line between 1 and 5 then a.unit_count else 0 end))::integer as retail_pvr,
  -(sum(case when a.line between 8 and 10 then amount else 0 end)/sum(case when a.account_type = 'sale' and a.line between 8 and 10 then a.unit_count else 0 end))::integer as wholesale_pvr,
  -(sum(case when a.is_trade = true and a.line between 1 and 10 then amount else 0 end)/sum(case when a.account_type = 'sale' and a.line between 1 and 5 then a.unit_count else 0 end))::integer as trade_pvr,
  -(sum(case when a.is_trade = false and a.line between 1 and 10 then amount else 0 end)/sum(case when a.account_type = 'sale' and a.line between 1 and 5 then a.unit_count else 0 end))::integer as non_trade_pvr
from uc_trans a
inner join (
  select store, year_month, control, trans_desc
  from uc_trans
  where account_type = 'sale'
  group by store, year_month, control, trans_desc
  having sum(unit_count) = 1) b on a.store = b.store
    and a.year_month = b.year_month 
    and a.control = b.control 
    and a.trans_desc = b.trans_desc
group by a.store, a.year_month
  