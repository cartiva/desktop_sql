﻿doing this all on localhost

11-22-17
copied files to production server:
jon@ubuntu:/mnt/hgfs/E/xfr between vms$ scp ignite_your_autumn.csv rydell@10.130.196.173:/home/rydell/files


-- CREATE EXTENSION file_fdw SCHEMA public;
-- 
-- create server parts foreign data wrapper file_fdw;
-- 
create foreign table ignite (
  part_no citext)
server parts options(filename '/home/jon/Desktop/ignite_your_autumn.csv', format 'csv');

create foreign table power (
  part_no citext)
server parts options(filename '/home/jon/Desktop/power_thru_fall.csv', format 'csv');

drop foreign table if exists oil cascade;
create foreign table oil (
  part_no citext)
server parts options(filename '/home/jon/Desktop/oil_the_wheelsl.csv', format 'csv');

-- create foreign tables on production server
-- tables are created in schema public under Foreign Tables
create foreign table ignite (
  part_no citext)
server parts options(filename '/home/rydell/files/ignite_your_autumn.csv', format 'csv');

create foreign table power (
  part_no citext)
server parts options(filename '/home/rydell/files/power_thru_fall.csv', format 'csv');

drop foreign table if exists oil cascade;
create foreign table oil (
  part_no citext)
server parts options(filename '/home/rydell/files/oil_the_wheelsl.csv', format 'csv');

create temp table lists as
select list_name, a.part_no
from ( -- 3291 rows with a few dups
  select 'power_thru_fall' as list_name, part_no
  from power 
  union
  select 'ignite_your_autumn' as list_name, part_no
  from ignite 
  union
  select 'oil_the_wheels' as list_name, part_no
  from oil) a
inner join dds.ext_pdpmast b on a.part_no = b.part_number
  and b.status = 'A' -- Active
group by list_name, a.part_no;
create unique index on lists(list_name, part_no);

select count(*) from lists
select * from lists limit 100

-- drop foreign table if exists ignite;  
-- drop foreign table if exists oil;  
-- drop foreign table if exists power;  



-- 10/12/17
the counts are all fucked up, i verified a few in arkona:
  10 Parts in inventory
    6 Monthly Demand
    should have used 6 deamand by month

need to dig into some old aqt queries and figure out how to qualify pdptdet
    
 11588804   
  
/*  PDPTDET */
PTCODE - Transaction Code
AP = Add Part
BO = Back Ordered
CM = Comment
CN = Cancelled
CP = RO Customer Pay Sale
CQ = Converted Quantity
CR = RO Correction
CS = RO Cause
DC = Discounts
DP = Delete Part
FR = Factory Return
GC = Stock Group Change
IA = Manual Inventory Adjustment
IS = RO Internal Sale
LA = Lifo Adjust
LS = Lost Sale
MP = Merged Part
OF = Fees
OR = Ordered
PA = Physical Inventory Adjust
PC = Part count from last physical inventory
PO = Purchase order
RC = Special Order Receipt
RC = Received
RO = Reorder
RS = Restocking charge
RT = Counter Return
SA = Counter Sale
SC = RO Service Contract Sale
SH = Shipping
SL = Sublet
SR = RO Return
TT = RO Tech Timedec
WS = RO Warranty sale
ZA = assign core part
ZR = remove core part

per open track:
SOStatus: ptsoep	Status of sale of the part on the transaction
'Blank or Empty' = Part was sold out of inventory
S = Special Order
E = Emergency Purchase
N = Negative on Hand
H = Hold
X = Part was removed from transaction


select ptsoep, count(*)
from dds.ext_pdptdet
where ptdate > 20170000
group by ptsoep

--11/23 not so sure on the ptsoep values now

-- 1 pdptdet ptco_
-- 2 filter ptcode
-- 3 filter ptsoep , this is the table attribute description: spec ord/emerg pu, open track is different

drop table if exists usage;
create temp table usage as
select a.list_name, a.part_no, b.part_description, b.cost, b.qty_on_hand, 
  b.qty_on_order, b.qty_on_back_order,
  sum(case when d.year_month = 201711 then ptqty else 0 end) as nov,
  sum(case when d.year_month = 201710 then ptqty else 0 end) as oct,
  sum(case when d.year_month = 201709 then ptqty else 0 end) as sep,
  sum(case when d.year_month = 201708 then ptqty else 0 end) as aug,
  sum(case when d.year_month = 201707 then ptqty else 0 end) as jul,
  sum(case when d.year_month = 201706 then ptqty else 0 end) as jun,
  sum(case when d.year_month = 201705 then ptqty else 0 end) as may,
  sum(case when d.year_month = 201704 then ptqty else 0 end) as apr,
  sum(case when d.year_month = 201703 then ptqty else 0 end) as mar,
  sum(case when d.year_month = 201702 then ptqty else 0 end) as feb,
  sum(case when d.year_month = 201701 then ptqty else 0 end) as jan,
  sum(case when d.year_month = 201612 then ptqty else 0 end) as dec
--   sum(case when d.year_month = 201611 then ptqty else 0 end) as nov,
--   sum(case when d.year_month = 201610 then ptqty else 0 end) as oct
from lists a
left join dds.ext_pdpmast b on a.part_no = b.part_number
  and b.company_number = 'RY1'
left join dds.ext_pdptdet c on a.part_no = c.ptpart
  and c.ptco_ = 'RY1'
  and c.ptcode in ('CP','IS','SA','WS','SC','SR','CR','RT','FR') 
  -- 11/23 modified filter on ptsoep
--   and c.ptsoep is null
  and (c.ptsoep is null or c.ptsoep in ('N', 'E'))
inner join dds.dim_date d on (select arkona.db2_integer_to_date_long(c.ptdate)) = d.the_date
where d.year_month between 201612 and 201711
--   and ptpart = '11569956'
group by a.list_name, a.part_no, b.part_description, b.cost, b.qty_on_hand, 
  b.qty_on_order, b.qty_on_back_order
order by a.list_name, a.part_no

selecT *
from usage
where part_no = '12585328'

-- 10/16/17
mark came in and asked if i saved my work
he anticipates running this on a monthly basis
should not be a problem
what i need to remember to do is to update dds.ext_pdptdet ON LOCALHOST, 
-- 11/22
lists tables now on production server, update pdptdet on production (173) server
using python projeccts/ext_arkona/ext_pdptdet.py
also update pdpmast

generate dates to do a full scrape of pdptdet for the entire period to be displayed,
today that is 12/01/2016 thru 11/22/2017
ran into an anomaly where an ro part did not show up in pdptdet until the close date
which was later than the ptdate, and got missed in the partial date scrape

-- previous date query for a partial scrape
        select the_date -- generates dates from 10/1 thru 11/21
        from (
          select *, the_year * 10000 + the_month * 100 + the_day as the_date
          from (
          select * from generate_series(2010, 2017, 1) as the_year) a
          cross join (
          select * from generate_series(1, 12, 1) as the_month) b
          cross join (
          select * from generate_series(1, 31, 1) as the_day) c) d
        where d.the_year = 2017
--           and d.the_month <= extract(month from current_Date)
          and d.the_month between 10 and 11
          and the_date < 20171122
        order by the_year,the_month,the_day;

-- date query for full scrape
        select the_date -- generates dates from 12/01/2016 -> 11/22/2017
        from (
          select *, the_year * 10000 + the_month * 100 + the_day as the_date
          from (
          select * from generate_series(2010, 2017, 1) as the_year) a
          cross join (
          select * from generate_series(1, 12, 1) as the_month) b
          cross join (
          select * from generate_series(1, 31, 1) as the_day) c) d
        where the_date between 20161201 and 20171122
        order by the_year,the_month,the_day;    


-- 3/10/18 -----------------------------------------------------------------------------------
3 more lists from mark
  OE Brakes eligible parts list
  Professional Brakes Eligible Parts List
  Advantage Brakes Eligible Parts List

1. moved ext_pdptdet & ext_pdpmast from dds to arkona




-- date query for full scrape
        select the_date -- generates dates from 11/01/2017 -> 03/10/2018
        from (
          select *, the_year * 10000 + the_month * 100 + the_day as the_date
          from (
          select * from generate_series(2010, 2018, 1) as the_year) a
          cross join (
          select * from generate_series(1, 12, 1) as the_month) b
          cross join (
          select * from generate_series(1, 31, 1) as the_day) c) d
        where the_date between 20171101 and 20180310
        order by the_year,the_month,the_day;  

copy lists from local machine to 173
scp /home/jon/Desktop/sql/parts/gm_lists/docs/Advantage_Brakes_Eligible_Parts_List.csv rydell@10.130.196.173:/home/rydell/files
scp /home/jon/Desktop/sql/parts/gm_lists/docs/OE_Brakes_Eligible_Parts_List.csv rydell@10.130.196.173:/home/rydell/files
scp /home/jon/Desktop/sql/parts/gm_lists/docs/Professional_Brakes_Eligible_Parts_List.csv rydell@10.130.196.173:/home/rydell/files  


create foreign table advantage_brakes (
  part_no citext, 
  description citext)
server parts options(filename '/home/rydell/files/Advantage_Brakes_Eligible_Parts_List.csv', format 'csv');

create foreign table oe_brakes (
  part_no citext,
  description citext)
server parts options(filename '/home/rydell/files/OE_Brakes_Eligible_Parts_List.csv', format 'csv');

drop foreign table if exists oil cascade;
create foreign table professinal_brakes (
  part_no citext,
  description citext)
server parts options(filename '/home/rydell/files/Professional_Brakes_Eligible_Parts_List.csv', format 'csv');


create temp table lists as
select list_name, a.part_no
from ( -- 6664 total rows, but only 191 when joined to pdpmast status active
  select 'Advantage_Brakes_Eligible_Parts_List' as list_name, part_no
  from advantage_brakes 
  union 
  select 'OE_Brakes_Eligible_Parts_List' as list_name, part_no
  from oe_brakes 
  union
  select 'Professional_Brakes_Eligible_Parts_List' as list_name, part_no
  from professinal_brakes) a
inner join arkona.ext_pdpmast b on a.part_no = b.part_number
  and b.status = 'A' -- Active
group by list_name, a.part_no;
create unique index on lists(list_name, part_no);

select count(*) from lists
select * from lists limit 100

select count(*) from advantage_brakes
select count(*) from oe_brakes
select count(*) from professinal_brakes

-------------------------------------------------------------------------------------------------------
-- 9/6/18
-------------------------------------------------------------------------------------------------------
2 new lists from mark

save the spreadsheets from mark as csv files with a single column: part_number
fuel_pumps_qpo_parts_list.csv
air_conditioning_qpo_parts_list.csv

copy lists from local machine to 173
scp /home/jon/Desktop/sql/parts/gm_lists/docs/fuel_pumps_qpo_parts_list.csv rydell@10.130.196.173:/home/rydell/files
scp /home/jon/Desktop/sql/parts/gm_lists/docs/air_conditioning_qpo_parts_list.csv rydell@10.130.196.173:/home/rydell/files

create foreign table fuel_pumps (
  part_number citext)
server parts options(filename '/home/rydell/files/fuel_pumps_qpo_parts_list.csv', format 'csv');

create foreign table air_conditioning (
  part_number citext)
server parts options(filename '/home/rydell/files/air_conditioning_qpo_parts_list.csv', format 'csv');

select max(ptdate) from arkona.ext_pdptdet;

foreign tables are in public schema

        select the_date 
        from (
          select *, the_year * 10000 + the_month * 100 + the_day as the_date
          from (
          select * from generate_series(2010, 2018, 1) as the_year) a
          cross join (
          select * from generate_series(1, 12, 1) as the_month) b
          cross join (
          select * from generate_series(1, 31, 1) as the_day) c) d
        where the_date between 20180708 and 20180906
        order by the_year,the_month,the_day;  


create temp table lists as
select list_name, a.part_number
from ( -- 411436 total rows, but only 536 when joined to pdpmast status active
  select 'fuel_pumps_qpo_parts_list' as list_name, part_number
  from fuel_pumps 
  union 
  select 'air_conditioning_qpo_parts_list' as list_name, part_number
  from air_conditioning) a
inner join arkona.ext_pdpmast b on a.part_number = b.part_number
  and b.status = 'A' -- Active
group by list_name, a.part_number;
create unique index on lists(list_name, part_number);

select count(*) from lists
select * from lists limit 100

create index on arkona.ext_pdptdet(ptco_);
create index on arkona.ext_pdptdet(ptcode);
create index on arkona.ext_pdptdet(ptsoep);


drop table if exists usage;
create temp table usage as
select a.list_name, a.part_number, b.part_description, b.cost, b.qty_on_hand, 
  b.qty_on_order, b.qty_on_back_order,
  sum(case when d.year_month = 201808 then ptqty else 0 end) as aug_18,
  sum(case when d.year_month = 201807 then ptqty else 0 end) as jul_18,
  sum(case when d.year_month = 201806 then ptqty else 0 end) as jun_18,
  sum(case when d.year_month = 201805 then ptqty else 0 end) as may_18,
  sum(case when d.year_month = 201804 then ptqty else 0 end) as apr_18,
  sum(case when d.year_month = 201803 then ptqty else 0 end) as mar_18,
  sum(case when d.year_month = 201802 then ptqty else 0 end) as feb_18,
  sum(case when d.year_month = 201801 then ptqty else 0 end) as jan_18,
  sum(case when d.year_month = 201712 then ptqty else 0 end) as dec_17,
  sum(case when d.year_month = 201711 then ptqty else 0 end) as nov_17,
  sum(case when d.year_month = 201710 then ptqty else 0 end) as oct_17,
  sum(case when d.year_month = 201709 then ptqty else 0 end) as sep_17
from lists a
left join arkona.ext_pdpmast b on a.part_number = b.part_number
  and b.company_number = 'RY1'
left join arkona.ext_pdptdet c on a.part_number = c.ptpart
  and c.ptco_ = 'RY1'
  and c.ptcode in ('CP','IS','SA','WS','SC','SR','CR','RT','FR') 
  -- 11/23 modified filter on ptsoep
--   and c.ptsoep is null
  and (c.ptsoep is null or c.ptsoep in ('N', 'E'))
inner join dds.dim_date d on (select arkona.db2_integer_to_date_long(c.ptdate)) = d.the_date
where d.year_month between 201709 and 201808
group by a.list_name, a.part_number, b.part_description, b.cost, b.qty_on_hand, 
  b.qty_on_order, b.qty_on_back_order
order by a.list_name, a.part_number;

-- and the spreadsheet for mark
select *
from usage
order by part_number;

---------------------------------------------------------------------------------
10/19/2018  new lists from mark, 3 in 1 workbook
---------------------------------------------------------------------------------
fresh extract for arkona tables: E:\python projects\ext_arkona
arkona.ext_pdpmast.py: full scrape
arkona.ext_pdptdet.py
  select max(ptdate) from arkona.ext_pdptdet  -- 20180906
  generates the dates for the extract  
        select the_date 
        from (
          select *, the_year * 10000 + the_month * 100 + the_day as the_date
          from (
          select * from generate_series(2010, 2018, 1) as the_year) a
          cross join (
          select * from generate_series(1, 12, 1) as the_month) b
          cross join (
          select * from generate_series(1, 31, 1) as the_day) c) d
        where the_date between 20180901 and 20181018
        order by the_year,the_month,the_day;  

save the spreadsheets from mark as csv files with a single column: part_number
201810_advantage_brakes.csv
201810_oe_brakes.csv
201810_professional_brakes.csv


copy lists from local machine to 173
scp /home/jon/Desktop/sql/parts/gm_lists/docs/201810_advantage_brakes.csv rydell@10.130.196.173:/home/rydell/files
scp /home/jon/Desktop/sql/parts/gm_lists/docs/201810_oe_brakes.csv rydell@10.130.196.173:/home/rydell/files  
scp /home/jon/Desktop/sql/parts/gm_lists/docs/201810_professional_brakes.csv rydell@10.130.196.173:/home/rydell/files  

create foreign table advantage_brakes (
  part_number citext)
server parts options(filename '/home/rydell/files/201810_advantage_brakes.csv', format 'csv');

create foreign table oe_brakes (
  part_number citext)
server parts options(filename '/home/rydell/files/201810_oe_brakes.csv', format 'csv');

create foreign table professional_brakes (
  part_number citext)
server parts options(filename '/home/rydell/files/201810_professional_brakes.csv', format 'csv');


create temp table lists as
select list_name, a.part_number
from ( -- 411436 total rows, but only 536 when joined to pdpmast status active
  select 'advantage' as list_name, part_number
  from advantage_brakes 
  union 
  select 'oe' as list_name, part_number
  from oe_brakes
  union 
  select 'professional' as list_name, part_number
  from professional_brakes) a
inner join arkona.ext_pdpmast b on a.part_number = b.part_number
  and b.status = 'A' -- Active
group by list_name, a.part_number;
create unique index on lists(list_name, part_number);

drop table if exists usage;
create temp table usage as
select a.list_name, a.part_number, b.part_description, b.cost, b.qty_on_hand, 
  b.qty_on_order, b.qty_on_back_order,
  sum(case when d.year_month = 201809 then ptqty else 0 end) as sep_18,
  sum(case when d.year_month = 201808 then ptqty else 0 end) as aug_18,
  sum(case when d.year_month = 201807 then ptqty else 0 end) as jul_18,
  sum(case when d.year_month = 201806 then ptqty else 0 end) as jun_18,
  sum(case when d.year_month = 201805 then ptqty else 0 end) as may_18,
  sum(case when d.year_month = 201804 then ptqty else 0 end) as apr_18,
  sum(case when d.year_month = 201803 then ptqty else 0 end) as mar_18,
  sum(case when d.year_month = 201802 then ptqty else 0 end) as feb_18,
  sum(case when d.year_month = 201801 then ptqty else 0 end) as jan_18,
  sum(case when d.year_month = 201712 then ptqty else 0 end) as dec_17,
  sum(case when d.year_month = 201711 then ptqty else 0 end) as nov_17,
  sum(case when d.year_month = 201710 then ptqty else 0 end) as oct_17
from lists a
left join arkona.ext_pdpmast b on a.part_number = b.part_number
  and b.company_number = 'RY1'
left join arkona.ext_pdptdet c on a.part_number = c.ptpart
  and c.ptco_ = 'RY1'
  and c.ptcode in ('CP','IS','SA','WS','SC','SR','CR','RT','FR') 
  -- 11/23 modified filter on ptsoep
--   and c.ptsoep is null
  and (c.ptsoep is null or c.ptsoep in ('N', 'E'))
inner join dds.dim_date d on (select arkona.db2_integer_to_date_long(c.ptdate)) = d.the_date
where d.year_month between 201710 and 201809
group by a.list_name, a.part_number, b.part_description, b.cost, b.qty_on_hand, 
  b.qty_on_order, b.qty_on_back_order
order by a.list_name, a.part_number;

-- and the spreadsheet for mark
select *
from usage
order by part_number;